<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::get('login', array('as' => 'auth-login', 'uses' => 'AuthController@login'));
    Route::post('postlogin', array('as' => 'auth-post-login', 'uses' => 'AuthController@postLogin'));
    Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@logout'));
});

Route::group(['prefix' => 'admin','middleware' => 'SentinelHasAccess:administrator', 'namespace' => 'Admin'], function () {

    Route::get('dashboard', array('as' => 'admin-dashboard', 'uses' => 'DashboardController@index'));

    // Route::group(['prefix' => 'management'], function () {

        Route::resource('/corporate-identity','CorporateIdentityController',
        [
            'names' => 'admin.corporate-identity',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/team','TeamController',
        [
            'names' => 'admin.team',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/contact','ContactController',
        [
            'names' => 'admin.contact',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/service','ServiceController',
        [
            'names' => 'admin.service',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/service-list','ServiceListController',
        [
            'names' => 'admin.service-list',
            'except' => [
                'show',
            ],
        ]);
        Route::resource('/our-client','OurClientController',
        [
            'names' => 'admin.client',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/career','CareerController',
        [
            'names' => 'admin.career',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/milestone','MilestoneController',
        [
            'names' => 'admin.milestone',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/compliance-and-governance','ComplianceGovernanceController',
        [
            'names' => 'admin.compliance',
            'except' => [
                'show',
            ],
        ]);

        Route::get('/compliance-and-governance/cgc', array('as' => 'admin.cgc.index', 'uses' => 'ComplianceController@cgc'));
        Route::get('/compliance-and-governance/cgc/update', array('as' => 'admin.cgc.update', 'uses' => 'ComplianceController@cgcUpdate'));
        Route::post('/compliance-and-governance/cgc/store', array('as' => 'admin.cgc.store', 'uses' => 'ComplianceController@cgcStore'));

        Route::get('/compliance-and-governance/hse', array('as' => 'admin.hse.index', 'uses' => 'ComplianceController@hse'));
        Route::get('/compliance-and-governance/hse/update', array('as' => 'admin.hse.update', 'uses' => 'ComplianceController@hseUpdate'));
        Route::post('/compliance-and-governance/hse/store', array('as' => 'admin.hse.store', 'uses' => 'ComplianceController@hseStore'));

        Route::post('/compliance-and-governance/create', array('as' => 'create-csr', 'uses' => 'CsrController@storeDesc'));
        Route::get('/charter-of-commitees', array('as' => 'admin.charter.index', 'uses' => 'ComplianceController@login'));

        Route::resource('/compliance-and-governance/charter','CharterController',
        [
            'names' => 'admin.charter',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/compliance-and-governance/csr','CsrController',
        [
            'names' => 'admin.csr',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation','InvestorRelationController',
        [
            'names' => 'admin.investor',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/image-header','ImageHeaderController',
        [
            'names' => 'admin.image-header',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation/financial-highlights','FinancialHighlightController',
        [
            'names' => 'admin.financial',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation/annual-reports','AnnualReportController',
        [
            'names' => 'admin.annual',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation/investor-calendars','InvestorCalendarController',
        [
            'names' => 'admin.calendar',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation/shareholders','ShareholderController',
        [
            'names' => 'admin.shareholder',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/investor-relation/shareholder-compo','ShareholderCompoController',
        [
            'names' => 'admin.shareholder-composition',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/news/corporate-action','CorporateActionController',
        [
            'names' => 'admin.corporate-action',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/news/media-clipping','MediaClippingController',
        [
            'names' => 'admin.media-clipping',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/news/media-center','MediaCenterController',
        [
            'names' => 'admin.media-center',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/file-manager','FileManagerController',
        [
            'names' => 'admin.filemanager',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/footer','FooterController',
        [
            'names' => 'admin.footer',
            'except' => [
                'show',
            ],
        ]);

        Route::resource('/company-with-tram','CompanyWithTramController',
        [
            'names' => 'admin.company',
            'except' => [
                'show',
            ],
        ]);

    // });

});
