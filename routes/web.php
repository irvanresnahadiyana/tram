<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

  return view('frontend.index');

});
Route::get('/admin-login', function () {
	$form = [
        'url' => route('auth-post-login'),
        'autocomplete' => 'off',
    ];
    return view('backend.auth.login', compact('form'));
});
Route::get('/home', array('as' => 'home', 'uses' => 'HomeController@home'));
Route::get('/about', array('as' => 'about', 'uses' => 'HomeController@about'));
Route::get('/milestone', array('as' => 'milestone', 'uses' => 'HomeController@milestone'));
Route::get('/services', array('as' => 'services', 'uses' => 'HomeController@services'));
Route::get('/careers', array('as' => 'careers', 'uses' => 'HomeController@careers'));
Route::get('/compliance', array('as' => 'compliance', 'uses' => 'HomeController@compliance'));
Route::get('/investor-relations', array('as' => 'investor-relation', 'uses' => 'HomeController@investor'));
Route::get('/investor-relation/financial-highlights', array('as' => 'financial-highlight', 'uses' => 'HomeController@financial'));
Route::get('/investor-relation/annual-reports', array('as' => 'annual-report', 'uses' => 'HomeController@annual'));
Route::get('/investor-relation/investor-calendar', array('as' => 'investor-calendar', 'uses' => 'HomeController@investorCalendar'));
Route::get('/investor-relation/shareholder-info', array('as' => 'shareholder-info', 'uses' => 'HomeController@shareholderInfo'));
Route::get('/investor-relation/shareholder-composition', array('as' => 'shareholder-compo', 'uses' => 'HomeController@shareholderCompo'));
Route::get('/news/corporate-year', array('as' => 'corporate-year', 'uses' => 'HomeController@corporateYear'));
Route::get('/news/media-clipping', array('as' => 'media-clipping', 'uses' => 'HomeController@mediaClipping'));
Route::get('/news/media-center', array('as' => 'media-center', 'uses' => 'HomeController@mediaCenter'));
Route::get('/contact-us', array('as' => 'contact-us', 'uses' => 'HomeController@contact'));
Route::get('/service-cate/{id}', array('as' => 'service-cate', 'uses' => 'HomeController@serviceCate'));
