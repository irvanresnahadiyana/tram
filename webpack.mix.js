let mix = require('laravel-mix');
let nodePath = './node_modules/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// vendors scripts

mix.scripts([
    nodePath + 'nestable2/jquery.nestable.js'
], 'public/js/vendor.js');

mix.js('resources/assets/js/app.js', 'public/js');



mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    nodePath + 'nestable2/jquery.nestable.css'
], 'public/css/vendor.css');

mix.styles([
    'resources/assets/css/frontend/main.css',
], 'public/css/frontend.css')
 .version();