// HTML document is loaded
$( window ).on( "load", function() {
        "use strict";

// var preloader
var loader = $( '.preloader-white' );
var bgpreloader = $( '.bg-preloader-white' );

// var navigation
var menumobile = jQuery( '#main-menu' );
var navdefault = jQuery( '.navbar-default-white' );
var Navactive = jQuery( "nav a" );
var subnav = jQuery( ".subnav" );

// loader function
loader.fadeOut( 'slow', function() {
	
	bgpreloader.fadeOut('slow');
	// animated transition & scroll onStep
    onStep();
	
	// mobile icon
    jQuery( ".navbar-toggle" )
                .on( "click", function() {
                    menumobile.toggleClass( 'menu-show' );
                    navdefault.toggleClass( 'fullHeight' );
                } );
								 
});
// loader end function


// sticky menu
var totop = jQuery('#totop');
var bodyScroll = jQuery('html,body');
var subnav = jQuery('.subnav');
var brandblack = jQuery('.navbar-brand.white img.black');
var brandwhite = jQuery('.navbar-brand.white img.white');
var headernav = jQuery('header');

var sticky = (function(){

	var $window, 
		$stickyNav, 
		$stickyParent, 
		stickyPos;

	var init = function(elem, options){
		$window 	       = jQuery(window);
		$stickyNav             = $(elem);
		$stickyParent          = $stickyNav.parent();
		stickyPos              = $stickyNav.offset().top  > 0 && top != 272 ;
		
		_eventHandlers();
	}

	var _stickyValidation = function(){

		var scrollPos = $window.scrollTop();
		
		if((scrollPos && jQuery(window).width() > 1199) >= stickyPos){
			 $stickyNav.addClass('sticky');
			 headernav.addClass('show');
			 brandblack.show();
			 brandwhite.hide();
			 subnav.fadeOut(100);
		}else{
			$stickyNav.removeClass('sticky');
			headernav.removeClass('show');
			brandblack.hide();
			brandwhite.show();
		    subnav.fadeIn(200);
		}
		if (jQuery(window).width() < 1200) {
        	brandblack.show();
			brandwhite.hide();
        }
		if(scrollPos >= 100){
			totop.addClass('show');
		}else{
			totop.removeClass('show');
		}
	}
    
	var _eventHandlers = function(){
		window.addEventListener('scroll', _stickyValidation);
		jQuery(document).height(_stickyValidation);
	}

	return {
		init: init
	}

}());

//Create jquery plugin
if (window.jQuery) {
    (function($) {
        $.fn.sticky = function(options) {
            this.each(function() {
                sticky.init(this, options);
            });
            return this;
        };
    })(window.jQuery);
}else{
	console.warn("jQuery library not defined");
}
  
 // totop var
 totop.on("click", function(e) {
    e.preventDefault();
    bodyScroll.animate({
      scrollTop: 0
    }, 800);
  });
//  sticky menu end

 
// animation block menu
$win.scroll(function() {
	    if ($("header").offset().top > 50) {
			totop.addClass('show');
	    } else {
			totop.removeClass('show');
	    }
});


//search show
 $('#btn-search').on("click", function(e) {
	$('#search-wrap').fadeIn(300);					
 });
 $('.fullblockinput').on("click", function(e) {
	$('#search-wrap').fadeOut(300);									   
 });
 
// expand service content
$('#btn-serv-tog').on("click", function(e) {
    $('#btn-serv-tog').hide();
	$('#serv-tog').addClass('show');				
 });

// contact form
        var contactname1 = $('#name-contact-1');
        var contactemail = $('#email-contact, input#email-contact');
        var contactmessage = $('#message-contact');
        var contactsent1 = $('#send-contact-1');
        //form failed succes var
        var successent = jQuery( "#mail_success" );
        var failedsent = jQuery( "#mail_failed" );
		
    // contact-1 form
	jQuery(function() {
    contactsent1.on('click', function(e) {
      e.preventDefault();
      var e = contactname1.val(),
        a = contactemail.val(),
        s = contactmessage.val(),
        r = !1;
      if (0 == a.length || "-1" == a.indexOf("@") || "-1" == a.indexOf(".")) {
        var r = !0;
        contactemail.css({
          "border": "2px solid #c8b16f"
        });
      } else contactemail.css({
        "border": "2px solid #f1f1f1"
      });
      if (0 == e.length) {
        var r = !0;
        contactname1.css({
          "border": "2px solid #c8b16f"
        });
      } else contactname1.css({
        "border": "2px solid #f1f1f1"
      });
      if (0 == s.length) {
        var r = !0;
        contactmessage.css({
          "border": "2px solid #c8b16f"
        });
      } else contactmessage.css({
        "border": "2px solid #f1f1f1"
      });
      return 0 == r && (contactsent.attr({
        disabled: "true",
        value: "Sending..."
      }), $.ajax({
        type: "POST",
        url: "send.php",
        data: "name=" + e + "&email=" + a + "&subject=You Got Email&message=" + s,
        success: function(e) {
          "success" == e ? (successent.fadeIn(500)) : (failedsent.html(e).fadeIn(500), contactsent.removeAttr("disabled").attr("value", "send").remove())
        }
      })), !1
    })
  });
	
//Magnific Popup html
// $('.detail-page').magnificPopup({					
// 		type: 'ajax',
// 		alignTop: true,
// 		showCloseBtn: true,
// 		overflowY: 'scroll'
// 	});

// // maginific pop video
// $('.open-popup-video').magnificPopup({
//   type:'inline',
//   midClick: true,
//   mainClass: 'custom-popup-class', 
//   callbacks: {
//             open: function () {
//                 $.magnificPopup.instance.close = function () {
//                     $("video").each(function () { this.pause() });
//                     $.magnificPopup.proto.close.call(this);
//                 };
//             }
//         }
// });



		
});
// HTML document is loaded end


$( document ).ready(function() {
  "use strict";
//slideshow background
var bgslideshow = $('#bgslideshow, .bgvertix');
$(function() {
    var slideBegin = 8000,
        transSpeed = 1000,
        simple_slideshow = bgslideshow,
        listItems = simple_slideshow.children('.bgvertix').addClass('animfadebg'),
        listLen = listItems.length,
        i = 0,
        changeList = function() {
            listItems.eq(i).fadeOut(transSpeed);
            i += 1, i === listLen && (i = 0), listItems.eq(i).fadeIn(transSpeed);
        };
    listItems.not(':first').hide(), setInterval(changeList, slideBegin);
});
});