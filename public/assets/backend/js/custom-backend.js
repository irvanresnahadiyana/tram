modal_loader = function(){
    HoldOn.open({
        theme:"sk-circle",
        message:"<h4>Proccessing....</h4>"
    });
};

modal_loader_close = function(){
    HoldOn.close();
};

$(".number-only").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$(".block-space").on({
  keydown: function(e) {
    if (e.which === 32)
      return false;
  },
  change: function() {
    this.value = this.value.replace(/\s/g, "");
  }
});

var slug = function(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
}

preview = function(me,type){
  if (me.files && me.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          // $('#image')
          //     .attr('src', e.target.result);
          $("#preview").attr('src',e.target.result);
      };
      reader.readAsDataURL(me.files[0]);
  }
}

function hideAlert()
{
  setTimeout(function() {
    $(".alert").fadeOut(3000);
  }, 2000);
}

function loadTinyMce()
{
  tinymce.init({
      selector: "textarea.tinymce",
      plugins: [
          "advlist autolink lists link image charmap print preview anchor",
          "searchreplace visualblocks code fullscreen",
          "insertdatetime media table contextmenu paste"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  });
}

function dataTablesIndex(table) {
   table.on('order.dt search.dt', function() {
       var pageIndex = table.page() * table.page.len();
       table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
           cell.innerHTML = pageIndex + i + 1;
       });
   }).draw();
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview-img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}