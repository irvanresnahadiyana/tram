<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'administrator',
            'name' => 'Admin Tram',
            'permissions' => [
                'administrator' => true,
            ],
        ]);

        // Sentinel::getRoleRepository()->createModel()->create([
        //     'slug' => 'salesman',
        //     'name' => 'Salesman',
        //     'permissions' => [
        //         'salesman' => true
        //     ],
        // ]);
    }
}
