<?php

use Illuminate\Database\Seeder;
use App\Models\Footer;

class FooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	\DB::table('footers')->truncate();
        $data = new Footer;
        $data->footer_text = "Lorem Ipsum";
        $data->save();

    }
}
