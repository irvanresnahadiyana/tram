<?php

use Illuminate\Database\Seeder;
use App\Models\Contact;

class contactSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	\DB::table('contacts')->truncate();
        $data = new Contact;
        $data->company_name = "Lorem Ipsum";
        $data->website_name = "Lorem Ipsum";
        $data->tricker_code = "Lorem Ipsum";
        $data->representative_office = "Lorem Ipsum";
        $data->company_address = "Lorem Ipsum";
        $data->media_contact = "Lorem Ipsum";
        $data->corporate_secertary = "Lorem Ipsum";
        $data->stock_exchange = "Lorem Ipsum";
        $data->share_register = "Lorem Ipsum";
        $data->public_accountant = "Lorem Ipsum";
        $data->save();

    }
}
