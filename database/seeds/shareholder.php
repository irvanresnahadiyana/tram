<?php

use Illuminate\Database\Seeder;
use App\Models\ShareholderCompo;

class shareholder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	\DB::table('shareholder_compositions')->truncate();
        $data = new ShareholderCompo;
        $data->description = "Lorem Ipsum";
        $data->description_id = "Lorem Ipsum";
        $data->save();

    }
}
