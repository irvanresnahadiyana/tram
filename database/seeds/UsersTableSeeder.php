<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = \Faker\Factory::create();

        // $avatar = $faker->image(avatar_path(), 128, 128);
        // $avatar = explode(DIRECTORY_SEPARATOR, $avatar);
        // $avatar = last($avatar);

        $user1 = Sentinel::registerAndActivate([
            'image' => 'b6c22e62ed082bbd114151575f93a96c.jpg',
            'email' => 'administrator@tram.com',
            'role_id' => 1,
            'password' => User::DEFAULT_PASSWORD,
            'first_name' => 'Admin',
            'last_name' => 'Tram',
            'is_admin' => true
        ]);

        Sentinel::findRoleBySlug('administrator')->users()->attach($user1);


    }
}
