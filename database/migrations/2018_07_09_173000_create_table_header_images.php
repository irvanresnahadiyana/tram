<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHeaderImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
         Schema::create('header_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_home')->nullable();
            $table->string('image_about')->nullable();
            $table->string('image_service')->nullable();
            $table->string('image_compliance')->nullable();
            $table->string('image_investor_relation')->nullable();
            $table->string('image_news')->nullable();
            $table->string('image_career')->nullable();
            $table->string('image_contact')->nullable();
            $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('header_images');
     }
}
