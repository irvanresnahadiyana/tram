<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCorporateIdentities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('corporate_identities', function (Blueprint $table) {
             $table->increments('id');
             $table->text('visi')->nullable();
             $table->text('misi')->nullable();
             $table->string('estabilishment')->nullable();
             $table->text('line_of_bussiness')->nullable();
             $table->text('legal_basis_estabilishment')->nullable();
             $table->text('about_us')->nullable();
             $table->text('service_description')->nullable();
             $table->text('values')->nullable();
             $table->text('compliance_governance')->nullable();
             $table->text('address')->nullable();
             $table->softDeletes();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('corporate_identities');
     }
}
