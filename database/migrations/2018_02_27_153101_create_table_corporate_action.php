<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCorporateAction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('corporate_actions', function (Blueprint $table) {
             $table->increments('id');
             $table->string('name')->nullable();
             $table->text('description')->nullable();
             $table->text('description_id')->nullable();
             $table->string('image')->nullable();
             $table->string('file')->nullable();
             $table->date('date')->nullable();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('corporate_actions');
     }
}
