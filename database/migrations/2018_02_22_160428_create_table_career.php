<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCareer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('careers', function (Blueprint $table) {
             $table->increments('id');
             $table->text('career_list')->nullable();
             $table->text('career_list_id')->nullable();
             $table->text('career_overview')->nullable();
             $table->text('career_overview_id')->nullable();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('careers');
     }
}
