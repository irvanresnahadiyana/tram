<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldForIndLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('corporate_identities', function (Blueprint $table) {
           $table->text('visi_id')->nullable();
           $table->text('misi_id')->nullable();
           $table->string('estabilishment_id')->nullable();
           $table->text('line_of_bussiness_id')->nullable();
           $table->text('legal_basis_estabilishment_id')->nullable();
           $table->text('about_us_id')->nullable();
           $table->text('service_description_id')->nullable();
           $table->text('values_id')->nullable();
           $table->text('compliance_governance_id')->nullable();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('corporate_identities', function (Blueprint $table) {
           $table->dropColumn('visi_id');
           $table->dropColumn('misi_id');
           $table->dropColumn('estabilishment_id');
           $table->dropColumn('line_of_bussiness_id');
           $table->dropColumn('legal_basis_estabilishment_id');
           $table->dropColumn('about_us_id');
           $table->dropColumn('service_description_id');
           $table->dropColumn('values_id');
           $table->dropColumn('compliance_governance_id');
         });
     }
}
