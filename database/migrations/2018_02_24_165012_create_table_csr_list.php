<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCsrList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('csr_lists', function (Blueprint $table) {
             $table->increments('id');
             $table->string('name')->nullable();
             $table->text('description')->nullable();
             $table->text('description_id')->nullable();
             $table->string('file')->nullable();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('csr_lists');
     }
}
