<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
         Schema::create('contacts', function (Blueprint $table) {
             $table->increments('id');
             $table->string('company_name')->nullable();
             $table->string('website_name')->nullable();
             $table->string('tricker_code')->nullable();
             $table->text('representative_office')->nullable();
             $table->text('company_address')->nullable();
             $table->text('media_contact')->nullable();
             $table->text('corporate_secertary')->nullable();
             $table->text('stock_exchange')->nullable();
             $table->text('share_register')->nullable();
             $table->text('public_accountant')->nullable();
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('contacts');
     }
}
