<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cancel' => 'Cancel',
    'save' => 'Save',
    'create' => 'Create',
    'delete' => 'Delete',
    'confirmation' => 'Confirmation',
    'delete_confirmation' => 'Are you sure you want to delete ?',
    'approval' => 'Yakin untuk mengapprove cuti ini ?',
    'course' => [
                    'name' => 'Course Name',
                    'description' => 'Description'
                ],
    'schedule' => [
                    'date' => 'Shedule Date',
                    'date_until' => 'Repeat time schedule until',
                    'appoinment' => 'Add Appoinment On',
                    'start_time' => 'Start Time',
                    'end_time' => 'End Time',
                    'status' => 'Status',
                    'devine' => 'Devine Into slots',
                    'duration' => 'Duration',
                    'break' => 'Break between slots',
                    'overlap' => 'Force when overlap',
                    'allow' => 'Allow multiple students per slots',
                    'location' => 'Location',
                    'display' => 'Display appoinment to students from',
                    'reminder' => 'Email a reminder',
                ],
    'required' => '<font color="red">*</font>',

];
