@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
   <div class="mainpreloader">
      <span></span>
   </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
   <!-- header -->
   <header class="init">
      <!-- nav -->
      <div class="navbar-default-white navbar-fixed-top">
         <div class="container-fluid m-5-hor">
            <div class="row">
               <!-- menu mobile display -->
               <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
               <span class="icon icon-bar"></span>
               <span class="icon icon-bar"></span>
               <span class="icon icon-bar"></span></button>
               <!-- logo -->
               <a class="navbar-brand white" href="{{ url('/')}}">
                 <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                 <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
               </a>
               <!-- logo end -->
               <!-- mainmenu start -->
               @include('frontend.layout.nav')
               <!-- mainmenu end -->
            </div>
         </div>
         <!-- container -->
      </div>
      <!-- nav end -->
   </header>
   <!-- header end -->
   <!-- subheader -->
   <section id="subheader" style="background: url({{ get_file($imageHeader->image_news) }})top fixed;">
      <div class="container-fluid m-5-hor">
         <div class="row">
            <div class="col-md-12">
               <h1>
                  News
               </h1>
            </div>
         </div>
      </div>
   </section>
   <!-- subheader end -->
   <!-- bahasa -->
   <section class="no-bottom no-top">
      <div class="container-fluid m-5-hor">
         <div class="row">
            <div class="space-single"></div>
            <div class="col-md-12 right">
                <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                <span>/</span>
                <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
            </div>
            <div class="space-single"></div>
         </div>
      </div>
   </section>
   <!-- bahasa end -->
   <!-- content -->
   <section class="whitepage no-top">
      <div class="container-fluid m-5-hor">
         <div class="row">
            <!-- menu -->
            <div class="col-md-12">
                <a class="menu-inv {!! (url(route('media-center')) == Request::url()) ? 'active' : '' !!}" href="{{ route('media-center')}}">Media Center</a>
                <a class="menu-inv {!! (url(route('media-clipping')) == Request::url()) ? 'active' : '' !!}" href="{{ route('media-clipping')}}">Media Clipping</a>
            </div>
            <!-- menu -->
            <div class="space-double"></div>
            <!-- space -->
            <!-- left -->
            <div class="col-md-9">
               <h3>Media Center</h3>
               <hr>
               <div id="accordion" class="panel-group">
                  <!-- panel-group -->
                  @foreach ($media as $key => $value)
                    <div class="panel panel-default acc-click">
                       <!-- panel-default -->
                       <div class="panel-heading">
                          <a data-toggle="collapse" data-parent="#accordion" href="#col-{{$value->id}}" onclick="myFunction();">
                            <h4 >{{ $value->name}}</h4>
                            <div id="acc">
                              @if(@$_GET['lang'] == "id")
                                {!! str_limit($value->description_id, 250, '...') !!}
                              @else
                                {!! str_limit($value->description, 250, '...') !!}
                              @endif
                            </div>
                          </a>
                       </div>
                       <div id="col-{{ $value->id }}" class="panel-collapse collapse">
                          <div class="panel-body">
                            @if(@$_GET['lang'] == "id")
                              {!! $value->description_id !!}
                            @else
                              {!! $value->description !!}
                            @endif
                          </div>
                       </div>
                    </div>
                  @endforeach
                  <!-- panel-default end -->
               </div>
               <hr>
             </div>
               <!-- panel-group end -->
               {{-- <hr> --}}
               <!-- next -->
               <div class="row">
                   <div class="col-md-6 text-left">
                     @if ($media->onFirstPage())
                         <li class="disabled"><span>&laquo;</span></li>
                     @else
                         <a class="prev" href="{{ $media->previousPageUrl() }}"><i class='fa fa-chevron-left'></i>  PREV</a>
                     @endif
                   </div>
                   <div class="col-md-6 text-right">
                     @if ($media->hasMorePages())
                         <a class="next" href="{{ $media->nextPageUrl() }}">NEXT <i class='fa fa-chevron-right'></i></a>
                     @else
                         <li class="disabled"><span>&raquo;</span></li>
                     @endif

                   </div>
               </div>
               <!-- prev -->
            </div>
            <!-- left end -->
            <!-- right -->
            {{-- <div class="col-md-3">
                <div class="date-year">
                    <div id="investio-relation" class="range-date">
                        <h3 class="heading-red">Corporate Action</h3>
                        <ul class="date-container">
                          @foreach (@$dateCorporateAction as $key => $value)
                            @php
                              $year = \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->year;
                            @endphp
                            <li>{{ $year }}
                            @if( \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->year == $year )
                              <i class="fa fa-chevron-down"></i>
                              <ul>
                                  <li class="text-uppercase">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->format('F') }}</li>
                              </ul>
                            @endif

                            </li>
                          @endforeach
                        </ul>
                    </div>
                </div>
            </div> --}}
            <!-- right end -->
         </div>
      </div>
   </section>
   <!-- content end -->
</div>
<!-- content wraper end -->
@section('script')
  <script>
  function myFunction() {
    var x = document.getElementById("acc");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }
  function updateURL() {
      if (history.pushState) {
          var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=en';
          window.history.pushState({path:newurl},'',newurl);

          location.reload();
      }
    }
    function updateURLID() {
        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=id';
            window.history.pushState({path:newurl},'',newurl);

            location.reload();
        }
      }
  </script>

@endsection
