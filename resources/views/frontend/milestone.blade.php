@extends('frontend.layout.master')    
    <!-- preloader -->
    <div class="bg-preloader-white"></div>
    <div class="preloader-white">
        <div class="mainpreloader">
            <span></span>
        </div>
    </div>
    <!-- preloader end -->
    <!-- content wraper -->
    <div class="content-wrapper">
        <!-- header -->
        <header class="init">
            <!-- nav -->
            <div class="navbar-default-white navbar-fixed-top">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <!-- menu mobile display -->
                        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span></button>
                        <!-- logo -->
                        <a class="navbar-brand white" href="{{ url('/')}}">
                            <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                            <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                        </a>
                        <!-- logo end -->
                        <!-- mainmenu start -->
                        @include('frontend.layout.nav')
                        <!-- mainmenu end -->
                    </div>
                </div>
                <!-- container -->
            </div>
            <!-- nav end -->
        </header>
        <!-- header end -->
        <!-- subheader -->
        <section id="subheader-flet">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Our Milestone
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader end -->
        <!-- bahasa -->
        <section class="no-bottom no-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <div class="space-single"></div>
                    <div class="col-md-12 right">
                        <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                        <span>/</span>
                        <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                    </div>
                    <div class="space-single"></div>
                </div>
            </div>
        </section>
        <!-- bahasa end -->
        <!-- content -->
        <section class="no-top no-bottom">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Our Milestone</h3>
                        <p>Tram milestone tram history of many years</p>
                        <span class="devider-cont"></span>
                    </div>
                    <!-- left -->
                    <div class="col-md-8">
                        <div class="space-single"></div>
                        <div class="sp-padding">
                            <div class="row box-round-our-milestone">
                                <div class="col-md-7 col-md-offset-1">
                                    <div class="cont-mile">Milestone TRAM history of many years</div>
                                </div>
                                <div class="col-md-3 text-center">
                                    <a class="btn" href="#mile">SELECTING by YEARS ></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- left end -->
                    <!-- right -->
                    <div class="col-md-4">
                        <div class="sp-padding">
                            <div class="box-round">
                                <iframe scrolling="no" src="https://www.bloomberg.com/quote/TRAM:IJ" style="border: 0px none; margin-left: 0px; height: 250px; margin-top: -81px; width: 100%; overflow: hidden;">
                                </iframe>
                            </div>
                        </div>
                    </div>
                    <!-- right end -->         
                </div>
            </div>
        </section>
        <!-- content end -->
        <!-- content -->
        <section class="whitepage no-top" id="mile">
            <div class="container-fluid m-5-hor">
                <div class="row wrap-retangle">
                    <!-- milestone -->
                    @foreach($milestone as $value)
                    <div class="col-md-4 mile-stone detail-page">
                        <a class="detail-page" data-toggle="modal" href="#myModal-{{$value->id}}">
                            <div class="img">
                                <img class="img-responsive" alt="milestone" src="{{ get_file(@$value->image)}}">
                            </div>
                            <div class="text-wrap">
                            <span>{{ date('Y', strtotime($value->years))}}</span>
                                <a href="#">More detail ></a>
                            </div>
                        </a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal-{{$value->id}}" role="dialog">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" style="color: black">Detail</h4>
                            </div>
                            <div class="modal-body">
                                @if(@$_GET['lang'] == "id")
                                    <p style="color: black">{{ strip_tags(@$value->description_id) }}</p>
                                @else
                                    <p style="color: black">{{ strip_tags(@$value->description) }}</p>
                                @endif
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        
                        </div>
                    </div>
                    @endforeach
                    <!-- milestone end -->
                    <div class="space-double"></div>
                    <!-- next -->
                    <div class="col-md-6 text-left">
                        @if ($milestone->onFirstPage())
                            <li class="disabled"><span>&laquo;</span></li>
                        @else
                            <a class="prev" href="{{ $milestone->previousPageUrl() }}"><i class='fa fa-chevron-left'></i>  PREV</a>
                        @endif
                    </div>
                    <div class="col-md-6 text-right">
                        @if ($milestone->hasMorePages())
                            <a class="next" href="{{ $milestone->nextPageUrl() }}">NEXT <i class='fa fa-chevron-right'></i></a>
                        @else
                            <li class="disabled"><span>&raquo;</span></li>
                        @endif
                    </div>
                    <!-- prev -->
                </div>
            </div>
        </section>