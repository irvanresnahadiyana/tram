@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-news">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        News
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- menu -->
                <div class="col-md-12">
                        <a class="menu-inv {!! (url(route('investor-relation')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-relation')}}">INVESTOR RELATIONS</a>
                        <a class="menu-inv {!! (url(route('shareholder-info')) == Request::url()) ? 'active' : '' !!}" href="{{ route('shareholder-info')}}">SHAREHOLDERS</a>
                        <a class="menu-inv {!! (url(route('financial-highlight')) == Request::url()) ? 'active' : '' !!}" href="{{ route('financial-highlight')}}">FINANCIAL HIGHLIGHTS</a>
                        <a class="menu-inv {!! (url(route('annual-report')) == Request::url()) ? 'active' : '' !!}" href="{{ route('annual-report')}}">ANNUAL REPORTS</a>
                        {{-- <a class="menu-inv {!! (url(route('investor-calendar')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-calendar')}}">INVESTOR CALENDAR</a> --}}
                        <a class="menu-inv {!! (url(route('corporate-year')) == Request::url()) ? 'active' : '' !!}" href="{{ route('corporate-year')}}">CORPORATE ACTION</a>
                </div>
                <!-- menu -->
                <div class="space-double"></div>
                <!-- space -->
                <!-- left -->
                <div class="col-md-9">
                    <h3>Corporate Action</h3>
                    <hr>
                    <div class="text-cont-cor-act">
                        <div class="row wrap-retangle">
                            <!-- row -->
                            @foreach ($corporatAction as $key => $value)
                              <div class="col-md-12 coryear">
                                  <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->format('F Y') }}</span>
                                  @if(@$_GET['lang'] == "id")
                                    <a href="{{ get_file($value->file)}}" target="_blank">{!!html_entity_decode($value->description_id) !!}</a>
                                  @else
                                    <a href="{{ get_file($value->file)}}" target="_blank">{!!html_entity_decode($value->description) !!}</a>
                                  @endif
                                  <hr>
                              </div>
                            @endforeach

                        </div>
                        <!-- row -->
                        <!-- next -->
                        <div class="row">
                            <div class="col-md-6 text-left">
                              @if ($corporatAction->onFirstPage())
                                  <li class="disabled"><span>&laquo;</span></li>
                              @else
                                  <a class="prev" href="{{ $corporatAction->previousPageUrl() }}"><i class='fa fa-chevron-left'></i>  PREV</a>
                              @endif
                            </div>
                            <div class="col-md-6 text-right">
                              @if ($corporatAction->hasMorePages())
                                  <a class="next" href="{{ $corporatAction->nextPageUrl() }}">NEXT <i class='fa fa-chevron-right'></i></a>
                              @else
                                  <li class="disabled"><span>&raquo;</span></li>
                              @endif

                            </div>
                        </div>
                        <!-- prev -->
                    </div>
                </div>
                <!-- left end -->
                <!-- right -->
                <div class="col-md-3">
                    <div class="date-year">
                        <div id="investio-relation" class="range-date">
                            <h3 class="heading-red">Corporate Action</h3>
                            <ul class="date-container">
                                @foreach($dataYearsCorporate as $keyYear => $valMonths)
                                    <li>{{ $keyYear }}
                                      <i class="fa fa-chevron-down"></i>
                                      @foreach($valMonths as $val)
                                        <ul>
                                            <li class="text-uppercase">{{ $val }}</li>
                                        </ul>
                                      @endforeach
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- right end -->
            </div>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- content wraper end -->
@section('script')
  {{-- <script>
  $( document ).ready(function() {
  "use strict";

    // custom next prev
	var $pagination = $('.wrap-retangle');
    var $lis = $pagination.find('div.coryear:not(.prev, .next)');
    $lis.filter(':gt(6)').hide();
    $lis.filter(':lt(7)').addClass('active');

    var $next = $(".next").click(function () {
    var idx = $lis.index($lis.filter('.active:last')) || 0;

        var $toHighlight = $lis.slice(idx + 1, idx + 7);
        if ($toHighlight.length == 0) {
            $prev.show();
            return;
        }

        $next.show();
        $lis.filter('.active').removeClass('active').hide();
        $toHighlight.fadeIn().addClass('active')
    });

    var $prev = $(".prev").click(function () {
    var idx = $lis.index($lis.filter('.active:first')) || 0;

        var start = idx < 6 ? 0 : idx - 7;
        var $toHighlight = $lis.slice(start, start + 7);
        if ($toHighlight.length == 0) {
            $prev.fadeOut();
            return;
        }

        $next.show();
        $lis.filter('.active').removeClass('active').hide();
        $toHighlight.show().addClass('active')
    });
   // custom next prev end

  });
  </script> --}}
@endsection
