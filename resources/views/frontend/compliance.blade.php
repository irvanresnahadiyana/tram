@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader" style="background: url({{ get_file($imageHeader->image_compliance) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Compliance & Governance
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- about us -->
    <section class="whitepage no-top">
    <div class="container-fluid m-5-hor">
        <div class="row">
            <!-- left -->
            <div class="col-md-12">
                <h3>Tram > Compliance & Governance</h3>
                <div class="space-single"></div>
                @if(isset($corporateIdentity))
                    @if(@$_GET['lang'] == "id")
                      {!! $corporateIdentity->compliance_governance_id !!}
                    @else
                      {!! $corporateIdentity->compliance_governance !!}
                    @endif
                @endif
            </div>
            <div class="space-single"></div>
            <div class="col-md-12">
                <h3>Good Corporate Governance<span class="devider-cont"></span></h3>
            </div>
            <div class="space-single"></div>
            <div class="col-md-12">
                @if(isset($cgc))
                @if(@$_GET['lang'] == "id")
                  {!! $cgc->description_id !!}
                @else
                  {!! $cgc->description !!}
                @endif
            @endif
            </div>
            <div class="space-double"></div>
            <div class="col-md-12">
                <h3>Health, Safety and Environment<span class="devider-cont"></span></h3>
            </div>
            <div class="space-single"></div>
            <div class="col-md-4"><img alt="hse-img" class="img-responsive" src="{{ get_file(@$hse->image)}}"></div>
            <div class="col-md-8">
                @if(isset($hse))
                  @if(@$_GET['lang'] == "id")
                    {!! $hse->description_id !!}
                  @else
                    {!! $hse->description !!}
                  @endif
                @endif
            </div>
        </div>
        <!-- left end -->
        <!-- charter -->
        <div class="space-double"></div>
        <div class="space-single"></div>
        <div class=" col-md-12">
            <h3>CHARTER OF COMMITEES<span class="devider-cont"></span></h3>
        </div>
        <div class="space-single"></div>
        <div class="charter">
            @foreach ($charter as $key => $value)
              <a target="_blank" href="{{ get_file(@$value->file)}}"><img alt="charter" class="img-responsive" src="{{ get_file(@$value->image)}}"></a>
            @endforeach
        </div>
    </div>
</div>
<!-- charter end -->
<!-- Vision & Mission -->
<section class="whitepage">
    <div class="container-fluid m-5-hor">
        <div class="row">
            <div class="col-md-12">
                <h3>Corporate Social Responsibility<span class="devider-cont"></span></h3>
            </div>
            <div class="space-single"></div>
            <div class="col-md-12">
                @if(isset($csr))
              @if(@$_GET['lang'] == "id")
                {!! $csr->description_id !!}
              @else
                {!! $csr->description !!}
              @endif
            @endif
            </div>
            <div class="space-double"></div>
            @foreach ($csrList as $key => $value)
              <div class="col-md-3">
                  <div class="csr text-center">
                      <center><img alt="csr" class="" src="{{get_file('assets/frontend/img/csr-img.png')}}"></center>
                      <h5>{{ $value->name }}</h5>
                      <a class="readmore" target="_blank" href="{{ get_file(@$value->file)}}">More detail ></a>
                  </div>
              </div>
            @endforeach
        </div>
    </div>
</section>
<!-- content wraper end -->
