@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-6" style="background: url({{ get_file($imageHeader->image_contact) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Contact Us
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="#">IND</a></span>
                    <span>/</span>
                    <span><a href="#">ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content contact -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <div id="map-1"></div>
                </div>
                <div class="col-md-12">Any inquiries regarding PT Trada Alam Minera Tbk. may be addressed into the following:</div>
                <div class="space-single"></div>
                <div class="col-md-12">
                    <b style="
                    font-weight: 600;">COMPANY NAME</b>
                    <div class="space-half"></div>
                    <p>{{ $contact->company_name }}</p>
                </div>
                <div class="space-single"></div>
                <div class="col-md-4">
                    <b style="
                    font-weight: 600;">WEBSITE NAME</b>
                    <div class="space-half"></div>
                    <p>{{ $contact->website_name }}</p>
                    <div class="space-single"></div>
                    <span><b style="
                        font-weight: 600;">ADDRESS</b></span>
                    <div class="space-half"></div>
                    <p>{!! $contact->company_address !!}</p>
                </div>
                <div class="col-md-4">
                    <b style="
                    font-weight: 600;">REPRESENTATIVE OFFICE</b>
                    <div class="space-half"></div>
                    <p>{!! $contact->representative_office !!}</p>
                </div>
                <div class="col-md-4">
                    <b style="
                    font-weight: 600;">TICKER CODE</b>
                    <div class="space-half"></div>
                    <p>{{ $contact->tricker_code }}</p>
                    <div class="space-half"></div>
                    {{-- <span><b style="
                        font-weight: 600;">Media Contact</b></span>
                    <div class="space-half"></div>
                    <p>{!! $contact->media_contact !!}</p> --}}
                </div>
                <div class="col-md-4" style="
                padding-top: 20px;">
                    <b style="
                    font-weight: 600;">STOCK EXCHANGE</b>
                    <div class="space-half"></div>
                    <p>{!! $contact->stock_exchange !!}</p>
                </div>
                <div class="space-single"></div>
                <div class="col-md-4">
                    <b style="
                    font-weight: 600;">CORPORATE SECRETARY</b>
                    <div class="space-half"></div>
                    <p>{!! $contact->corporate_secertary !!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- content contact end -->
    <!-- form  -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor bg-contact">
            <div class="row">
                <div class="col-md-4">
                    <span>SHARE REGISTER</span>
                    <div class="space-half"></div>
                    <p>{!! $contact->share_register !!}</p>
                    <div class="space-single"></div>
                    <span>PUBLIC ACCOUNTANT</span>
                    <div class="space-half"></div>
                    <p>{!! $contact->public_accountant !!}</p>
                </div>
                <div class="col-md-8">
                    <form id="form-contact1">
                        <div class="form-group">Address Your Inquiry Here</div>
                        <div class="form-group user-name">
                            <input type="text" class="form-control" required="" id="name-contact-1" placeholder="Your Name">
                        </div>
                        <div class="form-group user-email">
                            <input type="email" class="form-control" required="" id="email-contact" placeholder="Your Email">
                        </div>
                        <div class="form-group user-message">
                            <textarea class="form-control" required="" id="message-contact" placeholder="Your Message"></textarea>
                            <div class="success" id="mail_success">Thank you. Your message has been sent</div>
                            <div class="error" id="mail_failed">Error, email not sent</div>
                        </div>
                        <div class="form-group"><button type="submit" id="send-contact-1" class="btn-contact">Submit</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- form end  -->
</div>
<!-- content wraper end -->
