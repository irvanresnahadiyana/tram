<!-- mainmenu start -->
<div class="white menu-init" id="main-menu">
    <nav id="menu-center">
        <ul>
            <li><a class="{!! (url(route('home')) == Request::url()) ? 'actived' : '' !!}" href="{{ route('home') }}">Home</a></li>
            <li><a class="{!! (url(route('about')) == Request::url()) ? 'actived' : '' !!}" href="{{ route('about') }}">About</a></li>
            <li><a class="{!! (url(route('services')) == Request::url() OR Request::is('service-cate/*') ) ? 'actived' : '' !!}" href="{{ route('services')}}">Services</a></li>
            <li><a class="{!! (url(route('compliance')) == Request::url()) ? 'actived' : '' !!}"href="{{ route('compliance')}}">Compliance & Governance</a></li>
            <li><a class="{!! (url(route('investor-relation')) == Request::url() OR Request::is('investor-relation/*')) ? 'actived' : '' !!}"href="{{ route('investor-relation')}}">Investor Relations</a></li>
            <li><a class="{!! (url(route('corporate-year')) == Request::url() OR Request::is('news/*')) ? 'actived' : '' !!}"href="{{ route('media-center')}}">News</a></li>
            <li><a class="{!! (url(route('careers')) == Request::url()) ? 'actived' : '' !!}"href="{{ route('careers')}}">Career</a></li>
            <li><a class="{!! (url(route('contact-us')) == Request::url()) ? 'actived' : '' !!}"href="{{ route('contact-us')}}">Contact</a></li>
            <li>
                <a  id="btn-search" class="hidden-md hidden-sm hidden-xs">
                <i class="fa fa-search"></i></a>
                <div id="search-wrap">
                    <form role="search">
                        <div class="fullblockinput hidden-md hidden-sm hidden-xs"></div>
                        <div class="input-group">
                            <input type="text" id="search" class="form-control" placeholder="search">
                            <div class="input-group-btn"><button type="submit"><span class="icon"><i class="fa fa-search"></i></span></button></div>
                        </div>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
</div>
<!-- mainmenu end -->
