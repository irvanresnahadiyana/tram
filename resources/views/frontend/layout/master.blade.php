<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="utf-8">
        <title>| PT Trada Alam Minera Tbk</title>
        <meta content="" name="description">
        <meta content="" name="author">
        <meta content="" name="keywords">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <!-- favicon -->
        <link href="{{ get_file('assets/frontend/img/favicon.png') }}" rel="icon" sizes="32x32" type="image/png">
        <!-- Bootstrap CSS -->
        {!! Html::style('assets/frontend/css/bootstrap.min.css') !!}
        <!-- font themify CSS -->
        {!! Html::style('assets/frontend/css/themify-icons.css') !!}
        <!-- font awesome CSS -->
        {!! Html::style('assets/frontend/font-awesome/css/font-awesome.css') !!}
        <!-- revolution slider css -->
        {!! Html::style('assets/frontend/css/fullscreen.css', array('media' => 'screen')) !!}
        {!! Html::style('assets/frontend/css/rev-settings.css', array('media' => 'screen', 'type' => 'text/css')) !!}
        {!! Html::style('assets/frontend/rs-plugin/css/settings.css', array('media' => 'screen')) !!}
        <!-- on3step CSS -->
        {!! Html::style('assets/frontend/css/animated-on3step.css', array('rel' => 'stylesheet')) !!}
        {!! Html::style('assets/frontend/css/owl.carousel.css', array('rel' => 'stylesheet')) !!}
        {!! Html::style('assets/frontend/css/owl.theme.css', array('rel' => 'stylesheet')) !!}
        {!! Html::style('assets/frontend/css/owl.transitions.css', array('rel' => 'stylesheet')) !!}
        {!! Html::style('assets/frontend/css/tram.css', array('rel' => 'stylesheet')) !!}
        {!! Html::style('assets/frontend/css/queries-tram.css', array('rel' => 'stylesheet', 'media' => 'all', 'type' => 'text/css')) !!}
    </head>
    <body>

        @yield('content')
        
        <!-- Footer -->
        <footer class="foo">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <div class="col-md-12">
                        {!! $footer->footer_text !!}
                    </div>
                </div>
            </div>
        </footer>

        <!-- ScrolltoTop -->
        <div id="totop" class="init">
            <span class="ti-angle-up"></span>
        </div>

        <!--  map google  -->
        <script>
                function initMap() {
                    var myLatLng = {lat: -6.245598, lng: 106.802220};
                
                    var map = new google.maps.Map(document.getElementById('map-1'), {
                    zoom: 4,
                    center: myLatLng
                    });
                
                    var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'Hello World!'
                    });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCQ5KODzqooIP496GPLRaMAsZ4eN8Vro_U"></script>
            {!! Html::script('assets/frontend/js/map-1.js') !!}
            <script>
                // $(document).ready(function(){
                //     $('img').addClass('img-responsive');
                // });
                // $( document ).ready(function() {
                //     "use strict";
                    
                //     // custom next prev  
                //     var $pagination = $('.wrap-retangle');
                //     var $lis = $pagination.find('div.mile-stone:not(.prev, .next)');
                //     $lis.filter(':gt(8)').hide();
                //     $lis.filter(':lt(9)').addClass('active');
                    
                //     var $next = $(".next").click(function () {
                //     var idx = $lis.index($lis.filter('.active:last')) || 0;
                        
                //         var $toHighlight = $lis.slice(idx + 1, idx + 10);
                //         if ($toHighlight.length == 0) {
                //             $prev.show();
                //             return;
                //         }
                        
                //         $next.show();        
                //         $lis.filter('.active').removeClass('active').hide();
                //         $toHighlight.fadeIn().addClass('active')
                //     });
                    
                //     var $prev = $(".prev").click(function () {
                //     var idx = $lis.index($lis.filter('.active:first')) || 0;
                        
                //         var start = idx < 8 ? 0 : idx - 9;
                //         var $toHighlight = $lis.slice(start, start + 9);
                //         if ($toHighlight.length == 0) {
                //             $prev.fadeOut();
                //             return;
                //         }      
                        
                //         $next.show();
                //         $lis.filter('.active').removeClass('active').hide();
                //         $toHighlight.show().addClass('active')
                //     });
                //     // custom next prev end
                    
                //     });
            </script>
        @yield('script')
        <!-- plugin JS -->
        {!! Html::script('assets/frontend/plugin-js/plugin.js') !!}

        {!! Html::script('assets/frontend/plugin-js/bootstrap.min.js') !!}
        <!-- tram JS -->
        {!! Html::script('assets/frontend/js/main.js') !!}

        
        <script>
            function updateURL() {
                if (history.pushState) {
                    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=en';
                    window.history.pushState({path:newurl},'',newurl);
            
                    location.reload();
                }
                }
                function updateURLID() {
                    if (history.pushState) {
                        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=id';
                        window.history.pushState({path:newurl},'',newurl);
            
                        location.reload();
                    }
                }
        </script>
    </body>
</html>