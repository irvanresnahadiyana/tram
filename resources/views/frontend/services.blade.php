@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                    <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                    <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader" style="background: url({{ get_file($imageHeader->image_service) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Our Services
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- about us -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- left -->
                <div class="col-md-7">
                    <div class="sp-padding">
                        <h3>
                            Our Services
                            <span class="devider-cont"></span>
                        </h3>
                        <p id="serv-tog" class="init">
                            @if(isset($corporateIdentity))
                              @if(@$_GET['lang'] == "id")
                                {!! $corporateIdentity->service_description_id !!}
                              @else
                                {!! $corporateIdentity->service_description !!}
                              @endif
                            @endif
                        </p>
                        {{-- <a id="btn-serv-tog" class="readmore">Read more ></a> --}}
                    </div>
                </div>
                <!-- left end -->
                <!-- right -->
                <div class="col-md-4 col-md-offset-1">
                    <div class="sp-padding">
                        <div class="companies">
                            <span>Companies Corporated with Tram</span>
                            @foreach ($company as $val)
                            
                            {{-- <span>  PT. SMRUTAMA </span>
                            <span>  >  PT. Gunungbarautama </span>
                            <span>  Ship services, Mining and </span>
                            <span>  Infrastructure </span> --}}
                            <li><a href="{{ $val->link }}" target="_blank"><span>{{ $val->company_name }}</span></a></li>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- right -->
                <!-- coorporate -->
                <div class="col-md-12">
                    @foreach ($service as $key => $value)
                      <a class="btn-serv" href="{{ route('service-cate', ['id' => $value->id]) }}">{{ $value->name }}</a>
                    @endforeach
                </div>
                <!-- coorporate end -->
            </div>
        </div>
    </section>
    <!-- about us end -->
    <!-- Vision & Mission -->
    <section class="whitepage">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <h3>
                    our-clients
                    <span class="devider-cont"></span>
                </h3>
                <div class="space-double"></div>
                @foreach ($client as $key => $value)
                  <div class="col-md-2">
                      <img alt="brand" class="img-responsive" src="{{ get_file($value->image) }}">
                  </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Vision & Mission end -->
</div>
<!-- content wraper end -->
