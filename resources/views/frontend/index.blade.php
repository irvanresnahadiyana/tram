<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="utf-8">
        <title>Tram | Trada Minera Tbk</title>
        <meta content="" name="description">
        <meta content="" name="author">
        <meta content="" name="keywords">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <!-- favicon -->
        <link href="{{ get_file('assets/frontend/img/favicon.png') }}" rel="icon" sizes="32x32" type="image/png">
        <!-- Bootstrap CSS -->
        {!! Html::style('assets/frontend/css/bootstrap.min.css') !!}
        <!-- font themify CSS -->
        {!! Html::style('assets/frontend/css/themify-icons.css') !!}
        <!-- font awesome CSS -->
        {!! Html::style('assets/frontend/css/font-awesome.css') !!}
        <!-- revolution slider css -->
        <link rel="stylesheet" type="text/css" href="css/fullscreen.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
        <link rel="stylesheet" href="css/rev-settings.css" type="text/css">
        <!-- on3step CSS -->
        {!! Html::style('assets/frontend/css/animated-on3step.css') !!}
        {!! Html::style('assets/frontend/css/owl.carousel.css') !!}
        {!! Html::style('assets/frontend/css/owl.theme.css') !!}
        {!! Html::style('assets/frontend/css/owl.transitions.css') !!}
        {!! Html::style('assets/frontend/css/tram.css') !!}
        {!! Html::style('assets/frontend/css/queries-tram.css', array('media' => 'all', 'type' => 'text/css')) !!}
    </head>
    <body>
        <!-- preloader -->
        <div class="bg-preloader-white"></div>
        <div class="preloader-white">
            <div class="mainpreloader">
                <span></span>
            </div>
        </div>
        <!-- preloader end -->
        <!-- content wraper -->
        <a id="bgslideshow" href="{!! route('home') !!}">
            <div class="bgvertix"><img alt="background" src="{{ get_file('assets/frontend/img/intro/bg-1.jpg') }}"></div>
            <div class="bgvertix"><img alt="background" src="{{ get_file('assets/frontend/img/intro/bg-2.jpg') }}"></div>
            <div class="bgvertix"><img alt="background" src="{{ get_file('assets/frontend/img/intro/bg-3.jpg') }}"></div>
            <div class="overlay-main hidden-md hidden-sm hidden-xs"></div>
            <div class="overlay-black hidden-lg"></div>
            <div class="row">
                <div class="col-lg-6 col-md-12 text-center">
                    <img class="logo" alt="background" align="middle" src="{{ get_file('assets/frontend/img/intro/logo.png') }}">
                    <h1 class="mobile hidden-lg">Welcome to Tram</h1>
                </div>
                <div class="col-lg-offset-6"></div>
            </div>
            <div class="row hidden-md hidden-sm hidden-xs">
                <div class="col-lg-6 col-lg-offset-6 text-center">
                    <h1 class="desktop">Welcome to Tram</h1>
                </div>
            </div>
        </a>
        <!-- content wraper end -->
        <!-- plugin JS -->
        {!! Html::script('assets/frontend/plugin-js/plugin.js') !!}
        <!-- tram JS -->
        {!! Html::script('assets/frontend/js/main.js') !!}
    </body>
</html>
