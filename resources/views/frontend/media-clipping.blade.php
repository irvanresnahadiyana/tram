@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<style>
    #fade {
  display: none;
  position: fixed;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: black;
  z-index: 1001;
  -moz-opacity: 0.8;
  opacity: .80;
  filter: alpha(opacity=80);
}

#light {
  display: none;
  position: absolute;
  top: auto;
    left: 68%;
  max-width: 600px;
  max-height: 360px;
  margin-left: -300px;
  margin-top: -180px;
  border: 2px solid #FFF;
  background: #FFF;
  z-index: 1002;
  overflow: visible;
}

#boxclose {
  float: right;
  cursor: pointer;
  color: #fff;
  border: 1px solid #AEAEAE;
  border-radius: 3px;
  background: #222222;
  font-size: 31px;
  font-weight: bold;
  display: inline-block;
  line-height: 0px;
  padding: 11px 3px;
  position: absolute;
  right: 2px;
  top: 2px;
  z-index: 1002;
  opacity: 0.9;
}

.boxclose:before {
  content: "×";
}

#fade:hover ~ #boxclose {
  display:none;
}

.test:hover ~ .test2 {
  display: none;
}
</style>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-news">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        News
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- menu -->
                <div class="col-md-12">
                        <a class="menu-inv {!! (url(route('media-center')) == Request::url()) ? 'active' : '' !!}" href="{{ route('media-center')}}">Media Center</a>
                        <a class="menu-inv {!! (url(route('media-clipping')) == Request::url()) ? 'active' : '' !!}" href="{{ route('media-clipping')}}">Media Clipping</a>
                </div>
                <!-- menu -->
                <div class="space-double"></div>
                <!-- space -->
                <!-- left -->
                <div class="col-md-9">
                  <h3>Media Clipping</h3>
                  <div class="space-half"></div>
                  <div class="row">
                      @foreach ($media as $key => $value)
                        @if($value->type == "VIDEO")
                          <div class="col-md-4 text-center media-cliping" style="overflow:visible !important">
                              {{-- <a class="open-popup-video" href="#video-{{$value->id}}">
                              <img class="img-responsive" alt="img" src="{{ get_file(@$value->image)}}">
                              <span>{{ $value->name }}</span>
                              </a>
                              <!-- video -->
                              <div id="video-{{$value->id}}" class="popup-video mfp-hide">
                                  <div class="some-element">
                                      <video width="600" controls>
                                          <source src="{{ get_file(@$value->file)}}" type="video/mp4">
                                          Your browser does not support HTML5 video.
                                      </video>
                                  </div>
                              </div> --}}
                              <!-- video end -->
                              <div id="light">
                                    <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
                                    <video id="VisaChipCardVideo" width="600" height="400" controls>
                                        <source src="{{ get_file(@$value->file)}}" type="video/mp4">
                                        <!--Browser does not support <video> tag -->
                                      </video>
                                  </div>
                                  
                                  <div id="fade" onClick="lightbox_close();"></div>
                                  
                                  <div>
                                      
                                    <a href="#" onclick="lightbox_open();">
                                            <img class="img-responsive" alt="img" src="{{ get_file(@$value->image)}}">
                                            <span>{{ $value->name }}</span>
                                    </a>
                                  </div>
                          </div>
                          
                        @else
                          <div class="col-md-4 text-center media-cliping">
                              <a href="{{ get_file(@$value->image)}}" target="_blank">
                              <img class="img-responsive" alt="img" src="{{ get_file($value->image)}}">
                              {{-- <span>Read more</span> --}}
                              <span>{{ $value->name }}</span>
                              </a>
                          </div>
                        @endif
                      @endforeach
                  </div>
                  <div class="space-half"></div>
                  <!-- next -->
                  <div class="row">
                      <div class="col-md-6 text-left">
                        @if ($media->onFirstPage())
                            <li class="disabled"><span>&laquo;</span></li>
                        @else
                            <a class="prev" href="{{ $media->previousPageUrl() }}"><i class='fa fa-chevron-left'></i>  PREV</a>
                        @endif
                      </div>
                      <div class="col-md-6 text-right">
                        @if ($media->hasMorePages())
                            <a class="next" href="{{ $media->nextPageUrl() }}">NEXT <i class='fa fa-chevron-right'></i></a>
                        @else
                            <li class="disabled"><span>&raquo;</span></li>
                        @endif

                      </div>
                  </div>
                  <!-- prev -->
              </div>
                <!-- left end -->
                <!-- right -->
                {{-- <div class="col-md-3">
                    <div class="date-year">
                        <div id="investio-relation" class="range-date">
                            <h3 class="heading-red">Corporate Action</h3>
                            <ul class="date-container">
                              @foreach (@$dateCorporateAction as $key => $value)
                                @php
                                  $year = \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->year;
                                @endphp
                                <li>{{ $year }}
                                @if( \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->year == $year )
                                  <i class="fa fa-chevron-down"></i>
                                  <ul>
                                      <li class="text-uppercase">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $value->date)->format('F') }}</li>
                                  </ul>
                                @endif

                                </li>
                              @endforeach
                            </ul>
                        </div>
                    </div>
                </div> --}}
                <!-- right end -->
            </div>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- content wraper end -->
@section('script')
  {{-- <script>
  $( document ).ready(function() {
  "use strict";

    // custom next prev
	var $pagination = $('.wrap-retangle');
    var $lis = $pagination.find('div.coryear:not(.prev, .next)');
    $lis.filter(':gt(6)').hide();
    $lis.filter(':lt(7)').addClass('active');

    var $next = $(".next").click(function () {
    var idx = $lis.index($lis.filter('.active:last')) || 0;

        var $toHighlight = $lis.slice(idx + 1, idx + 7);
        if ($toHighlight.length == 0) {
            $prev.show();
            return;
        }

        $next.show();
        $lis.filter('.active').removeClass('active').hide();
        $toHighlight.fadeIn().addClass('active')
    });

    var $prev = $(".prev").click(function () {
    var idx = $lis.index($lis.filter('.active:first')) || 0;

        var start = idx < 6 ? 0 : idx - 7;
        var $toHighlight = $lis.slice(start, start + 7);
        if ($toHighlight.length == 0) {
            $prev.fadeOut();
            return;
        }

        $next.show();
        $lis.filter('.active').removeClass('active').hide();
        $toHighlight.show().addClass('active')
    });
   // custom next prev end

  });
  </script> --}}
  <script>
      window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  lightBoxVideo.play();
}

function lightbox_close() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  lightBoxVideo.pause();
}
  function updateURL() {
      if (history.pushState) {
          var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=en';
          window.history.pushState({path:newurl},'',newurl);

          location.reload();
      }
    }
    function updateURLID() {
        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?lang=id';
            window.history.pushState({path:newurl},'',newurl);

            location.reload();
        }
      }
  </script>
@endsection
