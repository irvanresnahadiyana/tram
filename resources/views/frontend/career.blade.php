@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-5" style="background: url({{ get_file($imageHeader->image_career) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>Career with</h1>
                    <h1>Trada Maritime</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="#">IND</a></span>
                    <span>/</span>
                    <span><a href="#">ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- about us -->
    <section class="whitepage no-top">
    <div class="container-fluid m-5-hor">
        <div class="row">
            <!-- left -->
            <div class="col-md-12">
                <div class="sp-padding">
                    <h3>Welcome to Tram</h3>
                    <p>Welcome to our employment section. Here you can view our current job openings and apply for positions online.</p>
                </div>
                <div class="space-single"></div>
                <div class="col-md-12">
                    <h3>Career Listings<span class="devider-cont"></span></h3>
                    @if(isset($career))
                        {!! $career->career_list !!}
                    @endif
                </div>
                <div class="space-single"></div>
                <div class="col-md-6">
                    <h3>Overview<span class="devider-cont"></span></h3>
                </div>
                <div class="col-md-3 col-md-offset-3 text-left">
                    <a class="btn-career" href="mailto:hrd@tram.co.id">hrd@tram.co.id</a>
                </div>
                <div class="space-half"></div>
                <div class="col-md-12">
                    @if(isset($career))
                        {!! $career->career_overview !!}
                    @endif
                </div>
                <div class="space-half"></div>
                {{-- <div class="col-md-12">
                    <p>Responsibilities</p>
                    <div class="space-half"></div>
                    <p>• Gather and evaluate user requirements in collaboration with product managers and engineers</p>
                    <p>• Illustrate design ideas using storyboards, process flows and sitemaps</p>
                    <p>• Design graphic user interface elements, like menus, tabs and widgets</p>
                    <p>• Build page navigation buttons and search fields</p>
                    <p>• Identify and troubleshoot UX problems (e.g. responsiveness)</p>
                    <p>• Conduct layout adjustments based on user feedback</p>
                    <p>• Adhere to style standards on fonts, colors and images</p>
                    <div class="space-half"></div>
                    <p>Requirements</p>
                    <p>• Proven work experience as a UI/UX Designer or similar role</p>
                    <p>• Portfolio of design projects</p>
                    <p>• Knowledge of wireframe tools (e.g. Wireframe.cc and InVision)</p>
                    <p>• Good time-management skills</p>
                    <p>• BSc in Design, Computer Science or relevant field</p>
                    <div class="space-half"></div>
                    <p>• Gathering and evaluating user requirements, in collaboration with product managers and engineers</p>
                    <p>• Illustrating design ideas using storyboards, process flows and sitemaps</p>
                    <p>• Designing graphic user interface elements, like menus, tabs and widgets</p>
                </div> --}}
            </div>
        </div>
        <!-- left end -->
    </div>
</div>
<!-- about us end -->
</div>
<!-- content wraper end -->
