@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                    <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                    <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-flet">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Our Fleet
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h3>Our Main Fleet</h3>
                    <span class="devider-cont"></span>
                </div>
                <div class="space-half"></div>
                <!-- menu -->
                <div class="col-md-12">
                    @foreach ($serviceAllItem as $key => $value)
                      <a class="menu-inv {!! Request::is('service-cate/'.$value->id) ? 'active' : '' !!}" href="{{ route('service-cate', ['id' => $value->id]) }}">{{ $value->name }}</a>
                    @endforeach
                </div>
                <!-- menu -->
                <div class="space-single"></div>
                <div class="col-md-12">
                    <h3>{{ $service->name }}</h3>
                    <div class="space-half"></div>
                    @if(@$_GET['lang'] == "id")
                      {!! $service->description !!}
                    @else
                      {!! $service->description !!}
                    @endif
                    {{-- <div class="space-single"></div>
                    <p>Below are the list of our dry bulk vessels</p>
                    <div class="space-half"></div> --}}
                </div>
                <!-- left -->
                <div class="col-md-8">
                    <div class="inv-content">
                        <div class="row">
                            <!-- conten db1 -->
                            @foreach ($serviceAll as $key => $value)
                              <div class="col-md-3">
                                  <img class="img-responsive" alt="img" src="{{ get_file($value->image)}}">
                              </div>
                              <div class="col-md-9">
                                  {!! $value->description !!}
                              </div>
                              <div class="space-double"></div>
                              <!-- spasi -->
                              <!-- conten db2 -->
                            @endforeach

                            {{-- <div class="col-md-3">
                                <img class="img-responsive" alt="img" src="img/dry-bulk/db2.jpg">
                            </div>
                            <div class="col-md-9">
                                <div class="fso-text">
                                    <span class="desc">Type </span>
                                    <span class="devider">:</span>
                                    <span>Towing Vessel 	 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Horsepower  </span>
                                    <span class="devider">:</span>
                                    <span> 2 x 1,100 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">DWT </span>
                                    <span class="devider">:</span>
                                    <span>171 tons </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">LOA 	</span>
                                    <span class="devider">:</span>
                                    <span>28.84 m 	 	 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Draft </span>
                                    <span class="devider">:</span>
                                    <span>2.775 m 	 	 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Breadth 	</span>
                                    <span class="devider">:</span>
                                    <span>8.00 m</span>
                                </div>
                                <!-- conten db2 end -->
                            </div>
                            <div class="space-double"></div>
                            <!-- spasi -->
                            <!-- conten db3 -->
                            <div class="col-md-3">
                                <img class="img-responsive" alt="img" src="img/dry-bulk/db3.jpg">
                            </div>
                            <div class="col-md-9">
                                <div class="fso-text">
                                    <span class="desc">Type </span>
                                    <span class="devider">:</span>
                                    <span>Flat Top Barge </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Cargo Capacity(M/T) </span>
                                    <span class="devider">:</span>
                                    <span>8,600 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">DWT </span>
                                    <span class="devider">:</span>
                                    <span>8,200 tons</span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">LOA 	</span>
                                    <span class="devider">:</span>
                                    <span>28.84 m </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Draft </span>
                                    <span class="devider">:</span>
                                    <span>2.775 m 	 </span>
                                </div>
                                <div class="fso-text">
                                    <span class="desc">Breadth 	</span>
                                    <span class="devider">:</span>
                                    <span>24.38 m</span>
                                </div>
                                <!-- conten db3 end -->
                            </div> --}}
                            <div class="space-double"></div>
                            <!-- spasi -->
                            <!-- next -->
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <a class="prev" href="#"><i class='fa fa-chevron-left'></i>  PREV</a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="next" href="#">NEXT <i class='fa fa-chevron-right'></i></a>
                                </div>
                            </div>
                            <!-- prev -->
                        </div>
                    </div>
                </div>
                <!-- left end -->
                <!-- right -->
                <div class="col-md-4">
                    <div class="sp-padding">
                        <div class="box-round">
                          <iframe scrolling="no" src="https://www.bloomberg.com/quote/TRAM:IJ" style="border: 0px none; margin-left: 0px; height: 250px; margin-top: -81px; width: 100%; overflow: hidden;">
                          </iframe>
                        </div>
                    </div>
                </div>
                <!-- right end -->
            </div>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- content wraper end -->
