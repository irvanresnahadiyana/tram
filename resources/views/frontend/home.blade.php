@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                    <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader" style="background: url({{ get_file($imageHeader->image_home) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Welcome to TRAM
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- about us -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- left -->
                <div class="col-md-7">
                    <div class="sp-padding">
                        <h3>
                            About Us
                            <span class="devider-cont"></span>
                        </h3>
                        @if(isset($corporateIdentity))
                            @if(@$_GET['lang'] == "id")
                              {!! str_limit($corporateIdentity->about_us_id, 250, '...') !!}
                            @else
                              {!! str_limit($corporateIdentity->about_us, 250, '...') !!}
                            @endif
                        @endif
                        <a class="readmore" href="{{ route('about') }}">Read more ></a>
                    </div>
                </div>
                <!-- left end -->
                <!-- right -->
                @include('frontend.layout.idx')
                <!-- right -->
            </div>
        </div>
    </section>
    <!-- about us end -->
    <!-- coorporate -->
    <section class="whitepage">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        Corporate Identity  -  PT. Trada Alam Minera Tbk
                        <span class="devider-cont"></span>
                    </h3>
                </div>
                <div class="col-md-2">
                    <div class="sp-padding">
                        <p>
                            <strong>Head Office Address</strong>
                            @if(isset($corporateIdentity))
                                {!! $corporateIdentity->address !!}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <div class="sp-padding">
                        <p>
                            <strong>Line Of Bussiness</strong>
                            @if(isset($corporateIdentity))
                                @if(@$_GET['lang'] == "id")
                                  {!! $corporateIdentity->line_of_bussiness_id !!}
                                @else
                                  {!! $corporateIdentity->line_of_bussiness !!}
                                @endif
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="sp-padding">
                        <p>
                            <strong>Date of Establishment</strong>
                            @if(isset($corporateIdentity))
                                {!! $corporateIdentity->estabilishment !!}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="sp-padding">
                        <p>
                            <strong>Legal Basis of Establishment</strong>
                            @if(isset($corporateIdentity))
                                @if(@$_GET['lang'] == "id")
                                  {!! $corporateIdentity->legal_basis_estabilishment_id !!}
                                @else
                                  {!! $corporateIdentity->legal_basis_estabilishment !!}
                                @endif
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <p>
                        <strong>Share Listing      10 September 2018 with ticker code : TRAM</strong>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- coorporate end -->
    <!-- Vision & Mission -->
    <section class="whitepage">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-3">
                    <img class="img-responsive" alt="logo" src="{{ get_file('assets/frontend/img/visi-misi.jpg') }}">
                </div>
                <div class="col-md-8 col-md-offset-1">
                    <h5>
                        Our Vision & Mission
                        <span class="devider-cont"></span>
                    </h5>
                    <p>
                    <div class="l-norm">
                        <strong class="vision">Vision</strong>
                        @if(isset($corporateIdentity))
                            @if(@$_GET['lang'] == "id")
                              {!! $corporateIdentity->visi_id !!}
                            @else
                              {!! $corporateIdentity->visi !!}
                            @endif
                        @endif
                    </div>
                    </p>
                    <p>
                    <div class="l-norm">
                        <strong class="vision">Mission</strong>
                        @if(isset($corporateIdentity))
                            @if(@$_GET['lang'] == "id")
                              {!! $corporateIdentity->misi_id !!}
                            @else
                              {!! $corporateIdentity->misi !!}
                            @endif
                        @endif
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Vision & Mission end -->
    <!-- Value -->
    <section class="whitepage">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-round">
                        <i class="fa fa-line-chart"></i>
                        <div class="cont-val">
                            <span><strong>Passion for Growth</strong></span>
                            <span>Dynamic, assertive, and achiever.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-round">
                        <i class="fa fa-lightbulb-o"></i>
                        <div class="cont-val">
                            <span><strong>Forward Thinking</strong></span>
                            <span>Always identify the right business
                            opportunities.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-round">
                        <i class="ti-wand"></i>
                        <div class="cont-val">
                            <span><strong>Excellent</strong></span>
                            <span>Professional, focus, and teamwork.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Value end -->
</div>
<!-- content wraper end -->
