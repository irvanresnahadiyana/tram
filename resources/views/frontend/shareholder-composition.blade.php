@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-1">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Investor Relations
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- menu -->
                <div class="col-md-12">
                    <a class="menu-inv {!! (url(route('investor-relation')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-relation')}}">INVESTOR RELATIONS</a>
                    <a class="menu-inv {!! (url(route('shareholder-info')) == Request::url() OR url(route('shareholder-compo')) == Request::url()) ? 'active' : '' !!}" href="{{ route('shareholder-info')}}">SHAREHOLDERS</a>
                    <a class="menu-inv {!! (url(route('financial-highlight')) == Request::url()) ? 'active' : '' !!}" href="{{ route('financial-highlight')}}">FINANCIAL HIGHLIGHTS</a>
                    <a class="menu-inv {!! (url(route('annual-report')) == Request::url()) ? 'active' : '' !!}" href="{{ route('annual-report')}}">ANNUAL REPORTS</a>
                    {{-- <a class="menu-inv {!! (url(route('investor-calendar')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-calendar')}}">INVESTOR CALENDAR</a> --}}
                    <a class="menu-inv {!! (url(route('corporate-year')) == Request::url()) ? 'active' : '' !!}" href="{{ route('corporate-year')}}">CORPORATE ACTION</a>
                </div>
                <!-- menu -->
                <!-- left -->
                <div class="col-md-9">
                    <div class="inv-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="head-share"><strong>Shareholders</strong></h5>
                                <a class="sharehold {!! (url(route('shareholder-info')) == Request::url()) ? 'active' : '' !!}" href="{{ route('shareholder-info')}}">Shareholders Information</a>
                                -
                                <a class="sharehold {!! (url(route('shareholder-compo')) == Request::url()) ? 'active' : '' !!}" href="{{ route('shareholder-compo')}}">Shareholders Composition</a>
                            </div>
                            <div class="space-single"></div>
                            <div class="col-md-12 text-cont">
                              @if(@$_GET['lang'] == "id")
                                {!! $shareholderCompo->description_id !!}
                              @else
                                {!! $shareholderCompo->description !!}
                              @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- left end -->
                <!-- right -->
                <div class="col-md-3">
                  <div class="date-year">
                      <div id="investio-relation" class="range-date">
                            <h3 class="heading-red">Investor Relations</h3>
                            <ul class="date-container">
                              @foreach($dataYears as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                            </ul>
                        </div>
                      <div id="financial-highlights" class="range-date">
                          <h3 class="heading-red">Financial Highlights</h3>
                          <ul class="date-container">
                              @foreach($dataYearsFinancial as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                          </ul>
                      </div>
                      <div id="annual-reports" class="range-date">
                          <h3 class="heading-red">Annual Reports</h3>
                          <ul class="date-container">
                              @foreach($dataYearsAnnual as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                          </ul>
                      </div>
                      {{-- <div id="investor-calendar" class="range-date">
                          <h3 class="heading-red">Investor Calendar</h3>
                          <ul class="date-container">
                            @foreach($dataYearsCalendar as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                          </ul>
                      </div> --}}
                  </div>
                </div>
                <!-- right end -->
            </div>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- content wraper end -->
