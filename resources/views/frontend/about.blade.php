@extends('frontend.layout.master')
        <!-- preloader -->
        <div class="bg-preloader-white"></div>
        <div class="preloader-white">
            <div class="mainpreloader">
                <span></span>
            </div>
        </div>
        <!-- preloader end -->
        <!-- content wraper -->
        <div class="content-wrapper">
            <!-- header -->
            <header class="init">
                <!-- nav -->
                <div class="navbar-default-white navbar-fixed-top">
                    <div class="container-fluid m-5-hor">
                        <div class="row">
                            <!-- menu mobile display -->
                            <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span>
                            <span class="icon icon-bar"></span></button>
                            <!-- logo -->
                            <a class="navbar-brand white" href="{{ url('/')}}">
                              <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                              <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                            </a>
                            <!-- logo end -->
                            <!-- mainmenu start -->
                            @include('frontend.layout.nav')
                            <!-- mainmenu end -->
                        </div>
                    </div>
                    <!-- container -->
                </div>
                <!-- nav end -->
            </header>
            <!-- header end -->
            <!-- subheader -->
            <section id="subheader" style="background: url({{ get_file($imageHeader->image_about) }})top fixed;">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>
                                About Us
                            </h1>
                        </div>
                    </div>
                </div>
            </section>
            <!-- subheader end -->
            <!-- bahasa -->
            <section class="no-bottom no-top">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <div class="space-single"></div>
                        <div class="col-md-12 right">
                            <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                            <span>/</span>
                            <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                        </div>
                        <div class="space-single"></div>
                    </div>
                </div>
            </section>
            <!-- bahasa end -->
            <!-- about us -->
            <section class="whitepage no-top">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <!-- left -->
                        <div class="col-md-7">
                            <div class="sp-padding">
                            @if(isset($corporateIdentity))
                              @if(@$_GET['lang'] == "id")
                                {!! $corporateIdentity->about_us_id !!}
                              @else
                                {!! $corporateIdentity->about_us !!}
                              @endif
                            @endif
                            </div>
                        </div>
                        <!-- left end -->
                        <!-- right -->
                        <div class="col-md-4 col-md-offset-1">
                            <div class="sp-padding">
                                <div class="box-round">
                                    <iframe scrolling="no" src="https://quote.kontan.co.id/TRAM" style="border: 0px none;m;margin-left: 0p;height: 560px;mar;margin-top: -438px;th: 100%;overflow: hidden;">
                                    </iframe>
                                </div>
                            </div>
                            <div class="red-color">Vision & Mission</div>
                            <div class="box-round">
                                <div class="vis-mis">
                                    <div class="red-color">VISION</div>
                                    @if(isset($corporateIdentity))
                                        @if(@$_GET['lang'] == "id")
                                        {!! $corporateIdentity->visi_id !!}
                                        @else
                                        {!! $corporateIdentity->visi !!}
                                        @endif
                                    @endif
                                </div>
                                <div class="vis-mis">
                                    <div class="red-color">MISSION STATEMENT</div>
                                    @if(isset($corporateIdentity))
                                        @if(@$_GET['lang'] == "id")
                                        {!! $corporateIdentity->misi_id !!}
                                        @else
                                        {!! $corporateIdentity->misi !!}
                                        @endif
                                    @endif
                                </div>
                                <div class="vis-mis">
                                    <div class="red-color">VALUES</div>
                                    @if(isset($corporateIdentity))
                                        @if(@$_GET['lang'] == "id")
                                        {!! $corporateIdentity->values_id !!}
                                        @else
                                        {!! $corporateIdentity->values !!}
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- right -->
                    </div>
                </div>
            </section>
            <!-- about us end -->
            <section class="whitepage">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Meet The executives</h3>
                        </div>
                        <div class="clearfix p-60"></div>
                    </div>
                    <div class="row color-page p-30">
                        <div class="col-md-12">
                            <div class="sp-padding">
                                <h4 class="head-team">Board of Commissioners</h4>
                                <span class="devider-cont"></span>
                            </div>
                        </div>
                        @foreach ($team as $key => $value)
                          @if($value->type == "BOC")
                          <div class="col-md-4 onStep" data-animation="fadeInUp" data-time="300">
                              <div class="team">
                                  <a class="detail-page" data-toggle="modal" href="#myModal-{{$value->id}}">
                                      <img alt="imageportofolio" class="" src="{{ get_file(@$value->image) }}" style="display: inline-block !important">
                                      <p class="vision strong">{{ @$value->name }}</p>
                                      <p class="vision">{{ @$value->jabatan }}</p>
                                      <p class="biography">Biography</p>
                                  </a>
                              </div>
                          </div>
                          <!-- Modal -->
                          <div class="modal fade" id="myModal-{{$value->id}}" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title" style="color: black">{{ @$value->name }}</h4>
                                </div>
                                <div class="modal-body">
                                  <p style="color: black">{{ strip_tags(@$value->biography) }}</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          @endif
                        @endforeach
        
                        <div class="col-md-12">
                           <div class="sp-padding">
                              <h4 class="head-team">Board of Directors</h4>
                              <span class="devider-cont"></span>
                           </div>
                        </div>
                        @foreach ($team as $key => $value)
                          @if($value->type == "BOD")
                          <div class="col-md-4 onStep" data-animation="fadeInUp" data-time="300">
                             <div class="team">
                                <a class="detail-page" data-toggle="modal" href="#myModal-{{$value->id}}">
                                   <img alt="imageportofolio" class="" src="{{ get_file(@$value->image) }}" style="display: inline-block !important">
                                   <p class="vision strong">{!! $value->name !!}
                                   <p class="vision">{{ @$value->jabatan }}</p>
                                   <p class="biography">Biography</p>
                                </a>
                             </div>
                          </div>
                          <!-- Modal -->
                          <div class="modal fade" id="myModal-{{$value->id}}" role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title" style="color: black">{{ @$value->name }}</h4>
                                </div>
                                <div class="modal-body">
                                  <p style="color: black">{{ strip_tags(@$value->biography) }}</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          @endif
                        @endforeach
                    </div>
                </div>
            </section>
            <!-- coorporate end -->
            <!-- Vision & Mission -->
            <section class="whitepage no-bottom">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sp-padding">
                                <h4 class="head-team">Group of Companies</h4>
                                <span class="devider-cont"></span>
                            </div>
                        </div>
                        <div class="col-md-12"><img class="img-responsive" alt="logo" src="{{ get_file(@$corporateIdentity->group_of_companies) }}"></div>
                    </div>
                </div>
            </section>
            <!-- Vision & Mission end -->
            <!-- Value -->
            <section class="whitepage">
                <div class="container-fluid m-5-hor">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sp-padding">
                                <h4 class="head-team">Our Milestone</h4>
                                <span class="devider-cont"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row box-round-our-milestone">
                        <div class="col-md-8 col-md-offset-1">
                            <div class="cont-mile">Milestone TRAM history of many years</div>
                        </div>
                        <div class="col-md-3 text-center">
                            <a class="btn" href="{{ route('milestone')}}">SELECTING by YEARS ></a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Value end -->
        