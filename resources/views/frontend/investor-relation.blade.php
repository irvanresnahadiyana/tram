@extends('frontend.layout.master')

<!-- preloader -->
<div class="bg-preloader-white"></div>
<div class="preloader-white">
    <div class="mainpreloader">
        <span></span>
    </div>
</div>
<!-- preloader end -->
<!-- content wraper -->
<div class="content-wrapper">
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <div class="container-fluid m-5-hor">
                <div class="row">
                    <!-- menu mobile display -->
                    <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span></button>
                    <!-- logo -->
                    <a class="navbar-brand white" href="{{ url('/')}}">
                      <img class="white" alt="logo" src="{{ get_file('assets/frontend/img/logo.png') }}">
                      <img class="black" alt="logo" src="{{ get_file('assets/frontend/img/logo-white.png') }}">
                    </a>
                    <!-- logo end -->
                    <!-- mainmenu start -->
                    @include('frontend.layout.nav')
                    <!-- mainmenu end -->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- nav end -->
    </header>
    <!-- header end -->
    <!-- subheader -->
    <section id="subheader-1" style="background: url({{ get_file($imageHeader->image_investor_relation) }})top fixed;">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        Investor Relations
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader end -->
    <!-- bahasa -->
    <section class="no-bottom no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <div class="space-single"></div>
                <div class="col-md-12 right">
                    <span><a href="JavaScript:void(0);" onclick="updateURLID();">IND</a></span>
                    <span>/</span>
                    <span><a href="JavaScript:void(0);" onclick="updateURL();"> ENG</a></span>
                </div>
                <div class="space-single"></div>
            </div>
        </div>
    </section>
    <!-- bahasa end -->
    <!-- content -->
    <section class="whitepage no-top">
        <div class="container-fluid m-5-hor">
            <div class="row">
                <!-- menu -->
                <div class="col-md-12">
                    <a class="menu-inv {!! (url(route('investor-relation')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-relation')}}">INVESTOR RELATIONS</a>
                    <a class="menu-inv {!! (url(route('shareholder-info')) == Request::url()) ? 'active' : '' !!}" href="{{ route('shareholder-info')}}">SHAREHOLDERS</a>
                    <a class="menu-inv {!! (url(route('financial-highlight')) == Request::url()) ? 'active' : '' !!}" href="{{ route('financial-highlight')}}">FINANCIAL HIGHLIGHTS</a>
                    <a class="menu-inv {!! (url(route('annual-report')) == Request::url()) ? 'active' : '' !!}" href="{{ route('annual-report')}}">ANNUAL REPORTS</a>
                    {{-- <a class="menu-inv {!! (url(route('investor-calendar')) == Request::url()) ? 'active' : '' !!}" href="{{ route('investor-calendar')}}">INVESTOR CALENDAR</a> --}}
                    <a class="menu-inv {!! (url(route('corporate-year')) == Request::url()) ? 'active' : '' !!}" href="{{ route('corporate-year')}}">CORPORATE ACTION</a>
                </div>
                <!-- menu -->
                <!-- left -->
                <div class="col-md-9">
                  @foreach ($investor as $key => $value)
                    <div class="inv-content">
                        <div class="row">
                            <div class="col-md-3">
                                <img class="img-responsive" alt="img" src="{{ get_file($value->image)}}">
                            </div>
                            <div class="col-md-9 text-cont">
                                @if(@$_GET['lang'] == "id")
                                  {!! $value->description_id !!}
                                @else
                                  {!! $value->description !!}
                                @endif
                                <a target="_blank" href="{{ get_file($value->file) }}">Read more</a>
                            </div>
                        </div>
                    </div>
                  @endforeach
                    <!-- next -->
                    <div class="row">
                        <div class="col-md-6 text-left">
                          @if ($investor->onFirstPage())
                              <li class="disabled"><span>&laquo;</span></li>
                          @else
                              <a class="prev" href="{{ $investor->previousPageUrl() }}"><i class='fa fa-chevron-left'></i>  PREV</a>
                          @endif
                        </div>
                        <div class="col-md-6 text-right">
                          @if ($investor->hasMorePages())
                              <a class="next" href="{{ $investor->nextPageUrl() }}">NEXT <i class='fa fa-chevron-right'></i></a>
                          @else
                              <li class="disabled"><span>&raquo;</span></li>
                          @endif

                        </div>
                    </div>
                    <!-- prev -->
                </div>
                <!-- left end -->
                <!-- right -->
                <div class="col-md-3">
                    <div class="date-year">
                        <div id="investio-relation" class="range-date">
                            <h3 class="heading-red">Investor Relations</h3>
                            <ul class="date-container">
                              @foreach($dataYears as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                            </ul>
                        </div>
                        <div id="financial-highlights" class="range-date">
                            <h3 class="heading-red">Financial Highlights</h3>
                            <ul class="date-container">
                                @foreach($dataYearsFinancial as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                            </ul>
                        </div>
                        <div id="annual-reports" class="range-date">
                            <h3 class="heading-red">Annual Reports</h3>
                            <ul class="date-container">
                                @foreach($dataYearsAnnual as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                            </ul>
                        </div>
                        {{-- <div id="investor-calendar" class="range-date">
                            <h3 class="heading-red">Investor Calendar</h3>
                            <ul class="date-container">
                              @foreach($dataYearsCalendar as $keyYear => $valMonths)
                                  <li>{{ $keyYear }}
                                    <i class="fa fa-chevron-down"></i>
                                    @foreach($valMonths as $val)
                                      <ul>
                                          <li class="text-uppercase">{{ $val }}</li>
                                      </ul>
                                    @endforeach
                                  </li>
                              @endforeach
                            </ul>
                        </div> --}}
                    </div>
                </div>
                <!-- right end -->
            </div>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- content wraper end -->
