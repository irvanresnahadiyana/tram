<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta('robots', 'noindex, nofollow') !!}
        {!! Html::meta('product', env('APP_NAME', 'FGBMFI')) !!}
        {!! Html::meta('description', env('APP_NAME', 'FGBMFI')) !!}
        {!! Html::meta('author', 'FGBMFI') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}

        <title>{{ env('APP_NAME') }} - @yield('title')</title>

        {!! Html::style('assets/backend/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! Html::style('assets/backend/bower_components/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('assets/backend/bower_components/Ionicons/css/ionicons.min.css') !!}

        @yield('header')

        {!! Html::style('assets/backend/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('assets/backend/plugins/iCheck/square/blue.css') !!}
    </head>


    {!! Form::open([
            'route' =>  'store-set-password',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box box-primary" style="margin-top: 20px">
          <div class="box-header with-border">
             <h3 class="box-title">Set Your Password</h3>
          </div>
            <div class="box-body">
              <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                  {!! Form::label('password', "Password", ['class' => 'col-sm-2 control-label']) !!}
                  <div class="col-sm-5">
                      <input type="password" name="password" class="form-control">
                      {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>
              <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                  {!! Form::label('password_confirmation', "Password Confirmation", ['class' => 'col-sm-2 control-label']) !!}
                  <div class="col-sm-5">
                      <input type="password" name="password_confirmation" class="form-control">
                      {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>
              <input type="hidden" name="id" value="{{ $data->id }}" class="form-control">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</html>
