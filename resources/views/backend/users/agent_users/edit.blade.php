@extends('backend.layouts.master.master')

@section('title')
    Agent Management - {{ trans('general.edit') }}
@endsection

@section('page-header')
    Agent Management <small>{{trans('general.edit')}}</small>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li><a href="{!! route('admin.agent.users.index') !!}">Agent Management</a></li>
        <li class="active">{{ trans('general.edit') }}</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::model($data,['route' =>['admin.agent.users.update',$data->id],'class' =>'form-horizontal','id' => 'form-user','method' => 'PATCH']) !!}
            @include('backend.users.agent_users._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{!! route('admin.users.index') !!}" class="btn btn-danger">Cancel</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
@endsection

@include('backend.users.scripts.create_script')
