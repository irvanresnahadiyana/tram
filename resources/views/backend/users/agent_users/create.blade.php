@extends('backend.layouts.master.master')

@section('title')
    {{ trans('general.users') }} {{ trans('general.management') }} - {{ trans('general.create_new') }}
@endsection

@section('page-header')
    Agent Management
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li><a href="{!! route('admin.agent.users.index') !!}">Agent Management</a></li>
        <li class="active">{{ trans('general.create_new') }} Agent</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.agent.users.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box box-primary" style="margin-top:20px">
            <div class="box-header with-border">
               <h3 class="box-title">Add New Agent</h3>
            </div>
            <div class="box-body">
                @include('backend.users.agent_users._form')
                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                    {!! Form::hidden('password', "Password", ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-5">
                        <input type="hidden" name="password" class="form-control" value="qwertyuiopasdfghjklzxcvbnmm">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{!! route('admin.agent.users.index') !!}" class="btn btn-danger">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@include('backend.users.scripts.create_script')
