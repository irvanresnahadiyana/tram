<div class="container">
    <div class="row">
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}} ">
            {!! Form::label('email', "Email", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::email('email', old('email') , ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name', "Agent Name", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('name', old('name') , ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
            {!! Form::label('phone_number', "Phone Number", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::number('phone_number', old('phone_number') , ['class' => 'form-control']) !!}
                {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            {!! Form::label('description', "Agent Description", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::textarea('description', old('description') , ['class' => 'form-control']) !!}
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
            {!! Form::label('address', "Address", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::textarea('address', old('address') , ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group {{ $errors->has('kecamatan') ? 'has-error' : ''}}">
            {!! Form::label('kecamatan', "Kecamatan", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('kecamatan', old('kecamatan') , ['class' => 'form-control']) !!}
                {!! $errors->first('kecamatan', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('kelurahan') ? 'has-error' : ''}}">
            {!! Form::label('kelurahan', "Kelurahan", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('kelurahan', old('kelurahan') , ['class' => 'form-control']) !!}
                {!! $errors->first('kelurahan', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('kota') ? 'has-error' : ''}}">
            {!! Form::label('kota', "Kota", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('kota', old('kota') , ['class' => 'form-control']) !!}
                {!! $errors->first('kota', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('provinsi') ? 'has-error' : ''}}">
            {!! Form::label('provinsi', "Provinsi", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-5">
                {!! Form::text('provinsi', old('provinsi') , ['class' => 'form-control']) !!}
                {!! $errors->first('provinsi', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
