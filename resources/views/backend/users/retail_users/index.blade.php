@extends('backend.layouts.master.master')

@section('title')
    {{ trans('general.users') }} {{ trans('general.management') }}
@endsection

@section('page-header')
    {{ trans('general.users') }} {{ trans('general.management') }}  <small>List Retail Users</small>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-map"></i> Home</a></li>
        <li class="active">{{ trans('general.list') }} {{ trans('general.users') }}</li>
    </ol>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-body">
        @if(Session::has('success'))
            <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
        @endif
        <a href="{!! route('admin.retail.users.create') !!}"><button type="button" class="btn btn-primary">+ Add User</button></a>
        {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
    </div>
</div>
@include('partials.delete-modal')
@endsection
@include('backend.users.scripts.index_script')
