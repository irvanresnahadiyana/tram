@extends('backend.layouts.master.master')

@section('title')
    {{ trans('general.users') }} {{ trans('general.management') }} - {{ trans('general.create_new') }}
@endsection

@section('page-header')
    {{ trans('general.users') }} {{ trans('general.management') }} <small>{{trans('general.create_new')}} Principle</small>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li><a href="{!! route('admin.principle.users.index') !!}">{{ trans('general.users') }} {{ trans('general.management') }}</a></li>
        <li class="active">{{ trans('general.create_new') }} Principle</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.principle.users.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
        ]) !!}
        <div class="box">
            <div class="box-body">
                @include('backend.users._form')  
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="{!! route('admin.users.index') !!}" class="btn btn-danger">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>              
            </div>
        </div>
    </form>
@endsection

@include('backend.users.scripts.create_script')