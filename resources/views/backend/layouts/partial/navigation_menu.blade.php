<ul class="sidebar-menu" data-widget="tree">
    <li class="header">NAVIGATION MENU</li>
    <li class="{!! (url(route('admin-dashboard')) == Request::url()) ? 'active' : '' !!}">
        <a href="{{route('admin-dashboard')}}"><i class='fa fa-tachometer'></i> <span>Dashboard</span></a>
    </li>
    <li class="{!! (url(route('admin.corporate-identity.index')) == Request::url()) ? 'active' : '' !!}">
        <a href="{{route('admin.corporate-identity.index')}}"><i class='fa fa-address-card'></i> <span>Corporate Identity</span></a>
    </li>
    <li class="{!! (Request::is('admin/milestone') OR Request::is('admin/milestone/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.milestone.index')}}"><i class='fa fa-cog'></i> <span>Milestone</span></a>
    </li>
    <li class="{!! (url(route('admin.team.index')) == Request::url()) ? 'active' : '' !!}">
        <a href="{{route('admin.team.index')}}"><i class='fa fa-user'></i> <span>Team</span></a>
    </li>
    <li class="treeview{!! (Request::is('admin/service-list') OR Request::is('admin/service/*') OR Request::is('admin/service') OR Request::is('admin/company-with-tram') ) ? ' active' : '' !!}">
	    <a href="#"><i class="fa fa-link"></i> <span>Services Management</span> <i class="fa fa-chevron-down"></i>
	        <span class="pull-right-container">
	            <i class="fa fa-cog-left pull-right"></i>
	        </span>
	    </a>
	    <ul class="treeview-menu">
	        <li class="{!! (Request::is('admin/service/create') OR Request::is('admin/service') ) ? ' active' : '' !!}"><a href="{{route('admin.service.index')}}"><i class="fa fa-cog"></i> Service Category</a></li>
            <li class="{!! (Request::is('admin/service-list')) ? ' active' : '' !!}"><a href="{{route('admin.service-list.index')}}"><i class="fa fa-cog"></i> Service List</a></li>
            <li class="{!! (Request::is('admin/company-with-tram')) ? ' active' : '' !!}"><a href="{{route('admin.company.index')}}"><i class="fa fa-home"></i> Company With Tram</a></li>

	    </ul>
	  </li>
    <li class="{!! (Request::is('admin/our-client') OR Request::is('admin/our-client/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.client.index')}}"><i class='fa fa-group'></i> <span>Our Clients</span></a>
    </li>
    <li class="{!! (Request::is('admin/career') OR Request::is('admin/career/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.career.index')}}"><i class='fa fa-briefcase'></i> <span>Careers</span></a>
    </li>
    <li class="treeview{!! (Request::is('admin/compliance-and-governance/') OR Request::is('admin/compliance-and-governance/*')) ? ' active' : '' !!}">
	    <a href="#"><i class="fa fa-link"></i> <span>Compliance & Governance</span> <i class="fa fa-chevron-down"></i>
	        <span class="pull-right-container">
	            <i class="fa fa-cog-left pull-right"></i>
	        </span>
	    </a>
	    <ul class="treeview-menu">
	        <li class="{!! (Request::is('admin/compliance-and-governance/cgc')) ? ' active' : '' !!}"><a href="{{route('admin.cgc.index')}}"><i class="fa fa-cog"></i> GCG</a></li>
          <li class="{!! (Request::is('admin/compliance-and-governance/hse')) ? ' active' : '' !!}"><a href="{{route('admin.hse.index')}}"><i class="fa fa-cog"></i> HSE</a></li>
          <li class="{!! (Request::is('admin/compliance-and-governance/charter')) ? ' active' : '' !!}"><a href="{{route('admin.charter.index')}}"><i class="fa fa-cog"></i> Charter Of Commitees</a></li>
          <li class="{!! (Request::is('admin/compliance-and-governance/csr') OR Request::is('admin/compliance-and-governance/csr/*')) ? ' active' : '' !!}"><a href="{{route('admin.csr.index')}}"><i class="fa fa-cog"></i> CSR</a></li>
      </ul>
	  </li>
    <li class="treeview{!! (Request::is('admin/investor-relation') OR Request::is('admin/investor-relation/*')) ? ' active' : '' !!}">
	    <a href="#"><i class="fa fa-link"></i> <span>Investor Relations</span> <i class="fa fa-chevron-down"></i>
	        <span class="pull-right-container">
	            <i class="fa fa-cog-left pull-right"></i>
	        </span>
	    </a>
	    <ul class="treeview-menu">
	        <li class="{!! (Request::is('admin/investor-relation') OR Request::is('admin/investor-relation/create') OR Request::is('admin/investor-relation/edit') ) ? ' active' : '' !!}"><a href="{{route('admin.investor.index')}}"><i class="fa fa-cog"></i> Investor Relation</a></li>
          <li class="{!! (Request::is('admin/investor-relation/shareholders') OR Request::is('admin/investor-relation/shareholders/*')) ? ' active' : '' !!}"><a href="{{route('admin.shareholder.index')}}"><i class="fa fa-cog"></i> Shareholder</a></li>
          <li class="{!! (Request::is('admin/investor-relation/shareholder-compo') OR Request::is('admin/investor-relation/shareholder-compo/*')) ? ' active' : '' !!}"><a href="{{route('admin.shareholder-composition.edit',['id' => 1])}}"><i class="fa fa-cog"></i> Shareholder Composition</a></li>
          <li class="{!! (Request::is('admin/investor-relation/financial-highlights') OR Request::is('admin/investor-relation/financial-highlights/*')) ? ' active' : '' !!}"><a href="{{route('admin.financial.index')}}"><i class="fa fa-cog"></i> Financial Highlights</a></li>
          <li class="{!! (Request::is('admin/investor-relation/annual-reports') OR Request::is('admin/investor-relation/annual-reports/*')) ? ' active' : '' !!}"><a href="{{route('admin.annual.index')}}"><i class="fa fa-cog"></i> Annual Report</a></li>
          <li class="{!! (Request::is('admin/news/corporate-action') OR Request::is('admin/news/corporate-action/*') ) ? ' active' : '' !!}"><a href="{{route('admin.corporate-action.index')}}"><i class="fa fa-cog"></i> Corporate Action</a></li>
            
          {{-- <li class="{!! (Request::is('admin/investor-relation/investor-calendars') OR Request::is('admin/investor-relation/investor-calendars/*')) ? ' active' : '' !!}"><a href="{{route('admin.calendar.index')}}"><i class="fa fa-cog"></i> Investor Calendar</a></li> --}}
      </ul>
	  </li>
    <li class="treeview{!! (Request::is('admin/news') OR Request::is('admin/news/*')) ? ' active' : '' !!}">
	    <a href="#"><i class="fa fa-link"></i> <span>News</span> <i class="fa fa-chevron-down"></i>
	        <span class="pull-right-container">
	            <i class="fa fa-cog-left pull-right"></i>
	        </span>
	    </a>
	    <ul class="treeview-menu">
	        <li class="{!! (Request::is('admin/news/media-clipping') OR Request::is('admin/news/media-clipping/*') ) ? ' active' : '' !!}"><a href="{{route('admin.media-clipping.index')}}"><i class="fa fa-cog"></i> Media Clipping</a></li>
            <li class="{!! (Request::is('admin/news/media-center') OR Request::is('admin/news/media-center/*') ) ? ' active' : '' !!}"><a href="{{route('admin.media-center.index')}}"><i class="fa fa-cog"></i> Media Center</a></li>
      </ul>
	  </li>
	  <li class="{!! (Request::is('admin/file-manager') OR Request::is('admin/file-manager/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.filemanager.index')}}"><i class='fa fa-file'></i> <span>File Manager</span></a>
    </li>
    <li class="{!! (Request::is('admin/contact') OR Request::is('admin/contact/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.contact.edit', ['id' => 1])}}"><i class='fa fa-phone'></i> <span>Contact Us</span></a>
    </li>
    <li class="{!! (Request::is('admin/footer') OR Request::is('admin/footer/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.footer.edit', ['id' => 1])}}"><i class='fa fa-cog'></i> <span>Footer</span></a>
    </li>
    <li class="{!! (Request::is('admin/image-header') OR Request::is('admin/image-header/*')) ? ' active' : '' !!}">
        <a href="{{route('admin.image-header.index', ['id' => 1])}}"><i class='fa fa-image'></i> <span>Image Header Setting</span></a>
    </li>
</ul>
