<!doctype html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta('robots', 'noindex, nofollow') !!}
        {!! Html::meta('product', env('APP_NAME')) !!}
        {!! Html::meta('description', env('APP_NAME')) !!}
        {!! Html::meta('author', 'Bengkel') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}
        <link href="{{ get_file('assets/frontend/img/favicon.png') }}" rel="icon" sizes="32x32" type="image/png">

        <title>{{ env('APP_NAME') }} - @yield('title')</title>

        {!! Html::style('assets/backend/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! Html::style('assets/backend/bower_components/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('assets/backend/bower_components/Ionicons/css/ionicons.min.css') !!}

        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

        @yield('header')

        {!! Html::style('assets/backend/dist/css/AdminLTE.css') !!}
        {!! Html::style('assets/backend/dist/css/skins/_all-skins.min.css') !!}

        {!! Html::style('assets/backend/plugins/datatables/jquery.dataTables.min.css') !!}
        {!! Html::style('assets/backend/plugins/datatables/buttons.dataTables.min.css') !!}

        {!! Html::style('assets/backend/plugins/HoldOn/HoldOn.min.css') !!}
        {!! Html::style('assets/backend/plugins/pace/pace.min.css') !!}
        {!! Html::style('assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
        {!! Html::style('assets/backend/bower_components/morris.js/morris.css') !!}
        {!! Html::style('assets/backend/bower_components/jvectormap/jquery-jvectormap.css') !!}
        {!! Html::style('assets/backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}
        {!! Html::style('assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}
        {!! Html::style('assets/global_plugins/sweetalert/sweetalert.css') !!}

        <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="/admin/dashboard" class="logo">
                  <span class="logo-mini"><b>{{ env('APP_NAME_INITIAL', 'B') }}</b>A</span>
                  <span class="logo-lg"><b>{{ env('APP_NAME') }} </b>Admin</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                  <!-- Sidebar toggle button-->
                  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                  </a>

                  <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                      <!-- Notifications: style can be found in dropdown.less -->
                      <!-- <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-bell-o"></i>
                          <span class="label label-warning">1</span>
                        </a>
                        <ul class="dropdown-menu">
                          <li class="header">You have 1 notifications</li>
                          <li>
                            <ul class="menu">
                              <li>
                                <a href="#">
                                  <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li class="footer"><a href="#">View all</a></li>
                        </ul>
                      </li> -->
                      <!-- User Account: style can be found in dropdown.less -->
                      <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <img src="{{ get_file('images/avatars/b6c22e62ed082bbd114151575f93a96c.jpg') }}" class="user-image" alt="{{ user_info('first_name') }}">
                          <span class="hidden-xs">{{ ucwords(user_info('full_name')) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <img src="{{ get_file('images/avatars/b6c22e62ed082bbd114151575f93a96c.jpg') }}" class="img-circle" alt="{{ user_info('first_name') }}">

                            <p>
                              {{ ucwords(user_info('full_name')) }}
                              <small>{{ ucwords(user_info('role')->name) }}</small>
                            </p>
                          </li>
                          <!-- Menu Footer-->
                          <li class="user-footer">
                            <!-- <div class="pull-left">
                              <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div> -->
                            <div class="col-sm-12">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <div class="pull-right">
                                      <a href="{!! route('logout') !!}" class="btn btn-danger btn-flat">Sign out</a>
                                    </div>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <!-- Control Sidebar Toggle Button -->
                      {{--  <li title="Layout Options">
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                      </li>  --}}
                    </ul>
                  </div>
                </nav>
            </header>


            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <section class="sidebar">
                  <div class="user-panel">
                    <div class="pull-left image">
                      <img src="{{ get_file('images/avatars/b6c22e62ed082bbd114151575f93a96c.jpg') }}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                      <p>{{ ucwords(user_info('full_name')) }}</p>
                      <i class="fa fa-circle text-success"></i> Online
                    </div>
                  </div>
                    @include('backend.layouts.partial.navigation_menu')
                </section>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>@yield('page-header')</h1>

                    @yield('breadcrumb')
                </section>
                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                  <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; {{ date('Y') }} <a href="#">{{ env('APP_NAME', 'FGBMFI') }}</a></strong>. All rights reserved.
            </footer>

            <!-- Control Sidebar -->

          {{--  <div class="control-sidebar-bg"></div>  --}}
        </div>

        {!! Html::script('assets/backend/bower_components/jquery/dist/jquery.min.js') !!}
        {!! Html::script('assets/backend/bower_components/jquery-ui/jquery-ui.min.js') !!}
        {!! Html::script('assets/backend/plugins/datatables/jquery.dataTables.min.js') !!}
        {!! Html::script('assets/backend/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        {!! Html::script('assets/backend/plugins/datatables/dataTables.buttons.min.js') !!}
        <script src="/vendor/datatables/buttons.server-side.js"></script>
        {!! Html::script('assets/backend/plugins/HoldOn/HoldOn.min.js') !!}
        {!! Html::script('assets/backend/plugins/pace/pace.min.js') !!}
        {!! Html::script('assets/global_plugins/sweetalert/sweetalert.min.js') !!}
        {!! Html::script('assets/backend/bower_components/morris.js/morris.min.js') !!}
        {!! Html::script('assets/backend/js/custom-backend.js') !!}
        {!! Html::script('assets/backend/bower_components/moment/min/moment.min.js') !!}
        {!! Html::script('assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}
        {!! Html::script('assets/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}

        <!-- {!! Html::script('assets/backend/bower_components/raphael/raphael.min.js') !!}
        {!! Html::script('assets/backend/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') !!}
        {!! Html::script('assets/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
        {!! Html::script('assets/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
        {!! Html::script('assets/backend/bower_components/jquery-knob/dist/jquery.knob.min.js') !!}
        {!! Html::script('assets/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') !!}
        {!! Html::script('assets/backend/bower_components/fastclick/lib/fastclick.js') !!}
        {!! Html::script('assets/backend/dist/js/pages/dashboard.js') !!} -->

        {!! Html::script('assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
        {{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> --}}
        <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

        {{-- VENDOR JS --}}
       <script src="{{ mix('js/vendor.js') }}"></script>

        {!! Html::script('assets/backend/dist/js/adminlte.min.js') !!}
        {!! Html::script('assets/backend/dist/js/demo.js') !!}

        <script type="text/javascript">
          $.widget.bridge('uibutton', $.ui.button);

          $(document).ajaxStart(function() {
              Pace.restart();
          });

          $(document).ready(function() {

              setTimeout(function() {
                  $(".alert").hide();
              }, 3000);

              $('#nip').change(function() {
                  $('#password').val($(this).val());
              });
          });

          function readURL(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage").change(function(){
              readURL(this);
          });

          function readURL1(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand1').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage1").change(function(){
              readURL1(this);
          });

          function readURL2(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand2').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage2").change(function(){
              readURL2(this);
          });


          function readURL3(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand3').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage3").change(function(){
              readURL3(this);
          });

          function readURL4(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand4').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage4").change(function(){
              readURL4(this);
          });

          function readURL5(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand5').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage5").change(function(){
              readURL5(this);
          });

          function readURL6(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand6').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage6").change(function(){
              readURL6(this);
          });

          function readURL7(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageBrand7').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#brandImage7").change(function(){
              readURL7(this);
          });

          function ktpImage(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageKtp').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#ktpImage").change(function(){
              ktpImage(this);
          });

          function simImage(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $('#imageSim').attr('src', e.target.result);
                  }

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#simImage").change(function(){
              simImage(this);
          });


          // $('.textarea').wysihtml5();

          

        </script>

        @yield('scripts')
    </body>
</html>
