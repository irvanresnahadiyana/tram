<!DOCTYPE html>
<html>
    <head>
        {!! Html::meta(null, null, ['charset' => 'UTF-8']) !!}
        {!! Html::meta('robots', 'noindex, nofollow') !!}
        {!! Html::meta('product', env('APP_NAME', 'FGBMFI')) !!}
        {!! Html::meta('description', env('APP_NAME', 'FGBMFI')) !!}
        {!! Html::meta('author', 'FGBMFI') !!}
        {!! Html::meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no') !!}

        <title>{{ env('APP_NAME') }} - @yield('title')</title>

        {!! Html::style('assets/backend/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! Html::style('assets/backend/bower_components/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('assets/backend/bower_components/Ionicons/css/ionicons.min.css') !!}

        @yield('header')

        {!! Html::style('assets/backend/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('assets/backend/plugins/iCheck/square/blue.css') !!}
    </head>

    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ route('auth-login') }}"><b>{{ env('APP_NAME') }}</b> | <font color="#3c8dbc">Admin</font></a>
            </div>

            <div class="login-box-body">
                @yield('content')
            </div>
        </div>

        {!! Html::script('assets/backend/bower_components/jquery/dist/jquery.min.js') !!}
        {!! Html::script('assets/backend/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        {!! Html::script('assets/backend/plugins/iCheck/icheck.min.js') !!}

        <script>
            $(document).ready(function() {
                $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
                  increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>