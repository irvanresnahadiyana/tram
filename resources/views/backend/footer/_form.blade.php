
<div class="form-group {{ $errors->has('footer_text') ? 'has-error' : ''}}">
    {!! Form::label('footer_text', "Footer Text", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('footer_text', @$data->footer_text , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('footer_text', '<p class="help-block">:message</p>') !!}
    </div>
</div>