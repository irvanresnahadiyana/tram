<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', "Title", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('name', @$data->name , ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Thumbnail Media'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand" src="{{(@$data->image) ? get_file($data->image, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image" id="brandImage"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', "Type Media", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        <select class="form-control" name="type">
            <option value="IMAGE">Image</option>
            <option value="PDF">PDF</option>
            <option value="VIDEO">Video</option>
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">
    {!! Form::label('file', trans('File / PDF'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
          <input type="file" accept="pdf" name="file" id="brandImage"  value="{{@$data->file}}"> {{ @$data->file }}
    </div>
</div>
@if(@$data->type == "IMAGE")
  <img style="margin-left:239px" src="{{ get_file(@$data->file)}}" class="img-responsive" width="500px">
@elseif (@$data->type == "PDF")
  <a style="margin-left:239px" target="_blank" href="{{ get_file(@$data->file)}}"> SEE PDF </a><br>
@elseif (@$data->type == "VIDEO")
  <video width="400" style="margin-left:239px" controls>
  <source src="{{ get_file(@$data->file) }}" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
@endif
<br>
