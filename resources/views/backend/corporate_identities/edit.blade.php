@extends('backend.layouts.master.master')

@section('title')
    Corporate Identity - {{ trans('general.edit') }}
@endsection

@section('page-header')
    Corporate Identity <small>{{trans('general.edit')}}</small>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li><a href="{!! route('admin.corporate-identity.index') !!}">Corporate Identity {{ trans('general.management') }}</a></li>
        <li class="active">{{ trans('general.edit') }}</li>
    </ol>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            {!! Form::model($data,['route' =>['admin.corporate-identity.update',$data->id],'class' =>'form-horizontal','id' => 'form-user','method' => 'PATCH', 'enctype' => 'multipart/form-data',]) !!}
            @include('backend.corporate_identities._form')
            <div class="row">
                <div class="col-xs-12 m-top-10 text-right">
                    <a href="{!! route('admin.corporate-identity.index') !!}" class="btn btn-danger">Cancel</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
@endsection
<style>
.wysihtml5-sandbox{
  width : 1000px !important;
  height: 300px !important;
}
</style>
@section('scripts')
  <script>
  $(document).ready(function() {
      // $('#about_us').summernote({
      //   tabsize: 2,
      //   height: 200
      // });
      //
      // $('#address').summernote({
      //   tabsize: 2,
      //   height: 200
      // });
      //
      // $('#legal_of_bussiness1').summernote({
      //   tabsize: 2,
      //   height: 200
      // });
      $('.wysihtml5-sandbox').css("resize", "both");
      $('.textarea').wysihtml5();
  });
  </script>

@endsection
