{{-- <div class="container"> --}}
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#about">About Us</a></li>
    <li><a data-toggle="tab" href="#addresses">Address</a></li>
    <li><a data-toggle="tab" href="#compliance_governances">Compliance & Governance</a></li>
    <li><a data-toggle="tab" href="#legal_of_bussiness1">Line Of Bussiness</a></li>
    <li><a data-toggle="tab" href="#estabilishment">Estabilishment</a></li>
    <li><a data-toggle="tab" href="#legal_basis_estabilishment">Legal Basis Establishment</a></li>
    <li><a data-toggle="tab" href="#visi">Visi</a></li>
    <li><a data-toggle="tab" href="#misi">Misi</a></li>
    <li><a data-toggle="tab" href="#service_description">Service Description</a></li>
    <li><a data-toggle="tab" href="#values">Values</a></li>
    <li><a data-toggle="tab" href="#group-companies">Group Of Companies</a></li>
  </ul>

  <div class="tab-content col-sm-12">
    <div id="about" class="tab-pane fade in active">
      <br>
      <div class="form-group {{ $errors->has('about_us') ? 'has-error' : ''}}">
          {!! Form::label('about_us', "About Us ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('about_us', old('about_us') , ['class' => 'form-control textarea', 'id' => 'about_us']) !!}
              {!! $errors->first('about_us', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('about_us') ? 'has-error' : ''}}">
          {!! Form::label('about_us', "About Us ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('about_us_id', old('about_us_id') , ['class' => 'form-control textarea', 'id' => 'about_us']) !!}
              {!! $errors->first('about_us_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="compliance_governances" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('compliance_governance') ? 'has-error' : ''}}">
          {!! Form::label('compliance_governance', "Compliance & Governance ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('compliance_governance', old('compliance_governance') , ['class' => 'form-control textarea', 'id' => 'compliance_governance']) !!}
              {!! $errors->first('compliance_governance', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('compliance_governance_id') ? 'has-error' : ''}}">
          {!! Form::label('compliance_governance_id', "Compliance & Governance ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('compliance_governance_id', old('compliance_governance_id') , ['class' => 'form-control textarea', 'id' => 'compliance_governance']) !!}
              {!! $errors->first('compliance_governance_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="addresses" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
          {!! Form::label('address', "Address ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('address', old('address') , ['class' => 'form-control textarea', 'id' => 'address']) !!}
              {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="legal_of_bussiness1" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('line_of_bussiness') ? 'has-error' : ''}}">
          {!! Form::label('line_of_bussiness', "Line Of Bussiness ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('line_of_bussiness', old('line_of_bussiness') , ['class' => 'form-control textarea', 'id' => 'line_of_bussiness']) !!}
              {!! $errors->first('line_of_bussiness', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('line_of_bussiness_id') ? 'has-error' : ''}}">
          {!! Form::label('line_of_bussiness_id', "Line Of Bussiness ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('line_of_bussiness_id', old('line_of_bussiness_id') , ['class' => 'form-control textarea', 'id' => 'line_of_bussiness']) !!}
              {!! $errors->first('line_of_bussiness_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="legal_basis_estabilishment" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('legal_basis_estabilishment') ? 'has-error' : ''}}">
          {!! Form::label('legal_basis_estabilishment', "Legal Basis Establishment ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('legal_basis_estabilishment', old('legal_basis_estabilishment') , ['class' => 'form-control textarea', 'id' => 'legal_basis_estabilishment']) !!}
              {!! $errors->first('legal_basis_estabilishment', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('legal_basis_estabilishment_id') ? 'has-error' : ''}}">
          {!! Form::label('legal_basis_estabilishment_id', "Legal Basis Establishment ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('legal_basis_estabilishment_id', old('legal_basis_estabilishment_id') , ['class' => 'form-control textarea', 'id' => 'legal_basis_estabilishment']) !!}
              {!! $errors->first('legal_basis_estabilishment_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="visi" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('visi') ? 'has-error' : ''}}">
          {!! Form::label('visi', "Visi ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('visi', old('visi') , ['class' => 'form-control textarea', 'id' => 'visi']) !!}
              {!! $errors->first('visi', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('visi_id') ? 'has-error' : ''}}">
          {!! Form::label('visi_id', "Visi ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('visi_id', old('visi_id') , ['class' => 'form-control textarea', 'id' => 'visi']) !!}
              {!! $errors->first('visi_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="misi" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('misi') ? 'has-error' : ''}}">
          {!! Form::label('misi', "Misi ( ENG )", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('misi', old('misi') , ['class' => 'form-control textarea', 'id' => 'misi']) !!}
              {!! $errors->first('misi', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <div class="form-group {{ $errors->has('misi_id') ? 'has-error' : ''}}">
          {!! Form::label('misi_id', "Misi ( IDN )", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('misi_id', old('misi_id') , ['class' => 'form-control textarea', 'id' => 'misi']) !!}
              {!! $errors->first('misi_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="service_description" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('service_description') ? 'has-error' : ''}}">
          {!! Form::label('service_description', "Service Description ( ENG )", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('service_description', old('service_description') , ['class' => 'form-control textarea', 'id' => 'service_description']) !!}
              {!! $errors->first('service_description', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('service_description_id') ? 'has-error' : ''}}">
          {!! Form::label('service_description_id', "Service Description ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('service_description_id', old('service_description_id') , ['class' => 'form-control textarea', 'id' => 'service_description']) !!}
              {!! $errors->first('service_description_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="values" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('values') ? 'has-error' : ''}}">
          {!! Form::label('values', "Values ( ENG ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('values', old('values') , ['class' => 'form-control textarea', 'id' => 'values']) !!}
              {!! $errors->first('values', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
      <hr>
      <div class="form-group {{ $errors->has('values_id') ? 'has-error' : ''}}">
          {!! Form::label('values_id', "Values ( IDN ) ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::textarea('values_id', old('values_id') , ['class' => 'form-control textarea', 'id' => 'values']) !!}
              {!! $errors->first('values_id', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="estabilishment" class="tab-pane fade">
      <br>
      <div class="form-group {{ $errors->has('estabilishment') ? 'has-error' : ''}}">
          {!! Form::label('estabilishment', "Estabilishment ", ['class' => 'col-sm-2 control-label']) !!}
          <div class="col-sm-5">
              {!! Form::text('estabilishment', old('estabilishment') , ['class' => 'form-control', 'id' => 'estabilishment', 'placeholder' => 'Ex: 7 Mei 1990']) !!}
              {!! $errors->first('estabilishment', '<p class="help-block">:message</p>') !!}
          </div>
      </div>
    </div>
    <div id="group-companies" class="tab-pane fade">
        <br>
        <div class="form-group {{ $errors->has('group_of_companies') ? 'has-error' : ''}}">
            {!! Form::label('group_of_companies', trans('Group Of Companies Image'), ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-6">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
                    <img id="imageBrand" src="{{(@$data->group_of_companies) ? get_file($data->group_of_companies, 'thumbnail') : url('images/noimagefound.jpg')}}">
                </div>
                <div>
                    <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Choose Image</span>
                        <input type="file" accept="image/jpg, image/png" name="group_of_companies" id="brandImage"  value="{{@$data->group_of_companies}}">
                    </span>
                </div>
            </div>
            </div>
        </div>
    </div>
  </div>
{{-- </div> --}}
