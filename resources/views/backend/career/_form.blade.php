<div class="form-group {{ $errors->has('career_list') ? 'has-error' : ''}}">
    {!! Form::label('career_list', "Career Listings ", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('career_list', old('career_list') , ['class' => 'form-control textarea', 'id' => 'career_list']) !!}
        {!! $errors->first('career_list', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<br>
<hr>
<div class="form-group {{ $errors->has('career_overview') ? 'has-error' : ''}}">
    {!! Form::label('career_overview', "Career Overview ", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('career_overview', old('career_overview') , ['class' => 'form-control textarea', 'id' => 'career_overview']) !!}
        {!! $errors->first('career_overview', '<p class="help-block">:message</p>') !!}
    </div>
</div>
