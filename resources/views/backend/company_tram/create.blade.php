@extends('backend.layouts.master.master')

@section('title')
    Company With Tram - {{ trans('general.create_new') }}
@endsection

@section('page-header')
Company With Tram
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li class="active">{{ trans('general.create_new') }} Company With Tram List</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.company.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
            'enctype' => 'multipart/form-data',
        ]) !!}
        <div class="box box-primary" style="margin-top: 20px" >
            @if(Session::has('success'))
                <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            {{-- <div class="box-header with-border">
               <h3 class="box-title">Ajukian Cuti Baru</h3>
            </div> --}}
            <div class="box-body">
                @include('backend.company_tram._form')
                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <a href="{!! route('admin-dashboard') !!}" class="btn btn-danger">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
