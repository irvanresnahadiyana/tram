@extends('backend.layouts.master.master')

@section('title', 'Dashboard')

@section('page-header', 'Dashboard')

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-tachometer"></i> Home</a></li>
    <li class="active">Dashboard</li>
</ol>
@endsection

@section('content')
<div class="row">
  <div class="container">
    <div class="centered text-center" style="margin-top: 20%">
      <h2 style="font-weight: 500"> SELAMAT DATANG DI SISTEM PENGAJUAN CUTI ONLINE </h2><br>
      <div class="col-md-6 col-xs-10">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{ $data->hak_cuti_tahunan }}</h3>

            <p>Sisa Cuti</p>
          </div>
          <div class="icon">
            {{-- <i class="ion ion-bag"></i> --}}
          </div>
          <a href="{{ route('employe.permohonan-cuti.index')}} " class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-md-6 col-xs-10">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{ $approved }} </h3>

            <p>Approved</p>
          </div>
          <div class="icon">
            {{-- <i class="ion ion-stats-bars"></i> --}}
          </div>
          <a href="{{ route('employe.permohonan-cuti.index')}} " class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@include('backend.dashboards.scripts.index_script')
