@extends('backend.layouts.master.master')


@section('title', 'Dashboard')

{{-- @section('page-header', 'Dashboard') --}}

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-tachometer"></i> Home</a></li>
    <li class="active">Dashboard</li>
</ol>
@endsection

@section('content')
<div class="row">
  <div class="container">
    <div class="centered text-center" style="margin-top: 20%">
      <h2 style="font-weight: 500"> WELCOME TO TRAM MANAGEMENT </h2><br>
    </div>
  </div>
</div>
@endsection

@include('backend.dashboards.scripts.index_script')
