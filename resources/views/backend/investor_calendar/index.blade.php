@extends('backend.layouts.master.master')

@section('title')
    Investor Calendars - {{ trans('general.management') }}
@endsection

@section('page-header')
    Investor Calendar {{ trans('general.management') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <a href="{!! route('admin.calendar.create') !!}"><button type="button" class="btn btn-primary">+ Add Investor Calendars</button></a>
    </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12 col-xs-6">
    <div class="row pull-right">
      <div class="col-lg-12 col-xs-6 ">
        {{--  <a href="{{ route('suppliers.create')}}" class="btn btn-primary"> Add New </a>   --}}
      </div>
    </div>
    <div class="row" >
      <div class="col-lg-12 col-xs-6">
          <div class="box box-primary " style="margin-top: 20px">
              <div class="box-header">
                @if(Session::has('success'))
                    <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
                @endif
              </div>
              <div class="box-body">
                  {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
          </div>
      </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@include('backend.investor_calendar.scripts.index_script')
