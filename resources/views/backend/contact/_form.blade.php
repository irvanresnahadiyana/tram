<div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
    {!! Form::label('company_name', "Company Name", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('company_name', @$data->company_name , ['class' => 'form-control']) !!}
        {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('website_name') ? 'has-error' : ''}}">
    {!! Form::label('website_name', "Website Name", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('website_name', @$data->website_name , ['class' => 'form-control']) !!}
        {!! $errors->first('website_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('tricker_code') ? 'has-error' : ''}}">
    {!! Form::label('tricker_code', "Tricker Code", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('tricker_code', @$data->tricker_code , ['class' => 'form-control']) !!}
        {!! $errors->first('tricker_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('representative_office') ? 'has-error' : ''}}">
    {!! Form::label('representative_office', "Representative Office Name", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('representative_office', @$data->representative_office , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('representative_office', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('company_address') ? 'has-error' : ''}}">
    {!! Form::label('company_address', "Company Address", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('company_address', @$data->company_address , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('company_address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('media_contact') ? 'has-error' : ''}}">
    {!! Form::label('media_contact', "Media Contact", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('media_contact', @$data->media_contact , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('media_contact', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('corporate_secertary') ? 'has-error' : ''}}">
    {!! Form::label('corporate_secertary', "Corporate Secertary", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('corporate_secertary', @$data->corporate_secertary , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('corporate_secertary', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('stock_exchange') ? 'has-error' : ''}}">
    {!! Form::label('stock_exchange', "Stock Exchange", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('stock_exchange', @$data->stock_exchange , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('stock_exchange', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('share_register') ? 'has-error' : ''}}">
    {!! Form::label('share_register', "Share Register", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('share_register', @$data->share_register , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('share_register', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('public_accountant') ? 'has-error' : ''}}">
    {!! Form::label('public_accountant', "Public Accountant", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('public_accountant', @$data->public_accountant , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('public_accountant', '<p class="help-block">:message</p>') !!}
    </div>
</div>