@section('scripts')

{!! $dataTable->scripts() !!}
<script>
    $(document).ready(function() {
        $(document).on('click', '#deleteData', function(e) {
            $('#submit').attr('disabld', true);
            var url = $(this).attr('data-href');
            $('#destroy').attr('action', url );
            $('#import').attr( 'method', 'delete' );
            $('#delete-modal').modal('show');
            $("#text-body-confirm").text("{{ trans('label.delete_confirmation') }}");
            $('#submit').attr('value','Delete');
            e.preventDefault();
        });
    });
    // $('#submit').click(function () {
    //     modal_loader();
    // });
</script>

@endsection
