<div class="form-group {{ $errors->has('image_home') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Home'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand" src="{{(@$data->image_home) ? get_file($data->image_home, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_home" id="brandImage"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_about') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image About'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand1" src="{{(@$data->image_about) ? get_file($data->image_about, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_about" id="brandImage1"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_service') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Service'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand2" src="{{(@$data->image_service) ? get_file($data->image_service, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_service" id="brandImage2"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_compliance') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Compliance'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand3" src="{{(@$data->image_compliance) ? get_file($data->image_compliance, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_compliance" id="brandImage3"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_investor_relation') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Investor Relation'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand4" src="{{(@$data->image_investor_relation) ? get_file($data->image_investor_relation, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_investor_relation" id="brandImage4"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_news') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image News'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand5" src="{{(@$data->image_news) ? get_file($data->image_news, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_news" id="brandImage5"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_career') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Career'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand6" src="{{(@$data->image_career) ? get_file($data->image_career, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_career" id="brandImage6"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('image_contact') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image Contact'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand7" src="{{(@$data->image_contact) ? get_file($data->image_contact, 'thumbnail') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image_contact" id="brandImage7"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
