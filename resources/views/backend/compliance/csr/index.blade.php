@extends('backend.layouts.master.master')

@section('title')
    CSR {{ trans('general.management') }}
@endsection

@section('page-header')
    CSR {{ trans('general.management') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        {{-- <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-map"></i> Home</a></li>
        <li class="active">{{ trans('general.list') }} {{ trans('general.users') }}</li> --}}

    </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12 col-xs-6">
    <div class="row pull-right">
      <div class="col-lg-12 col-xs-6 ">
        {{--  <a href="{{ route('suppliers.create')}}" class="btn btn-primary"> Add New </a>   --}}
      </div>
    </div>
    <div class="row" >
      <div class="col-lg-12 col-xs-6">
        {!! Form::open([
                'route' =>  'create-csr',
                'class' =>  'form-horizontal',
                'id'    =>  'form-user',
                'enctype' => 'multipart/form-data',
            ]) !!}
            <div class="box box-primary" style="margin-top: 20px" >
                @if(Session::has('success_csr'))
                    <div class="alert alert-success"><em> {!! session('success_csr') !!}</em></div>
                @endif
                @if(Session::has('error_csr'))
                    <div class="alert alert-error"><em> {!! session('error_csr') !!}</em></div>
                @endif
                {{-- <div class="box-header with-border">
                   <h3 class="box-title">Ajukian Cuti Baru</h3>
                </div> --}}
                <div class="box-body">
                    @include('backend.compliance.csr._form_desc')
                    <div class="form-group pull-right">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        <h3> CSR LIST </h3>
          <div class="box box-primary " style="margin-top: 20px">
              <div class="box-header">
                <a href="{!! route('admin.csr.create') !!}"><button type="button" class="btn btn-primary pull-right">+ Add CSR</button></a>
                @if(Session::has('success'))
                    <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
                @endif
              </div>
              <div class="box-body">
                  {!! $dataTable->table(['class' => 'datatable table table-striped', 'cellspacing'=>"0", 'width'=>"100%"]) !!}
              </div>
          </div>
      </div>
    </div>
</div>
@include('partials.delete-modal')
@endsection
@include('backend.compliance.csr.scripts.index_script')
