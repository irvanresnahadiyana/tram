<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', "Name", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('name', @$data->name , ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', "Description ( ENG )", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('description', @$data->description , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description_id') ? 'has-error' : ''}}">
    {!! Form::label('description_id', "Description ( IDN )", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::textarea('description_id', @$data->description_id , ['class' => 'form-control textarea']) !!}
        {!! $errors->first('description_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('File / PDF'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
          <input type="file" accept="pdf" name="file" id="brandImage"  value="{{@$data->file}}">
          File : {{ get_file(@$data->file)}}
    </div>
</div>
