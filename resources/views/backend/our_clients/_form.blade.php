<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', "Name", ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::text('name', old('name') , ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('Image / Icon'), ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
      <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: auto;">
              <img id="imageBrand" src="{{(@$data->image) ? get_file($data->image, 'original') : url('images/noimagefound.jpg')}}">
          </div>
          <div>
              <span class="btn btn-default btn-file">
                  <span class="fileinput-new">Choose Image</span>
                  <input type="file" accept="image/jpg, image/png" name="image" id="brandImage"  value="{{@$data->image}}">
              </span>
          </div>
      </div>
    </div>
</div>
