@extends('backend.layouts.master.master')

@section('title')
    Our Client - {{ trans('general.create_new') }}
@endsection

@section('page-header')
    Our Client
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('admin-dashboard') !!}"><i class="fa fa-hashtag"></i> Home</a></li>
        <li class="active">{{ trans('general.create_new') }} Our Client</li>
    </ol>
@endsection

@section('content')
    {!! Form::open([
            'route' =>  'admin.client.store',
            'class' =>  'form-horizontal',
            'id'    =>  'form-user',
            'enctype' => 'multipart/form-data',
        ]) !!}
        <div class="box box-primary" style="margin-top: 20px" >
            @if(Session::has('success'))
                <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-error"><em> {!! session('error') !!}</em></div>
            @endif
            {{-- <div class="box-header with-border">
               <h3 class="box-title">Ajukian Cuti Baru</h3>
            </div> --}}
            <div class="box-body">
                @include('backend.our_clients._form')
                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <a href="{!! route('admin-dashboard') !!}" class="btn btn-danger">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('scripts')
  <script>
  $(document).ready(function() {
      // $('#about_us').summernote({
      //   tabsize: 2,
      //   height: 200
      // });
      //
      // $('#address').summernote({
      //   tabsize: 2,
      //   height: 200
      // });
      //
      // $('#legal_of_bussiness1').summernote({
      //   tabsize: 2,
      //   height: 200
      // });

      $('.textarea').wysihtml5();
  });
  </script>

@endsection
