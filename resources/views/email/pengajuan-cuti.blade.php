<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{{ $title }}</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;">
  <p style="margin-top: 0px; margin-bottom: 20px;">Hi, Ada karyawan yang mengajukan cuti dengan data sebagai berikut :</p>
  <p style="margin-top: 0px; margin-bottom: 10px;">Nama : {{ $nama }}</p>
  <p style="margin-top: 0px; margin-bottom: 10px;">NIP : {{ $nip }}</p>
  <p style="margin-top: 0px; margin-bottom: 10px;">Mulai Cuti : {{ $mulai_cuti }} Sampai Dengan : {{ $akhir_cuti }}</p>
  <p style="margin-top: 0px; margin-bottom: 10px;">Jenis Cuti : {{ $jenis_cuti }}</p>
  <p style="margin-top: 0px; margin-bottom: 10px;">Alasan Cuti : {{ $alasan }}</p>
  <p style="margin-top: 0px; margin-bottom: 10px;"><a href="{{ $url }}">Approve Cuti ?</a></p>
</div>
</body>
</html>
