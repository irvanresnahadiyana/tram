<?php

use Step\Acceptance\Admin as AdminTester;

class ProductCategoryCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToCreateCategory(AdminTester $I)
    {
        $I->loginAsAdmin();
        $I->amOnPage('admin/management/categories/create');
        $I->fillField('name', 'Ceiling/Plafon');
        $I->fillField('slug', 'ceiling-plafon');
        $I->fillField('description');
        $I->attachFile('image', 'category-image.jpg');
        $I->click('Save');
        $I->see('Category created successfully!');
    }
}
