<?php
namespace Step\Acceptance;

class Admin extends \AcceptanceTester
{

    public function loginAsAdmin()
    {
        $I = $this;
        $I->amOnPage('/');
        $I->fillField('email', 'admin@wavin.com');
        $I->fillField('password', '12345678');
        $I->click('Sign In');
    }

}