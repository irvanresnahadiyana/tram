<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuperAdminInvitation extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->view('email.approval-cuti')
      ->subject('Approval Cuti')
      ->with([
          'nama' => $this->data['nama'],
          'mulai_cuti' => $this->data['mulai_cuti'],
          'akhir_cuti' => $this->data['akhir_cuti'],
          'title' => "Approval Cuti"
          ]);
    }
}
