<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgentInvitation extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->view('email.pengajuan-cuti')
      ->subject('Pengajuan Cuti Karyawan')
      ->with([
          'nama' => $this->data['nama'],
          'nip' => $this->data['nip'],
          'mulai_cuti' => $this->data['mulai_cuti'],
          'akhir_cuti' => $this->data['akhir_cuti'],
          'jenis_cuti' => $this->data['jenis_cuti'],
          'alasan' => $this->data['alasan'],
          'url' => $this->data['url'],
          'title' => "Pengajuan Cuti"
          ]);
    }
}
