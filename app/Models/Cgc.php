<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cgc extends Model
{
  protected $table = 'cgc';

  protected $guarded = ['id'];
}
