<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialHighlight extends Model
{
  protected $table = 'financial_highlights';

  protected $guarded = ['id'];
}
