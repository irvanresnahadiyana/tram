<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaCenter extends Model
{
  protected $table = 'media_center';

  protected $guarded = ['id'];
}
