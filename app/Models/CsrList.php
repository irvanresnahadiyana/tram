<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CsrList extends Model
{
  protected $table = 'csr_lists';

  protected $guarded = ['id'];
}
