<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Model
{
		use SoftDeletes;
		protected $dates = ['deleted_at'];
    const DEFAULT_PASSWORD = '12345678';

    protected $fillable = ['role_id', 'email', 'password', 'is_admin', 'address', 'no_ktp', 'phone_number'];

    protected $table = 'users';


}
