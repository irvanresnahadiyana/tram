<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestorRelation extends Model
{
  protected $table = 'investor_relations';

  protected $guarded = ['id'];
}
