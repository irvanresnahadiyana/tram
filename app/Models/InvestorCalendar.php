<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestorCalendar extends Model
{
  protected $table = 'investor_calendars';

  protected $guarded = ['id'];
}
