<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateAction extends Model
{
  protected $table = 'corporate_actions';

  protected $guarded = ['id'];
}
