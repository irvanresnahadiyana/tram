<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnnualReport extends Model
{
  protected $table = 'annual_reports';

  protected $guarded = ['id'];
}
