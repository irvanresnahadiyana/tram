<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageHeader extends Model
{
    protected $table = 'header_images';

    protected $guarded = ['id']; 
}
