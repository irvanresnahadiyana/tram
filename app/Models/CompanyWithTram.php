<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyWithTram extends Model
{
    protected $table = 'company_with_tram';
    public $timestamps = false;
    protected $guarded = ['id'];
}
