<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charter extends Model
{
  protected $table = 'charter_of_commitees';

  protected $guarded = ['id'];
}
