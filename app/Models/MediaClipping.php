<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaClipping extends Model
{
  protected $table = 'media_clippings';

  protected $guarded = ['id'];
}
