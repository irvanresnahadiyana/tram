<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareholderCompo extends Model
{
    protected $table = 'shareholder_compositions';

  	protected $guarded = ['id'];
}
