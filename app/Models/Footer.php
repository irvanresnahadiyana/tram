<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table = 'footers';
    public $timestamps = false;
  	protected $guarded = ['id'];
}
