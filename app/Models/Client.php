<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  protected $table = 'our_clients';

  protected $guarded = ['id'];
}
