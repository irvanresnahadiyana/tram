<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shareholder extends Model
{
  protected $table = 'shareholders';

  protected $guarded = ['id'];
}
