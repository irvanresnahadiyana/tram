<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateIdentity extends Model
{
    protected $table = 'corporate_identities';

    protected $guarded = ['id'];
}
