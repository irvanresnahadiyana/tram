<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Agent;
use Sentinel;
use Mail;
use App\Mail\SuperAdminInvitation;
use App\Mail\AgentInvitation;

class UserRepository
{

    //------------ ADMINISTRATOR USER --------------//

    /**
     * [Insert new user]
     * @param  [type] $data [array]
     * @return [type]       [obj/bool]
     */
    public function createNew($data)
    {
        $user = Sentinel::registerAndActivate($data);

        $role = Sentinel::findRoleBySlug('super-admin');
        $role->users()->attach($user);

        //insert another data to user
        $userUpdate = User::find($user->id);
        $userUpdate->update([
                'phone_number' => $data['phone_number'],
                'no_ktp' => $data['no_ktp'],
                'address' => $data['address'],
                'role_id' => 1,
                'is_admin' => True,
        ]);

        // SEND EMAIL
        $data = [
            'email' => $userUpdate->email,
            'fullname' => $userUpdate->first_name.' '.$userUpdate->last_name,
            'url' => route('set-password', ["id" => $userUpdate->id])
        ];

        Mail::to($userUpdate->email)->send(new SuperAdminInvitation($data));

        return $user;
    }

    /**
     * [Update data user]
     * @param  [type] $data [array]
     * @param  [type] $id   [int]
     * @return [type]       [obj/bool]
     */
    public function updateData($data, $id)
    {
        $findData =  Sentinel::findById($id);
        if (!empty($findData)) {

            $findData->email = $data['email'];
            $findData->first_name = $data['first_name'];
            $findData->last_name = $data['last_name'];
            $findData->phone_number = $data['phone_number'];
            $findData->address = $data['address'];


            if ($findData->save()) {
                return $findData;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


     //------------ AGENT USER --------------//

    /**
     * [Insert new user]
     * @param  [type] $data [array]
     * @return [type]       [obj/bool]
     */
    public function createNewAgent($data)
    {
        $agent = Agent::create($data);

        $user = Sentinel::registerAndActivate($data);
        $role = Sentinel::findRoleBySlug('agent');
        $role->users()->attach($user);

        //insert another data to user
        $userUpdate = User::find($user->id);
        $userUpdate->update(['phone_number' => $data['phone_number'], 'role_id' => 2, 'is_admin' => false, ]);

        // SEND EMAIL
        $data = [
            'email' => $userUpdate->email,
            'fullname' => $data['name'],
            'url' => route('set-password', ["id" => $userUpdate->id])
        ];

        Mail::to($userUpdate->email)->send(new AgentInvitation($data));

        return $user;
    }

    /**
     * [Update data user]
     * @param  [type] $data [array]
     * @param  [type] $id   [int]
     * @return [type]       [obj/bool]
     */
    public function updateDataAgent($data, $id)
    {
        $findData =  Agent::findOrFail($id);
        if (!empty($findData)) {
            return $findData->update($data);
        } else {
            return false;
        }
    }

    //------------ RETAIL USER --------------//

   /**
    * [Insert new user]
    * @param  [type] $data [array]
    * @return [type]       [obj/bool]
    */
   public function createNewRetail($data)
   {
       $user = Sentinel::registerAndActivate($data);

       $role = Sentinel::findRoleBySlug('retail');
       $role->users()->attach($user);

       //insert another data to user
       $userUpdate = User::find($user->id);
       $userUpdate->update(['phone_number' => $data['phone_number'], 'role_id' => 3, 'is_admin' => false, ]);

       return $user;
   }

   /**
    * [Update data user]
    * @param  [type] $data [array]
    * @param  [type] $id   [int]
    * @return [type]       [obj/bool]
    */
   public function updateDataRetail($data, $id)
   {
       $findData =  Sentinel::findById($id);
       if (!empty($findData)) {

           $findData->email = $data['email'];
           $findData->first_name = $data['first_name'];
           $findData->last_name = $data['last_name'];
           $findData->phone_number = $data['phone_number'];


           if ($findData->save()) {
               return $findData;
           } else {
               return false;
           }
       } else {
           return false;
       }
   }

    /**
     * [Get user data]
     * @param  [type] $id [int]
     * @return [type]     [obj]
     */
    public function getUserById($id)
    {
        return User::findOrFail($id);
    }

    public function getAgentById($id)
    {
        return Agent::findOrFail($id);
    }

    /**
     * [Soft delete data user]
     * @param  [type] $id [int]
     * @return [type]     [bool]
     */
    public function deleteData($id)
    {
        $findData = User::find($id);

        if ($findData) {
            if ($findData->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function deleteDataAgent($id)
    {
        $findData = Agent::find($id);
        $findDataUser = User::where('email', $findData->email)->first();

        if ($findData) {
            if ($findData->delete() && $findDataUser->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


}
