<?php

namespace App\Repositories;

use App\Models\Pegawai;
use App\Models\PermohonanCuti;
use App\Models\User;
use Sentinel;
use Mail;
use App\Mail\AgentInvitation;

class PegawaiRepository
{

    public function createNewPegawai($data)
    {
        // CREATE PEGAWAI
        $pegawai = Pegawai::create($data);

        // CREATE USER LOGIN
        $credentials = [
            'email'    => $data['email_kantor'],
            'password' => $data['nip'],
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug('employe');
        $role->users()->attach($user);

        //insert another data to user
        $userUpdate = User::find($user->id);
        $userUpdate->update(['phone_number' => $data['no_telpon'], 'role_id' => 2, 'is_admin' => false, ]);

        // SEND EMAIL
        // $data = [
        //     'email' => $userUpdate->email,
        //     'fullname' => $data['name'],
        //     'url' => route('set-password', ["id" => $userUpdate->id])
        // ];

        // Mail::to($userUpdate->email)->send(new SalesmanInvitation($data));

        return $pegawai;
    }

    public function updatePegawai($data, $id)
    {
        $findData =  Pegawai::findOrFail($id);
        if (!empty($findData)) {
            return $findData->update($data);
        } else {
            return false;
        }
    }

    public function getDataById($id)
    {
        return Pegawai::find($id);
    }

    public function deleteDataPegawai($id)
    {
        $findData = Pegawai::find($id);

        if ($findData) {
            if ($findData->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function deleteDataCuti($id)
    {
        $findData = PermohonanCuti::find($id);

        if ($findData) {
            if ($findData->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function permohonanCuti($data)
    {
        $email = user_info('email');
        $nip = Pegawai::where('email_kantor', $email)->first();
        $no_cuti = PermohonanCuti::count();
        $no = 1 + $no_cuti;
        $cuti = new PermohonanCuti;
        $cuti->no_cuti = "CT-00".$no;
        $cuti->nip = $nip->nip;
        $cuti->tanggal_pengajuan = date("Y-m-d");
        $cuti->mulai_cuti = $data['mulai_cuti'];
        $cuti->akhir_cuti = $data['akhir_cuti'];
        $cuti->jenis_cuti = $data['jenis_cuti'];
        $cuti->alasan_cuti = $data['alasan_cuti'];
        $cuti->status_persetujuan = "pending";
        $cuti->save();

        // SEND EMAIL
        $data = [
            'nama' => $nip->nama,
            'nip' => $cuti->nip,
            'mulai_cuti' => $cuti->mulai_cuti,
            'akhir_cuti' => $cuti->akhir_cuti,
            'alasan' => $cuti->alasan_cuti,
            'jenis_cuti' => $cuti->jenis_cuti,
            'url' => route('admin.approval.approval')
        ];

        Mail::to("hr-rw@mailinator.com")->send(new AgentInvitation($data));

        return true;
    }

}
