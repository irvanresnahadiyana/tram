<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ApiService
{
	private $guzzle;

	public function __construct()
	{
		$this->guzzle = new Client();
	}

	public function getUser($id)
	{
		return $this->requestApi('GET', 'users/' . $id);
	}

	public function getPost($id)
	{
		return $this->requestApi('GET', 'posts/' . $id);
	}

	public function getNews()
	{
		return $this->requestApi('GET','posts/');
	}
	private function requestApi($method, $endPoint)
	{
		$response = $this->guzzle
			->request($method, env('API_URL', '') . $endPoint);

		$data = $response->getBody();
		
		return json_decode($data->getContents());
	}
}