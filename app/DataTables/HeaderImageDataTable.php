<?php

namespace App\DataTables;

use App\Models\ImageHeader;
use Yajra\DataTables\Services\DataTable;

class HeaderImageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($data){
                $edit_url = route('admin.image-header.edit', $data->id);
                // $delete_url = route('admin.calendar.destroy', $data->id);
                return view('partials.action-button')->with(compact('edit_url'));
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ImageHeader $model)
    {
        $query = ImageHeader::all();

         return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
      {
          return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    // ->addAction(['width' => '80px'])
                    // ->parameters($this->getBuilderParameters());
                    ->parameters($this->getParams());
      }

      public function getParams() {
          return [
              'dom' => 'lfrtip',
              'buttons' => ['create'],
              'stateSave' => true,
              'serverSide' => true,
              'processing' => true,
              'responsive' => true
          ];
      }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
      return [
          // 'id',
          'image_home',
          'image_about',
          'image_service',
        //   'image_compliance',
        //   'image_investor_relation',
        //   'image_news',
        //   'image_career',
        //   'image_contact'
      ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'HeaderImage_' . date('YmdHis');
    }
}
