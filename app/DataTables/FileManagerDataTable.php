<?php

namespace App\DataTables;

use App\Models\FileManager;
use Yajra\DataTables\Services\DataTable;

class FileManagerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('image', function($charter) {
                 if ($charter->image) {
                     return '<img src="'.get_file($charter->image).'" width=200px>';
                 }

                 return 'In-Active';
             })
            ->editColumn('description', function($data){

              if($data->description){
                return $data->description;
              }

            })
            ->addColumn('action', function($data){
                $delete_url = route('admin.filemanager.destroy', $data->id);
                return view('partials.action-button')->with(compact('delete_url'));
            })
            ->rawColumns(['description','action', 'image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FileManager $model)
    {
        $query = FileManager::all();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
      {
          return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    // ->addAction(['width' => '80px'])
                    // ->parameters($this->getBuilderParameters());
                    ->parameters($this->getParams());
      }

      public function getParams() {
          return [
              'dom' => 'lfrtip',
              'buttons' => ['create'],
              'stateSave' => true,
              'serverSide' => true,
              'processing' => true,
              'responsive' => true
          ];
      }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'id',
            'image',
            'link',
            'description',
            // 'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'FileManager_' . date('YmdHis');
    }
}
