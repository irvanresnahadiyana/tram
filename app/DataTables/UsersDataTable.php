<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($user){
                $edit_url = route('admin.users.edit', $user->id);
                $delete_url = route('admin.users.destroy', $user->id);
                return view('partials.action-button')->with(compact('edit_url','delete_url'));
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $query = User::join('role_users','role_users.user_id','=','users.id')
            ->join('roles','roles.id','=','role_users.role_id')
            ->join('activations','activations.user_id','=','users.id')
            ->where('roles.id', '=', 1)
            ->select('users.id','users.first_name','users.last_name','users.created_at','roles.name as role_name');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->addAction(['width' => '80px'])
                // ->addAction(['width' => '80px'])
                // ->parameters($this->getBuilderParameters());
                ->parameters($this->getParams());
    }

    public function getParams() {
        return [
            'dom' => 'lfrtip',
            'buttons' => ['create'],
            'stateSave' => true,
            'serverSide' => true,
            'processing' => true,
            'responsive' => true
        ];
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'first_name' => ['name'=>'users.first_name', 'data' => 'first_name', 'title'=>'First Name', 'id' => 'first_name'],
            'last_name' => ['name'=>'users.last_name', 'data' => 'last_name', 'title'=>'First Name', 'id' => 'last_name'],
            'role_name' => ['name'=>'roles.name', 'data' => 'role_name', 'title'=>'Role', 'id' => 'role_name'],
            'created_at' => ['name'=>'users.created_at', 'data' => 'created_at', 'title'=>'Created At', 'id' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }
}
