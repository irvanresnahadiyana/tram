<?php

namespace App\DataTables;

use App\Models\ServiceList;
use Yajra\DataTables\Services\DataTable;

class ServiceListDataTable extends DataTable
{
  /**
   * Build DataTable class.
   *
   * @param mixed $query Results from query() method.
   * @return \Yajra\DataTables\DataTableAbstract
   */
  public function dataTable($query)
  {
      return datatables($query)
       ->addColumn('action', function($data){
           $edit_url = route('admin.service-list.edit', $data->id);
           $delete_url = route('admin.service-list.destroy', $data->id);
             return view('partials.action-button')->with(compact('edit_url','delete_url'));
       });
  }

  /**
   * Get query source of dataTable.
   *
   * @param \App\User $model
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function query(ServiceList $model)
  {
      $query = ServiceList::join('service_categories', 'service_lists.service_id', '=', 'service_categories.id')
                          ->select('service_lists.id', 'service_categories.name');

      return $this->applyScopes($query);
  }

  /**
   * Optional method if you want to use html builder.
   *
   * @return \Yajra\DataTables\Html\Builder
   */
   public function html()
   {
       return $this->builder()
                 ->columns($this->getColumns())
                 ->minifiedAjax()
                 ->addAction(['width' => '80px'])
                 // ->addAction(['width' => '80px'])
                 // ->parameters($this->getBuilderParameters());
                 ->parameters($this->getParams());
   }

   public function getParams() {
       return [
           'dom' => 'lfrtip',
           'buttons' => ['create'],
           'stateSave' => true,
           'serverSide' => true,
           'processing' => true,
           'responsive' => true
       ];
   }

  /**
   * Get columns.
   *
   * @return array
   */
  protected function getColumns()
  {
      return [
          // 'id',
          'name',
      ];
  }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ServiceList_' . date('YmdHis');
    }
}
