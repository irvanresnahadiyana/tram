<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $form = [
            'url' => route('auth-post-login'),
            'autocomplete' => 'off',
        ];

        return view('backend.auth.login', compact('form'));
    }

    /**
     * Admin Handle login request.
     *
     * @param  \App\Http\Requests\Auth\WebLoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $backToLogin = redirect()->route('auth-login')->withInput();
        $findUser = Sentinel::findByCredentials(['login' => $request->input('email')]);


        // If we can not find user based on email...
        if (! $findUser) {
            flash()->error('Wrong email!');
            return $backToLogin;
        }

        try {
            $remember = (bool) $request->input('remember_me');
            // If password is incorrect...
            if (! Sentinel::authenticate($request->all(), $remember)) {
                flash()->error('Password is incorrect!');

                return $backToLogin;
            }

            flash()->success('Login success!');

            $role = Sentinel::findRoleById($findUser->roles->first()->id);
            if($role->slug == "administrator"){
              return redirect()->route('admin-dashboard');
            } else {
              flash()->error('Access not granted!');
              return $backToLogin;
            }

        } catch (ThrottlingException $e) {
            flash()->error('Too many attempts!');
        } catch (NotActivatedException $e) {
            flash()->error('Please activate your account before trying to log in.');
        }

        return $backToLogin;
    }

    /**
     * [Logout Admin]
     * @return [type] [Session Destroy]
     */
    public function logout()
    {
        Sentinel::logout();
        flash()->success(trans('general.logout_success'));
        return redirect()->route('auth-login');
    }

}
