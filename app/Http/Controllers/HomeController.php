<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CorporateIdentity;
use App\Models\ServiceCategory;
use App\Models\Team;
use App\Models\Client;
use App\Models\Career;
use App\Models\Hse;
use App\Models\Cgc;
use App\Models\Charter;
use App\Models\Csr;
use App\Models\CsrList;
use App\Models\InvestorRelation;
use App\Models\FinancialHighlight;
use App\Models\AnnualReport;
use App\Models\InvestorCalendar;
use App\Models\Shareholder;
use App\Models\ShareholderCompo;
use App\Models\CorporateAction;
use App\Models\MediaClipping;
use App\Models\MediaCenter;
use App\Models\ServiceList;
use App\Models\Contact;
use App\Models\Footer;
use App\Models\CompanyWithTram;
use App\Models\Milestone;
use App\Models\ImageHeader;

class HomeController extends Controller
{
    public function home()
    {
        $corporateIdentity = CorporateIdentity::first();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.home', compact('corporateIdentity','footer', 'imageHeader'));
    }

    public function about()
    {
        $corporateIdentity = CorporateIdentity::first();
        $team = Team::get();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.about', compact('corporateIdentity', 'team', 'footer','imageHeader'));
    }

    public function milestone()
    {
        $footer = Footer::first();
        $milestone = Milestone::orderBy('years', 'desc')->paginate(9);
        return view('frontend.milestone', compact('footer','milestone'));
    }

    public function services()
    {
        $corporateIdentity = CorporateIdentity::first();
        $service = ServiceCategory::all();
        $client = Client::all();
        $footer = Footer::first();
        $company = CompanyWithTram::get();
        $imageHeader = ImageHeader::first();
        return view('frontend.services', compact('corporateIdentity', 'service', 'client','footer','company','imageHeader'));
    }

    public function serviceCate($id)
    {
        $serviceAll = ServiceList::where('service_id', $id)->get();
        $serviceAllItem = ServiceCategory::all();
        $service = ServiceCategory::where('id', $id)->first();
        $footer = Footer::first();
        return view('frontend.service-category', compact('service', 'serviceAll', 'serviceAllItem', 'footer'));
    }

    public function careers()
    {
        $career = Career::first();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.career', compact('career', 'footer','imageHeader'));
    }

    public function contact()
    {   
        $contact = Contact::first();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.contact-us', compact('contact', 'footer','imageHeader'));
    }

    public function compliance()
    {
        $corporateIdentity = CorporateIdentity::first();
        $hse = Hse::first();
        $cgc = Cgc::first();
        $charter = Charter::all();
        $csr = Csr::first();
        $csrList = CsrList::all();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.compliance', compact('corporateIdentity','hse','cgc','charter','csr','csrList', 'footer','imageHeader'));
    }

    public function investor()
    {
        $investor = InvestorRelation::orderBy('date', 'desc')->paginate(5);
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::all();
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::all();
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }

        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.investor-relation', compact('imageHeader','investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual', 'calendar', 'dateCalendar', 'dataYears','dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function financial()
    {
        $investor = InvestorRelation::all();
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::orderBy('date', 'desc')->paginate(5);
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::all();
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }
        $footer = Footer::first();
        return view('frontend.financial_highlight', compact('investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual', 'calendar', 'dateCalendar', 'dataYears','dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function annual()
    {
        $investor = InvestorRelation::all();
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::all();
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::orderBy('date', 'desc')->paginate(5);
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }
        $footer = Footer::first();
        return view('frontend.annual_reports', compact('investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual', 'calendar', 'dateCalendar','dataYears','dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function investorCalendar()
    {
        $investor = InvestorRelation::all();
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::all();
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::all();
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->paginate(5);
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }
        $footer = Footer::first();
        return view('frontend.investor-calendar', compact('investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual','calendar', 'dateCalendar', 'dataYears' ,'dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function shareholderInfo()
    {
        $investor = InvestorRelation::all();
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::all();
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::all();
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $shareholder = Shareholder::where('id', 1)->first();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }
        $footer = Footer::first();
        return view('frontend.shareholder-info', compact('investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual','calendar', 'dateCalendar', 'shareholder', 'dataYears','dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function shareholderCompo()
    {
        $investor = InvestorRelation::all();
        $dateInvestor = InvestorRelation::orderBy('date', 'desc')->get();
        $financial = FinancialHighlight::all();
        $dateFinancial = FinancialHighlight::orderBy('date', 'desc')->get();
        $annual = AnnualReport::all();
        $dateAnnual = AnnualReport::orderBy('date', 'desc')->get();
        $calendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $dateCalendar = InvestorCalendar::orderBy('date', 'desc')->get();
        $shareholder = Shareholder::where('id', 2)->first();
        $shareholderCompo = ShareholderCompo::where('id', 1)->first();
        $years = [];
        $dataYears = [];
        foreach ($dateInvestor as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYears[$year][] = $month;
            } else {
                $dataYears[$year][] = $month;
            }
            
        }

        $dataYearsFinancial = [];
        foreach ($dateFinancial as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsFinancial[$year][] = $month;
            } else {
                $dataYearsFinancial[$year][] = $month;
            }
            
        }

        $dataYearsAnnual = [];
        foreach ($dateAnnual as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsAnnual[$year][] = $month;
            } else {
                $dataYearsAnnual[$year][] = $month;
            }
            
        }

        $dataYearsCalendar = [];
        foreach ($dateCalendar as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCalendar[$year][] = $month;
            } else {
                $dataYearsCalendar[$year][] = $month;
            }
            
        }
        $footer = Footer::first();
        return view('frontend.shareholder-composition', compact('investor', 'dateInvestor', 'financial', 'dateFinancial', 'annual', 'dateAnnual','calendar', 'dateCalendar', 'shareholderCompo', 'dataYears' ,'dataYearsFinancial', 'dataYearsCalendar', 'dataYearsAnnual', 'footer'));
    }

    public function corporateYear()
    {
        $corporatAction = CorporateAction::orderBy('date', 'desc')->paginate(5);
        $dateCorporateAction = CorporateAction::orderBy('date', 'desc')->get();
        $footer = Footer::first();
        $years = [];
        $dataYears = [];
        $dataYearsCorporate = [];
        foreach ($dateCorporateAction as $key => $value) {
            $year = date('Y', strtotime($value->date));
            $month = date('F',strtotime($value->date));
            if (!in_array($year, $years)) {
                array_push($years, $year);
                $dataYearsCorporate[$year][] = $month;
            } else {
                $dataYearsCorporate[$year][] = $month;
            }
            
        }
        return view('frontend.corporate-year', compact('corporatAction', 'dateCorporateAction', 'footer', 'dataYearsCorporate'));
    }

    public function mediaClipping()
    {
        $media = MediaClipping::orderBy('created_at', 'desc')->paginate(9);
        $corporatAction = CorporateAction::orderBy('date', 'desc')->get();
        $dateCorporateAction = CorporateAction::orderBy('date', 'desc')->get();
        $footer = Footer::first();
        
        return view('frontend.media-clipping', compact('corporatAction', 'dateCorporateAction', 'media', 'footer'));
    }

    public function mediaCenter()
    {
        $media = MediaCenter::orderBy('created_at', 'desc')->paginate(5);
        $corporatAction = CorporateAction::orderBy('date', 'desc')->get();
        $dateCorporateAction = CorporateAction::orderBy('date', 'desc')->get();
        $footer = Footer::first();
        $imageHeader = ImageHeader::first();
        return view('frontend.media-center', compact('corporatAction', 'dateCorporateAction', 'media', 'footer','imageHeader'));
    }
}
