<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\FinancialHighlightDataTable;
use App\Http\Controllers\Controller;
use App\Models\FinancialHighlight;

class FinancialHighlightController extends Controller
{
  public function index(Request $req, FinancialHighlightDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.financial_highlights.index');
      } else {
          return $dataTable->render('backend.financial_highlights.index');
      }
  }

  public function create()
  {
      return view('backend.financial_highlights.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $investor = New FinancialHighlight;
      $investor->description = $param['description'];
      $investor->description_id = $param['description_id'];
      $investor->date = $param['date'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/financial/');
        $investor->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/financial/';
            $file->move($destinationPath, $filename);
            $investor->file = 'uploads/financial/'.$filename;
      }

      $investor->save();

      if (!empty($investor)) {
          $request->session()->flash('success', 'Financial created successfully!');
          return redirect()->route('admin.financial.index');
      } else {
          $request->session()->flash('error', 'Financial failed to created!');
          return redirect()->route('admin.financial.index');
      }
  }

  public function edit($id)
  {
      $data = FinancialHighlight::findOrFail($id);

      if (!empty($data)) {
          return view('backend.financial_highlights.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.financial.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = FinancialHighlight::findOrFail($id);
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->date = $param['date'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/financial/');
        $updateData->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/financial/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/financial/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Financial Highlight updated successfully!');
          return redirect()->route('admin.financial.index');
      } else {
          $request->session()->flash('error', 'Financial Highlight failed to updated!');
          return redirect()->route('admin.financial.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = FinancialHighlight::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Investor Relation successfully deleted!');
              return redirect()->route('admin.financial.index');
          } else {
              session()->flash('error', 'Investor Relation failed deleted!');
              return redirect()->route('admin.financial.index');
          }
      } else {
          $request->session()->flash('error', 'Investor Relation not found!');
              return redirect()->route('admin.financial.index');
      }
  }
}
