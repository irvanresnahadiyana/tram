<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\CsrListDataTable;
use App\Http\Controllers\Controller;
use App\Models\CsrList;
use App\Models\Csr;

class CsrController extends Controller
{
  public function index(Request $req, CsrListDataTable $dataTable)
  {
      $param = $req->all();
      $data = Csr::first();
      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.compliance.csr.index', compact('data'));
      } else {
          return $dataTable->render('backend.compliance.csr.index', compact('data'));
      }
  }

  public function create()
  {
      return view('backend.compliance.csr.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $csr = New CsrList;
      $csr->name = $param['name'];
      $csr->description = $param['description'];
      $csr->description_id = $param['description_id'];

      // if (@$param['file']) {
      //   $uploadImage = upload_file($param['file'], 'uploads/csr/');
      //   $csr->file = $uploadImage;
      // }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/csr/';
            $file->move($destinationPath, $filename);
            $csr->file = 'uploads/csr/'.$filename;
      }

      $csr->save();

      if (!empty($csr)) {
          $request->session()->flash('success', 'CSR created successfully!');
          return redirect()->route('admin.csr.index');
      } else {
          $request->session()->flash('error', 'CSR failed to created!');
          return redirect()->route('admin.csr.index');
      }
  }

  public function storeDesc(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $findExisting = Csr::all();
      if(count($findExisting) > 0){
        $csr = Csr::findOrFail(1);
        $csr->description = $param['description'];
        $csr->description_id = $param['description_id'];
        $csr->update();
      } else {
        $csr = New Csr;
        $csr->description = $param['description'];
        $csr->description_id = $param['description_id'];

        $csr->save();
      }

      if (!empty($csr)) {
          $request->session()->flash('success_csr', 'CSR updated successfully!');
          return redirect()->route('admin.csr.index');
      } else {
          $request->session()->flash('error_csr', 'CSR failed to created!');
          return redirect()->route('admin.csr.index');
      }
  }

  public function edit($id)
  {
      $data = CsrList::findOrFail($id);

      if (!empty($data)) {
          return view('backend.compliance.csr.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.csr.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = CsrList::findOrFail($id);
      $updateData->update($param);

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Csr updated successfully!');
          return redirect()->route('admin.csr.index');
      } else {
          $request->session()->flash('error', 'Team failed to updated!');
          return redirect()->route('admin.csr.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = CsrList::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Csr successfully deleted!');
              return redirect()->route('admin.csr.index');
          } else {
              session()->flash('error', 'Csr failed deleted!');
              return redirect()->route('admin.csr.index');
          }
      } else {
          $request->session()->flash('error', 'Csr not found!');
              return redirect()->route('admin.csr.index');
      }
  }
}
