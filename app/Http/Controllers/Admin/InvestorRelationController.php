<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\InvestorRelationDataTable;
use App\Http\Controllers\Controller;
use App\Models\InvestorRelation;

class InvestorRelationController extends Controller
{
  public function index(Request $req, InvestorRelationDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.investor_relation.index');
      } else {
          return $dataTable->render('backend.investor_relation.index');
      }
  }

  public function create()
  {
      return view('backend.investor_relation.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $investor = New InvestorRelation;
      $investor->description = $param['description'];
      $investor->description_id = $param['description_id'];
      $investor->date = $param['date'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/investor/');
        $investor->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/investor/';
            $file->move($destinationPath, $filename);
            $investor->file = 'uploads/investor/'.$filename;
      }

      $investor->save();

      if (!empty($investor)) {
          $request->session()->flash('success', 'Investor Relation created successfully!');
          return redirect()->route('admin.investor.index');
      } else {
          $request->session()->flash('error', 'Investor Relation failed to created!');
          return redirect()->route('admin.investor.index');
      }
  }

  public function edit($id)
  {
      $data = InvestorRelation::findOrFail($id);

      if (!empty($data)) {
          return view('backend.investor_relation.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.investor.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = InvestorRelation::findOrFail($id);
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->date = $param['date'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/investor/');
        $updateData->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/investor/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/investor/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Investor updated successfully!');
          return redirect()->route('admin.investor.index');
      } else {
          $request->session()->flash('error', 'Investor failed to updated!');
          return redirect()->route('admin.investor.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = InvestorRelation::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Investor Relation successfully deleted!');
              return redirect()->route('admin.investor.index');
          } else {
              session()->flash('error', 'Investor Relation failed deleted!');
              return redirect()->route('admin.investor.index');
          }
      } else {
          $request->session()->flash('error', 'Investor Relation not found!');
              return redirect()->route('admin.Investor.index');
      }
  }
}
