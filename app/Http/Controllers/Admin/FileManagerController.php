<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\FileManagerDataTable;
use App\Http\Controllers\Controller;
use App\Models\FileManager;

class FileManagerController extends Controller
{
    public function index(Request $req, FileManagerDataTable $dataTable)
	{
      	$param = $req->all();

      	if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.file_manager.index');
      	} else {
          return $dataTable->render('backend.file_manager.index');
      	}
	}

  	public function create()
  	{
      return view('backend.file_manager.create');
  	}

  	public function store(Request $request)
  	{
      $param = $request->except('_token');
      // dd($param);
      $data = New FileManager;
      $data->description = $param['description'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/filemanager/');
        $data->image = $uploadImage['original'];
        $data->link = \URL::to('/').'/'.$uploadImage['original'];
      }

      $data->save();

      if (!empty($data)) {
          $request->session()->flash('success', 'Media added successfully!');
          return redirect()->route('admin.filemanager.index');
      } else {
          $request->session()->flash('error', 'Media failed to created!');
          return redirect()->route('admin.filemana.index');
      }
  	}

  	public function destroy($id)
  	{
      if (!empty($id)) {
          $deleteData = FileManager::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Media successfully deleted!');
              return redirect()->route('admin.filemanager.index');
          } else {
              session()->flash('error', 'Media failed deleted!');
              return redirect()->route('admin.filemanager.index');
          }
      } else {
          $request->session()->flash('error', 'Media not found!');
              return redirect()->route('admin.filemanager.index');
      }
  	}
}
