<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataTables\UsersDataTable;
use App\Repositories\UserRepository;
use App\Http\Requests\UserRequest;
use App\Http\Requests\SetPasswordRequest;
use App\Models\User;
use Datatables;
use Session;
use DB;
use Sentinel;

class UserController extends Controller
{
	protected $model, $repository;

    public function __construct()
    {
        $this->model = new User;
        $this->repository = new UserRepository;
    }

    public function index(Request $req, UsersDataTable $dataTable)
    {
        $param = $req->all();

        if (array_key_exists('message', $param)) {
            flash()->success($param['message']);
            return $dataTable->render('backend.users.index');
        } else {
            return $dataTable->render('backend.users.index');
        }
    }

    public function create()
    {

        return view('backend.users.create');
    }

    public function store(UserRequest $request)
    {
        $param = $request->except('_token');
        $saveData = $this->repository->createNew($param);

        if (!empty($saveData)) {
            $request->session()->flash('success', 'User created successfully!');
            return redirect()->route('admin.users.index');
        } else {
            $request->session()->flash('error', 'User failed to created!');
            return redirect()->route('admin.users.index');
        }
    }

    public function edit($id)
    {
        $data = $this->repository->getUserById($id);

        if (!empty($data)) {
            return view('backend.users.edit')->withData($data);
        } else {
            session()->flash('error', 'Data not found!');
            return redirect()->route('admin.users.index');
        }
    }


    public function update(Request $request, $id)
    {
        $param = $request->except('_token');
        $findData =  Sentinel::findById($id);

        // if($request->password != ''){
        //     $updateUser = $updateUser = Sentinel::update($findData, $request->except(['id','email']));
        // } else {
            $updateData = $this->repository->updateData($param, $id);
        // }

        if (!empty($updateData)) {
            $request->session()->flash('success', 'User updated successfully!');
            return redirect()->route('admin.users.index');
        } else {
            $request->session()->flash('error', 'User failed to updated!');
            return redirect()->route('admin.users.index');
        }

    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $deleteData = $this->repository->deleteData($id);

            if ($deleteData != false) {
                session()->flash('success', 'User successfully deleted!');
                return redirect()->route('admin.users.index');
            } else {
                session()->flash('error', 'User failed deleted!');
                return redirect()->route('admin.users.index');
            }
        } else {
            $request->session()->flash('error', 'User not found!');
                return redirect()->route('admin.users.index');
        }
    }

		public function setPassword($id)
    {
				$data = $this->repository->getUserById($id);
				return view('backend.users.set_password')->withData($data);
    }

		public function storeSetPassword(SetPasswordRequest $request)
    {
				$param = $request->except('_token');
				$user = Sentinel::findById($param['id']);
				$credentials = [
				    'email' => $user->email,
						'password' => $param['password']
				];
				$user = Sentinel::update($user, $credentials);

				return redirect()->route('auth-login');
    }
}
