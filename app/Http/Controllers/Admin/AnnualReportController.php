<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\AnnualReportDataTable;
use App\Http\Controllers\Controller;
use App\Models\AnnualReport;

class AnnualReportController extends Controller
{
  public function index(Request $req, AnnualReportDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.annual_reports.index');
      } else {
          return $dataTable->render('backend.annual_reports.index');
      }
  }

  public function create()
  {
      return view('backend.annual_reports.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $annual = New AnnualReport;
      $annual->description = $param['description'];
      $annual->description_id = $param['description_id'];
      $annual->date = $param['date'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/annual/');
        $annual->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/annual/';
            $file->move($destinationPath, $filename);
            $annual->file = 'uploads/annual/'.$filename;
      }

      $annual->save();

      if (!empty($annual)) {
          $request->session()->flash('success', 'Annual created successfully!');
          return redirect()->route('admin.annual.index');
      } else {
          $request->session()->flash('error', 'Annual failed to created!');
          return redirect()->route('admin.annual.index');
      }
  }

  public function edit($id)
  {
      $data = AnnualReport::findOrFail($id);

      if (!empty($data)) {
          return view('backend.annual_reports.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.annual.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = AnnualReport::findOrFail($id);
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->date = $param['date'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/annual/');
        $updateData->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/annual/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/annual/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Annual Report updated successfully!');
          return redirect()->route('admin.annual.index');
      } else {
          $request->session()->flash('error', 'Annual Report failed to updated!');
          return redirect()->route('admin.annual.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = AnnualReport::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Annual Report successfully deleted!');
              return redirect()->route('admin.annual.index');
          } else {
              session()->flash('error', 'Annual Report failed deleted!');
              return redirect()->route('admin.annual.index');
          }
      } else {
          $request->session()->flash('error', 'Investor Relation not found!');
              return redirect()->route('admin.annual.index');
      }
  }
}
