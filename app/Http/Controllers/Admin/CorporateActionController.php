<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\CorporateActionDataTable;
use App\Http\Controllers\Controller;
use App\Models\CorporateAction;

class CorporateActionController extends Controller
{
  public function index(Request $req, CorporateActionDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.corporate_action.index');
      } else {
          return $dataTable->render('backend.corporate_action.index');
      }
  }

  public function create()
  {
      return view('backend.corporate_action.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $data = New CorporateAction;
      // $data->name = $param['name'];
      $data->description = $param['description'];
      $data->description_id = $param['description_id'];
      $data->date = $param['date'];
      //
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $investor->image = $uploadImage['original'];
      // }
      //
      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/corporate_action/';
            $file->move($destinationPath, $filename);
            $data->file = 'uploads/corporate_action/'.$filename;
      }

      $data->save();

      if (!empty($data)) {
          $request->session()->flash('success', 'Corporate Action created successfully!');
          return redirect()->route('admin.corporate-action.index');
      } else {
          $request->session()->flash('error', 'Corporate Action failed to created!');
          return redirect()->route('admin.corporate-action.index');
      }
  }

  public function edit($id)
  {
      $data = CorporateAction::findOrFail($id);

      if (!empty($data)) {
          return view('backend.corporate_action.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.corporate-action.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = CorporateAction::findOrFail($id);
      // $updateData->name = $param['name'];
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->date = $param['date'];
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $updateData->image = $uploadImage['original'];
      // }
      //
      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/corporate_action/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/corporate_action/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Corporate Action updated successfully!');
          return redirect()->route('admin.corporate-action.index');
      } else {
          $request->session()->flash('error', 'Corporate Action failed to updated!');
          return redirect()->route('admin.corporate-action.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = CorporateAction::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Corporate Action successfully deleted!');
              return redirect()->route('admin.corporate-action.index');
          } else {
              session()->flash('error', 'Shareholder Relation failed deleted!');
              return redirect()->route('admin.corporate-action.index');
          }
      } else {
          $request->session()->flash('error', 'Corporate Action Relation not found!');
              return redirect()->route('admin.corporate-action.index');
      }
  }
}
