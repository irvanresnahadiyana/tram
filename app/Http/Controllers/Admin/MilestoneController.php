<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\MilestoneDataTable;
use App\Http\Controllers\Controller;
use App\Models\Milestone;

class MilestoneController extends Controller
{
    public function index(Request $req, MilestoneDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.milestone.index');
      } else {
          return $dataTable->render('backend.milestone.index');
      }
  }

  public function create()
  {
      return view('backend.milestone.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $data = New Milestone;
      // $data->name = $param['name'];
      $data->description = $param['description'];
      $data->description_id = $param['description_id'];
      $data->years = $param['date'];
      //
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $investor->image = $uploadImage['original'];
      // }
      //
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/milestone/');
        $data->image = $uploadImage['original'];
      }

      $data->save();

      if (!empty($data)) {
          $request->session()->flash('success', 'Milestone created successfully!');
          return redirect()->route('admin.milestone.index');
      } else {
          $request->session()->flash('error', 'Milestone failed to created!');
          return redirect()->route('admin.milestone.index');
      }
  }

  public function edit($id)
  {
      $data = Milestone::findOrFail($id);

      if (!empty($data)) {
          return view('backend.milestone.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.milestone.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Milestone::findOrFail($id);
      // $updateData->name = $param['name'];
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->years = $param['date'];
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $updateData->image = $uploadImage['original'];
      // }
      //
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/milestone/');
        $updateData->image = $uploadImage['original'];
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Corporate Action updated successfully!');
          return redirect()->route('admin.milestone.index');
      } else {
          $request->session()->flash('error', 'Corporate Action failed to updated!');
          return redirect()->route('admin.milestone.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = Milestone::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Milestone successfully deleted!');
              return redirect()->route('admin.milestone.index');
          } else {
              session()->flash('error', 'Milestone failed deleted!');
              return redirect()->route('admin.milestone.index');
          }
      } else {
          $request->session()->flash('error', 'Milestone Relation not found!');
              return redirect()->route('admin.milestone.index');
      }
  }
}
