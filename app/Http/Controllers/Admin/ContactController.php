<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    public function edit($id)
	  {
	      $data = Contact::findOrFail($id);

	      if (!empty($data)) {
	          return view('backend.contact.edit')->withData($data);
	      } else {
	          session()->flash('error', 'Data not found!');
	          return redirect()->route('admin.contact.edit', ['id' => 1]);
	      }
	  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Contact::findOrFail($id);
      $updateData->company_name = $param['company_name'];
      $updateData->website_name = $param['website_name'];
      $updateData->tricker_code = $param['tricker_code'];
      $updateData->representative_office = $param['representative_office'];
      $updateData->company_address = $param['company_address'];
      $updateData->media_contact = $param['media_contact'];
      $updateData->corporate_secertary = $param['corporate_secertary'];
      $updateData->stock_exchange = $param['stock_exchange'];
      $updateData->share_register = $param['share_register'];
      $updateData->public_accountant = $param['public_accountant'];
      
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Contact updated successfully!');
          return redirect()->route('admin.contact.edit', ['id' => 1]);
      } else {
          $request->session()->flash('error', 'Contact failed to updated!');
          return redirect()->route('admin.contact.edit', ['id' => 1]);
      }

  }
}
