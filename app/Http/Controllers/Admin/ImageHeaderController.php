<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\HeaderImageDataTable;
use App\Http\Controllers\Controller;
use App\Models\ImageHeader;

class ImageHeaderController extends Controller
{
    public function index(Request $req, HeaderImageDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.image_header.index');
      } else {
          return $dataTable->render('backend.image_header.index');
      }
  }

  public function create()
  {
      return view('backend.image_header.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $image_header = New ImageHeader;
      

      if (@$param['image_home']) {
        $uploadImage = upload_file($param['image_home'], 'uploads/image_headers/');
        $image_header->image_home = $uploadImage['original'];
      }

      if (@$param['image_about']) {
        $uploadImage = upload_file($param['image_about'], 'uploads/image_headers/');
        $image_header->image_about = $uploadImage['original'];
      }


      if (@$param['image_service']) {
        $uploadImage = upload_file($param['image_service'], 'uploads/image_headers/');
        $image_header->image_service = $uploadImage['original'];
      }
      

      if (@$param['image_compliance']) {
        $uploadImage = upload_file($param['image_compliance'], 'uploads/image_headers/');
        $image_header->image_compliance = $uploadImage['original'];
      }

      if (@$param['image_investor_relation']) {
        $uploadImage = upload_file($param['image_investor_relation'], 'uploads/image_headers/');
        $image_header->image_investor_relation = $uploadImage['original'];
      }

      if (@$param['image_news']) {
        $uploadImage = upload_file($param['image_news'], 'uploads/image_headers/');
        $image_header->image_news = $uploadImage['original'];
      }

      if (@$param['image_career']) {
        $uploadImage = upload_file($param['image_career'], 'uploads/image_headers/');
        $image_header->image_career = $uploadImage['original'];
      }

      if (@$param['image_contact']) {
        $uploadImage = upload_file($param['image_contact'], 'uploads/image_headers/');
        $image_header->image_contact = $uploadImage['original'];
      }

      $image_header->save();

      if (!empty($image_header)) {
          $request->session()->flash('success', 'Image created successfully!');
          return redirect()->route('admin.image-header.index');
      } else {
          $request->session()->flash('error', 'Image failed to created!');
          return redirect()->route('admin.image-header.index');
      }
  }

  public function edit($id)
  {
      $data = ImageHeader::findOrFail($id);

      if (!empty($data)) {
          return view('backend.image_header.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.image-header.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = ImageHeader::findOrFail($id);

      if (@$param['image_home']) {
        $uploadImage = upload_file($param['image_home'], 'uploads/image_headers/');
        $updateData->image_home = $uploadImage['original'];
      }

      if (@$param['image_about']) {
        $uploadImage = upload_file($param['image_about'], 'uploads/image_headers/');
        $updateData->image_about = $uploadImage['original'];
      }


      if (@$param['image_service']) {
        $uploadImage = upload_file($param['image_service'], 'uploads/image_headers/');
        $updateData->image_service = $uploadImage['original'];
      }
      

      if (@$param['image_compliance']) {
        $uploadImage = upload_file($param['image_compliance'], 'uploads/image_headers/');
        $updateData->image_compliance = $uploadImage['original'];
      }

      if (@$param['image_investor_relation']) {
        $uploadImage = upload_file($param['image_investor_relation'], 'uploads/image_headers/');
        $updateData->image_investor_relation = $uploadImage['original'];
      }

      if (@$param['image_news']) {
        $uploadImage = upload_file($param['image_news'], 'uploads/image_headers/');
        $updateData->image_news = $uploadImage['original'];
      }

      if (@$param['image_career']) {
        $uploadImage = upload_file($param['image_career'], 'uploads/image_headers/');
        $updateData->image_career = $uploadImage['original'];
      }

      if (@$param['image_contact']) {
        $uploadImage = upload_file($param['image_contact'], 'uploads/image_headers/');
        $updateData->image_contact = $uploadImage['original'];
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Image updated successfully!');
          return redirect()->route('admin.image-header.index');
      } else {
          $request->session()->flash('error', 'Image failed to updated!');
          return redirect()->route('admin.image-header.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = FinancialHighlight::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Investor Relation successfully deleted!');
              return redirect()->route('admin.financial.index');
          } else {
              session()->flash('error', 'Investor Relation failed deleted!');
              return redirect()->route('admin.financial.index');
          }
      } else {
          $request->session()->flash('error', 'Investor Relation not found!');
              return redirect()->route('admin.financial.index');
      }
  }
}
