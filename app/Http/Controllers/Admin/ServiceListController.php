<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\ServiceListDataTable;
use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\ServiceList;

class ServiceListController extends Controller
{
  public function index(Request $req, ServiceListDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.services_list.index');
      } else {
          return $dataTable->render('backend.services_list.index');
      }
  }

  public function create()
  {
      $service = ServiceCategory::all();
      $dataService = [];
      foreach ($service as $value) {
          $dataService[$value->id] = $value->name;
      }
      return view('backend.services_list.create', compact('dataService'));
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      $saveData = New ServiceList;
      $saveData->description = $param['description'];
      $saveData->service_id = $param['service_id'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/service/');
        $saveData->image = $uploadImage['original'];
      }
      $saveData->save();

      if (!empty($saveData)) {
          $request->session()->flash('success', 'Service created successfully!');
          return redirect()->route('admin.service-list.index');
      } else {
          $request->session()->flash('error', 'Service failed to created!');
          return redirect()->route('admin.service-list.index');
      }
  }

  public function addList(Request $request)
  {
      $param = $request->except('_token');
      $saveData = New ServiceList;
      $saveData->description = $param['description'];
      $saveData->service_id = $param['service_id'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/service/');
        $saveData->image = $uploadImage['original'];
      }
      $saveData->save();

      if (!empty($saveData)) {
          $request->session()->flash('success', 'Service created successfully!');
          return redirect()->route('admin.service.index');
      } else {
          $request->session()->flash('error', 'Service failed to created!');
          return redirect()->route('admin.service.index');
      }
  }

  public function edit($id)
  {
      $data = ServiceList::findOrFail($id);
      $service = ServiceCategory::all();
      $dataService = [];
      foreach ($service as $value) {
          $dataService[$value->id] = $value->name;
      }

      if (!empty($data)) {
          return view('backend.services_list.edit', compact('data', 'dataService'));
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.service-list.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = ServiceList::findOrFail($id);
      $updateData->description = $param['description'];
      $updateData->service_id = $param['service_id'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/service/');
        $updateData->image = $uploadImage['original'];
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Service updated successfully!');
          return redirect()->route('admin.service-list.index');
      } else {
          $request->session()->flash('error', 'Service failed to updated!');
          return redirect()->route('admin.service-list.index');
      }

  }

  public function destroy($id)
    {
      if (!empty($id)) {
          $deleteData = ServiceList::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Service successfully deleted!');
              return redirect()->route('admin.service.index');
          } else {
              session()->flash('error', 'Service failed deleted!');
              return redirect()->route('admin.service.index');
          }
      } else {
          $request->session()->flash('error', 'Service not found!');
              return redirect()->route('admin.service.index');
      }
    }
}
