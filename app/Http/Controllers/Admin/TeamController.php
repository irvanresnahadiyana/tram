<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\TeamDataTable;
use App\Http\Controllers\Controller;
use App\Models\Team;

class TeamController extends Controller
{
  public function index(Request $req, TeamDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.teams.index');
      } else {
          return $dataTable->render('backend.teams.index');
      }
  }

  public function create()
  {
      return view('backend.teams.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $team = New Team;
      $team->name = $param['name'];
      $team->type = $param['type'];
      $team->jabatan = $param['jabatan'];
      $team->biography = $param['biography'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/avatars/');
        $team->image = $uploadImage['thumbnail'];
      }
      $team->save();

      if (!empty($team)) {
          $request->session()->flash('success', 'Identity created successfully!');
          return redirect()->route('admin.team.index');
      } else {
          $request->session()->flash('error', 'Identity failed to created!');
          return redirect()->route('admin.team.index');
      }
  }

  public function edit($id)
  {
      $data = Team::findOrFail($id);

      if (!empty($data)) {
          return view('backend.teams.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.team.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Team::findOrFail($id);

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/avatars/');
        $updateData->image = $uploadImage['thumbnail'];
      }
      $updateData->name = $param['name'];
      $updateData->type = $param['type'];
      $updateData->jabatan = $param['jabatan'];
      $updateData->biography = $param['biography'];
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Team updated successfully!');
          return redirect()->route('admin.team.index');
      } else {
          $request->session()->flash('error', 'Team failed to updated!');
          return redirect()->route('admin.team.index');
      }

  }
}
