<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\CareerDataTable;
use App\Http\Controllers\Controller;
use App\Models\Career;

class CareerController extends Controller
{
  public function index(Request $req, CareerDataTable $dataTable)
  {
      $datas = Career::count();
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.career.index', compact('datas'));
      } else {
          return $dataTable->render('backend.career.index', compact('datas'));
      }
  }

  public function create()
  {
      return view('backend.career.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $career = New Career;
      $career->career_list = $param['career_list'];
      $career->career_overview = $param['career_overview'];
      $career->save();

      if (!empty($career)) {
          $request->session()->flash('success', 'Career added successfully!');
          return redirect()->route('admin.career.index');
      } else {
          $request->session()->flash('error', 'Career failed to created!');
          return redirect()->route('admin.career.index');
      }
  }

  public function edit($id)
  {
      $data = Career::findOrFail($id);

      if (!empty($data)) {
          return view('backend.career.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.career.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Career::findOrFail($id);
      $updateData->career_list = $param['career_list'];
      $updateData->career_overview = $param['career_overview'];
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Career updated successfully!');
          return redirect()->route('admin.career.index');
      } else {
          $request->session()->flash('error', 'Career failed to updated!');
          return redirect()->route('admin.career.index');
      }

  }
}
