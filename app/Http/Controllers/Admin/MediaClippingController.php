<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\MediaClippingDataTable;
use App\Http\Controllers\Controller;
use App\Models\MediaClipping;

class MediaClippingController extends Controller
{
  public function index(Request $req, MediaClippingDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.media_clipping.index');
      } else {
          return $dataTable->render('backend.media_clipping.index');
      }
  }

  public function create()
  {
      return view('backend.media_clipping.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $data = New MediaClipping;
      $data->name = $param['name'];
      $data->type = $param['type'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/media_clipping/');
        $data->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/media_clipping/';
            $file->move($destinationPath, $filename);
            $data->file = 'uploads/media_clipping/'.$filename;
      }

      $data->save();

      if (!empty($data)) {
          $request->session()->flash('success', 'Media Clipping added successfully!');
          return redirect()->route('admin.media-clipping.index');
      } else {
          $request->session()->flash('error', 'Media Clipping failed to created!');
          return redirect()->route('admin.media-clipping.index');
      }
  }

  public function edit($id)
  {
      $data = MediaClipping::findOrFail($id);

      if (!empty($data)) {
          return view('backend.media_clipping.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.media-clipping.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = MediaClipping::findOrFail($id);
      $updateData->name = $param['name'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/media_clipping/');
        $updateData->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/media_clipping/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/media_clipping/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Media Clipping updated successfully!');
          return redirect()->route('admin.media-clipping.index');
      } else {
          $request->session()->flash('error', 'Media Clipping failed to updated!');
          return redirect()->route('admin.media-clipping.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = MediaClipping::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Media successfully deleted!');
              return redirect()->route('admin.media-clipping.index');
          } else {
              session()->flash('error', 'Media failed deleted!');
              return redirect()->route('admin.media-clipping.index');
          }
      } else {
          $request->session()->flash('error', 'Media not found!');
              return redirect()->route('admin.media-clipping.index');
      }
  }
}
