<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cgc;
use App\Models\Hse;

class ComplianceController extends Controller
{
  public function cgc()
  {
      $data = Cgc::first();
      if($data){
          return view('backend.compliance.cgc.create', compact('data'));
      } else {
          return view('backend.compliance.cgc.create');
      }

  }

  public function cgcStore(Request $request)
  {
      $findExisting = Cgc::all();
      $param = $request->except('_token');
      if(count($findExisting) > 0){
        $saveData = Cgc::findOrFail(1);
        $saveData->description = $param['description'];
        $saveData->description_id = $param['description_id'];

        if (@$param['image']) {
          $uploadImage = upload_file($param['image'], 'uploads/compliance/');
          $saveData->image = $uploadImage['original'];
        }
        $saveData->save();
      } else {
        $saveData = New Cgc;
        $saveData->description = $param['description'];
        $saveData->description_id = $param['description_id'];

        if (@$param['image']) {
          $uploadImage = upload_file($param['image'], 'uploads/compliance/');
          $saveData->image = $uploadImage['original'];
        }
        $saveData->save();
      }


      if (!empty($saveData)) {
          $request->session()->flash('success', 'Cgc updated successfully!');
          return redirect()->route('admin.cgc.index');
      } else {
          $request->session()->flash('error', 'Cgc failed to created!');
          return redirect()->route('admin.cgc.index');
      }
  }

  public function hse()
  {
      $data = Hse::first();
      if($data){
          return view('backend.compliance.hse.create', compact('data'));
      } else {
          return view('backend.compliance.hse.create');
      }

  }

  public function hseStore(Request $request)
  {
      $findExisting = Hse::all();
      $param = $request->except('_token');
      if(count($findExisting) > 0){
        $saveData = Hse::findOrFail(1);
        $saveData->description = $param['description'];
        $saveData->description_id = $param['description_id'];

        if (@$param['image']) {
          $uploadImage = upload_file($param['image'], 'uploads/compliance/');
          $saveData->image = $uploadImage['original'];
        }
        $saveData->save();
      } else {
        $saveData = New Hse;
        $saveData->description = $param['description'];
        $saveData->description_id = $param['description_id'];

        if (@$param['image']) {
          $uploadImage = upload_file($param['image'], 'uploads/compliance/');
          $saveData->image = $uploadImage['original'];
        }
        $saveData->save();
      }


      if (!empty($saveData)) {
          $request->session()->flash('success', 'Hse updated successfully!');
          return redirect()->route('admin.hse.index');
      } else {
          $request->session()->flash('error', 'Hse failed to created!');
          return redirect()->route('admin.hse.index');
      }
  }
}
