<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Footer;

class FooterController extends Controller
{
    public function edit($id = 0)
	  {
        $data = Footer::findOrFail($id);

        if (!empty($data)) {
            return view('backend.footer.edit')->withData($data);
        } else {
            session()->flash('error', 'Data not found!');
            return redirect()->route('admin.footer.edit', ['id' => 1]);
        }
	  }

    public function update(Request $request, $id)
    {
        $param = $request->except('_token');
        $updateData = Footer::findOrFail($id);
        $updateData->footer_text = $param['footer_text'];
        
        $updateData->update();

        if (!empty($updateData)) {
            $request->session()->flash('success', ' Footer updated successfully!');
            return redirect()->route('admin.footer.edit', ['id' => 1]);
        } else {
            $request->session()->flash('error', 'Footer failed to updated!');
            return redirect()->route('admin.footer.edit', ['id' => 1]);
        }

    }
}
