<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShareholderCompo;

class ShareholderCompoController extends Controller
{
    public function create()
  {
      return view('backend.shareholder_composition.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $shareholder = New ShareholderCompo;
      // $shareholder->name = $param['name'];
      $shareholder->description = $param['description'];
      $shareholder->description_id = $param['description_id'];
      // $investor->date = $param['date'];
      //
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $investor->image = $uploadImage['original'];
      // }
      //
      // if ($file = $request->hasFile('file')) {
      //       $file = $request->file('file');
      //       $filename = time() . '.' . $file->getClientOriginalExtension();
      //       $destinationPath = public_path() . '/uploads/investor/';
      //       $file->move($destinationPath, $filename);
      //       $investor->file = 'uploads/investor/'.$filename;
      // }

      $shareholder->save();

      if (!empty($shareholder)) {
          $request->session()->flash('success', 'Shareholder Composition created successfully!');
          return redirect()->route('admin.shareholder-composition.index');
      } else {
          $request->session()->flash('error', 'Shareholder Composition failed to created!');
          return redirect()->route('admin.shareholder-composition.index');
      }
  }

  public function edit($id)
  {
      $data = ShareholderCompo::findOrFail($id);

      if (!empty($data)) {
          return view('backend.shareholder_composition.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.shareholder-composition.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = ShareholderCompo::findOrFail($id);
      // $updateData->name = $param['name'];
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      // $updateData->date = $param['date'];
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $updateData->image = $uploadImage['original'];
      // }
      //
      // if ($file = $request->hasFile('file')) {
      //       $file = $request->file('file');
      //       $filename = time() . '.' . $file->getClientOriginalExtension();
      //       $destinationPath = public_path() . '/uploads/investor/';
      //       $file->move($destinationPath, $filename);
      //       $updateData->file = 'uploads/investor/'.$filename;
      // }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Shareholder Composition updated successfully!');
          return redirect()->route('admin.shareholder-composition.edit', ['id' => 1]);
      } else {
          $request->session()->flash('error', 'Shareholder Composition failed to updated!');
          return redirect()->route('admin.shareholder-composition.edit', ['id' => 1]);
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = Shareholder::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Shareholder Composition successfully deleted!');
              return redirect()->route('admin.shareholder-composition.index');
          } else {
              session()->flash('error', 'Shareholder Composition failed deleted!');
              return redirect()->route('admin.shareholder-composition.index');
          }
      } else {
          $request->session()->flash('error', 'shareholder Composition not found!');
              return redirect()->route('admin.shareholder-composition.index');
      }
  }
}
