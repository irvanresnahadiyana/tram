<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\ServiceCategoryDataTable;
use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\ServiceList;

class ServiceController extends Controller
{
  public function index(Request $req, ServiceCategoryDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.services.index');
      } else {
          return $dataTable->render('backend.services.index');
      }
  }

  public function create()
  {
      return view('backend.services.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $service = ServiceCategory::create($param);

      if (!empty($service)) {
          $request->session()->flash('success', 'Service created successfully!');
          return redirect()->route('admin.service.index');
      } else {
          $request->session()->flash('error', 'Service failed to created!');
          return redirect()->route('admin.service.index');
      }
  }

  public function edit($id)
  {
      $data = ServiceCategory::findOrFail($id);

      if (!empty($data)) {
          return view('backend.services.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.service.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = ServiceCategory::findOrFail($id);
      $updateData->update($param);

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Service updated successfully!');
          return redirect()->route('admin.service.index');
      } else {
          $request->session()->flash('error', 'Service failed to updated!');
          return redirect()->route('admin.service.index');
      }

  }

    public function destroy($id)
    {
      if (!empty($id)) {
          $deleteData = ServiceCategory::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Service successfully deleted!');
              return redirect()->route('admin.service.index');
          } else {
              session()->flash('error', 'Service failed deleted!');
              return redirect()->route('admin.service.index');
          }
      } else {
          $request->session()->flash('error', 'Service not found!');
              return redirect()->route('admin.service.index');
      }
    }
}
