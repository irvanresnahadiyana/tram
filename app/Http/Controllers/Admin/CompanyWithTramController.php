<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataTables\CompanyWithTramDataTable;
use App\Models\CompanyWithTram;

class CompanyWithTramController extends Controller
{
    public function index(Request $req, CompanyWithTramDataTable $dataTable)
    {
        $param = $req->all();

        if (array_key_exists('message', $param)) {
            flash()->success($param['message']);
            return $dataTable->render('backend.company_tram.index');
        } else {
            return $dataTable->render('backend.company_tram.index');
        }
    }

    public function create()
    {
        return view('backend.company_tram.create');
    }

    public function store(Request $request)
    {
        $param = $request->except('_token');
        $saveData = New CompanyWithTram;
        $saveData->company_name = $param['company_name'];
        $saveData->link = $param['link'];
        $saveData->save();

        if (!empty($saveData)) {
            $request->session()->flash('success', 'Data created successfully!');
            return redirect()->route('admin.company.index');
        } else {
            $request->session()->flash('error', 'Data failed to created!');
            return redirect()->route('admin.company.index');
        }
    }

    public function edit($id)
    {
        $data = CompanyWithTram::findOrFail($id);
        if (!empty($data)) {
            return view('backend.company_tram.edit', compact('data'));
        } else {
            session()->flash('error', 'Data not found!');
            return redirect()->route('admin.company.index');
        }
    }

    public function update(Request $request, $id)
    {
        $param = $request->except('_token');
        $updateData = CompanyWithTram::findOrFail($id);
        $updateData->company_name = $param['company_name'];
        $updateData->link = $param['link'];
        $updateData->update();

        if (!empty($updateData)) {
            $request->session()->flash('success', 'Data updated successfully!');
            return redirect()->route('admin.company.index');
        } else {
            $request->session()->flash('error', 'Data failed to updated!');
            return redirect()->route('admin.company.index');
        }

    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $deleteData = CompanyWithTram::findOrFail($id);
            $deleteData->delete();

            if ($deleteData != false) {
                session()->flash('success', 'Data successfully deleted!');
                return redirect()->route('admin.company.index');
            } else {
                session()->flash('error', 'Data failed deleted!');
                return redirect()->route('admin.company.index');
            }
        } else {
            $request->session()->flash('error', 'Data not found!');
                return redirect()->route('admin.company.index');
        }
    }
}
