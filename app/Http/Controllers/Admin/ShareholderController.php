<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\ShareholderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Shareholder;

class ShareholderController extends Controller
{
  public function index(Request $req, ShareholderDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.shareholder.index');
      } else {
          return $dataTable->render('backend.shareholder.index');
      }
  }

  public function create()
  {
      return view('backend.shareholder.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $shareholder = New Shareholder;
      $shareholder->name = $param['name'];
      $shareholder->description = $param['description'];
      $shareholder->description_id = $param['description_id'];
      // $investor->date = $param['date'];
      //
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $investor->image = $uploadImage['original'];
      // }
      //
      // if ($file = $request->hasFile('file')) {
      //       $file = $request->file('file');
      //       $filename = time() . '.' . $file->getClientOriginalExtension();
      //       $destinationPath = public_path() . '/uploads/investor/';
      //       $file->move($destinationPath, $filename);
      //       $investor->file = 'uploads/investor/'.$filename;
      // }

      $shareholder->save();

      if (!empty($shareholder)) {
          $request->session()->flash('success', 'Shareholder Relation created successfully!');
          return redirect()->route('admin.shareholder.index');
      } else {
          $request->session()->flash('error', 'Shareholder Relation failed to created!');
          return redirect()->route('admin.shareholder.index');
      }
  }

  public function edit($id)
  {
      $data = Shareholder::findOrFail($id);

      if (!empty($data)) {
          return view('backend.shareholder.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.shareholder.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Shareholder::findOrFail($id);
      $updateData->name = $param['name'];
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      // $updateData->date = $param['date'];
      // if (@$param['image']) {
      //   $uploadImage = upload_file($param['image'], 'uploads/investor/');
      //   $updateData->image = $uploadImage['original'];
      // }
      //
      // if ($file = $request->hasFile('file')) {
      //       $file = $request->file('file');
      //       $filename = time() . '.' . $file->getClientOriginalExtension();
      //       $destinationPath = public_path() . '/uploads/investor/';
      //       $file->move($destinationPath, $filename);
      //       $updateData->file = 'uploads/investor/'.$filename;
      // }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', ' Shareholder updated successfully!');
          return redirect()->route('admin.shareholder.index');
      } else {
          $request->session()->flash('error', 'Shareholder failed to updated!');
          return redirect()->route('admin.shareholder.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = Shareholder::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Shareholder Relation successfully deleted!');
              return redirect()->route('admin.shareholder.index');
          } else {
              session()->flash('error', 'Shareholder Relation failed deleted!');
              return redirect()->route('admin.shareholder.index');
          }
      } else {
          $request->session()->flash('error', 'shareholder Relation not found!');
              return redirect()->route('admin.shareholder.index');
      }
  }
}
