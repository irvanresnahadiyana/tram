<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\CharterDataTable;
use App\Http\Controllers\Controller;
use App\Models\Charter;

class CharterController extends Controller
{
  public function index(Request $req, CharterDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.compliance.charter_of_commitee.index');
      } else {
          return $dataTable->render('backend.compliance.charter_of_commitee.index');
      }
  }

  public function create()
  {
      return view('backend.compliance.charter_of_commitee.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $charter = New Charter;
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/coc/');
        $charter->image = $uploadImage['original'];
      }
      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/coc/';
            $file->move($destinationPath, $filename);
            $charter->file = 'uploads/coc/'.$filename;
      }
      $charter->save();

      if (!empty($charter)) {
          $request->session()->flash('success', 'Identity created successfully!');
          return redirect()->route('admin.charter.index');
      } else {
          $request->session()->flash('error', 'Identity failed to created!');
          return redirect()->route('admin.charter.index');
      }
  }

  public function edit($id)
  {
      $data = Charter::findOrFail($id);

      if (!empty($data)) {
          return view('backend.compliance.charter_of_commitee.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.charter.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Charter::findOrFail($id);
      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/coc/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/coc/'.$filename;
      }
      if ($file = $request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/coc/';
            $file->move($destinationPath, $filename);
            $updateData->image = 'uploads/coc/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Coc updated successfully!');
          return redirect()->route('admin.charter.index');
      } else {
          $request->session()->flash('error', 'Coc failed to updated!');
          return redirect()->route('admin.charter.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = Charter::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Coc successfully deleted!');
              return redirect()->route('admin.charter.index');
          } else {
              session()->flash('error', 'Coc failed deleted!');
              return redirect()->route('admin.charter.index');
          }
      } else {
          $request->session()->flash('error', 'Coc not found!');
              return redirect()->route('admin.charter.index');
      }
  }
}
