<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\OurClientDataTable;
use App\Http\Controllers\Controller;
use App\Models\Client;

class OurClientController extends Controller
{
  public function index(Request $req, OurClientDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.our_clients.index');
      } else {
          return $dataTable->render('backend.our_clients.index');
      }
  }

  public function create()
  {
      return view('backend.our_clients.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $client = New Client;
      $client->name = $param['name'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/our_clients/');
        $client->image = $uploadImage['original'];
      }
      $client->save();

      if (!empty($client)) {
          $request->session()->flash('success', 'Client added successfully!');
          return redirect()->route('admin.client.index');
      } else {
          $request->session()->flash('error', 'Client added to created!');
          return redirect()->route('admin.client.index');
      }
  }

  public function edit($id)
  {
      $data = Client::findOrFail($id);

      if (!empty($data)) {
          return view('backend.our_clients.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.client.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = Client::findOrFail($id);
      if($file = $request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/our_clients/';
            $file->move($destinationPath, $filename);
            $updateData->image = 'uploads/our_clients/'.$filename;
      }
      $updateData->name = $param['name'];
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Client updated successfully!');
          return redirect()->route('admin.client.index');
      } else {
          $request->session()->flash('error', 'Client failed to updated!');
          return redirect()->route('admin.client.index');
      }

  }

  public function destroy($id)
    {
      if (!empty($id)) {
          $deleteData = Client::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Client successfully deleted!');
              return redirect()->route('admin.client.index');
          } else {
              session()->flash('error', 'Client failed deleted!');
              return redirect()->route('admin.client.index');
          }
      } else {
          $request->session()->flash('error', 'Client not found!');
              return redirect()->route('admin.client.index');
      }
    }
}
