<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataTables\CorporateIdentityDataTable;
use App\Models\CorporateIdentity;
use Datatables;


class CorporateIdentityController extends Controller
{
    public function __construct()
    {
        $this->model = new CorporateIdentity;
        // $this->repository = new UserRepository;
    }

    public function index(Request $req, CorporateIdentityDataTable $dataTable)
    {
        $param = $req->all();

        if (array_key_exists('message', $param)) {
            flash()->success($param['message']);
            return $dataTable->render('backend.corporate_identities.index');
        } else {
            return $dataTable->render('backend.corporate_identities.index');
        }
    }

    public function create()
    {
        return view('backend.corporate_identities.create');
    }

    public function store(Request $request)
    {
        $param = $request->except('_token');
        // dd($param);
        $saveData = CorporateIdentity::create($param);

        // if ($file = $request->hasFile('image')) {
        //     $file = $request->file('image');
        //     $filename = time() . '.' . $file->getClientOriginalExtension();
        //     $destinationPath = public_path() . '/uploads/corporate_action/';
        //     $file->move($destinationPath, $filename);
        //     $saveData->file = 'uploads/corporate_action/'.$filename;
        // }
        // $saveData->update();

        if (!empty($saveData)) {
            $request->session()->flash('success', 'Identity created successfully!');
            return redirect()->route('admin.corporate-identity.index');
        } else {
            $request->session()->flash('error', 'Identity failed to created!');
            return redirect()->route('admin.corporate-identity.index');
        }
    }

    public function edit($id)
    {
        $data = CorporateIdentity::findOrFail($id);

        if (!empty($data)) {
            return view('backend.corporate_identities.edit')->withData($data);
        } else {
            session()->flash('error', 'Data not found!');
            return redirect()->route('admin.corporate-identity.index');
        }
    }

    public function update(Request $request, $id)
    {
        $param = $request->except('_token');
        $updateData = CorporateIdentity::findOrFail($id);
        $updateData->update($param);

        if (@$param['group_of_companies']) {
            $updateData = CorporateIdentity::findOrFail($id);
            $uploadImage = upload_file($param['group_of_companies'], 'uploads/corporate_identity/');
            $updateData->group_of_companies = $uploadImage['original'];
            $updateData->update();
        }
        

        if (!empty($updateData)) {
            $request->session()->flash('success', 'Identity updated successfully!');
            return redirect()->route('admin.corporate-identity.index');
        } else {
            $request->session()->flash('error', 'Identity failed to updated!');
            return redirect()->route('admin.corporate-identity.index');
        }

    }

}
