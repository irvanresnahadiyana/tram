<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\InvestorCalendarDataTable;
use App\Http\Controllers\Controller;
use App\Models\InvestorCalendar;

class InvestorCalendarController extends Controller
{
  public function index(Request $req, InvestorCalendarDataTable $dataTable)
  {
      $param = $req->all();

      if (array_key_exists('message', $param)) {
          flash()->success($param['message']);
          return $dataTable->render('backend.investor_calendar.index');
      } else {
          return $dataTable->render('backend.investor_calendar.index');
      }
  }

  public function create()
  {
      return view('backend.investor_calendar.create');
  }

  public function store(Request $request)
  {
      $param = $request->except('_token');
      // dd($param);
      $investor = New InvestorCalendar;
      $investor->description = $param['description'];
      $investor->description_id = $param['description_id'];
      $investor->date = $param['date'];

      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/calendar/');
        $investor->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/calendar/';
            $file->move($destinationPath, $filename);
            $investor->file = 'uploads/calendar/'.$filename;
      }

      $investor->save();

      if (!empty($investor)) {
          $request->session()->flash('success', 'Investor Calendar created successfully!');
          return redirect()->route('admin.calendar.index');
      } else {
          $request->session()->flash('error', 'Investor Calendar to created!');
          return redirect()->route('admin.calendar.index');
      }
  }

  public function edit($id)
  {
      $data = InvestorCalendar::findOrFail($id);

      if (!empty($data)) {
          return view('backend.investor_calendar.edit')->withData($data);
      } else {
          session()->flash('error', 'Data not found!');
          return redirect()->route('admin.calendar.index');
      }
  }

  public function update(Request $request, $id)
  {
      $param = $request->except('_token');
      $updateData = InvestorCalendar::findOrFail($id);
      $updateData->description = $param['description'];
      $updateData->description_id = $param['description_id'];
      $updateData->date = $param['date'];
      if (@$param['image']) {
        $uploadImage = upload_file($param['image'], 'uploads/calendar/');
        $updateData->image = $uploadImage['original'];
      }

      if ($file = $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/calendar/';
            $file->move($destinationPath, $filename);
            $updateData->file = 'uploads/calendar/'.$filename;
      }
      $updateData->update();

      if (!empty($updateData)) {
          $request->session()->flash('success', 'Investor Calendar updated successfully!');
          return redirect()->route('admin.calendar.index');
      } else {
          $request->session()->flash('error', 'Investor Calendar failed to updated!');
          return redirect()->route('admin.calendar.index');
      }

  }

  public function destroy($id)
  {
      if (!empty($id)) {
          $deleteData = FinancialHighlight::findOrFail($id);
          $deleteData->delete();

          if ($deleteData != false) {
              session()->flash('success', 'Investor Relation successfully deleted!');
              return redirect()->route('admin.financial.index');
          } else {
              session()->flash('error', 'Investor Relation failed deleted!');
              return redirect()->route('admin.financial.index');
          }
      } else {
          $request->session()->flash('error', 'Investor Relation not found!');
              return redirect()->route('admin.financial.index');
      }
  }
}
